# Code for "Untangling the Mistral and Seasonal Atmospheric Forcing Driving Deep Convection in the Gulf of Lion: 1993-2013"

This is the code used to perform the analysis and produce plots for the JGR Oceans's article ["Untangling the Mistral and Seasonal Atmospheric Forcing Driving Deep Convection in the Gulf of Lion: 1993-2013"](). Intermediate data is also provided.
