import pickle
import numpy as np
from numpy import ma
from sklearn.metrics import mean_squared_error

#  ──────────────────────────────────────────────────────────────────────────

with open('../data/wrforch_quikscat.pkl', 'rb') as file:
    data = pickle.load(file)

# with open('../data/wrforch_quikscat_patch.pkl', 'rb') as file:
    # data_patch = pickle.load(file)

#  ──────────────────────────────────────────────────────────────────────────

t = data['t']

lat = data['wrforch']['lat_lon'][:, 0]
lon = data['wrforch']['lat_lon'][:, 1]

wrforch = data['wrforch']
quikscat = data['quikscat']

wrforch['u'] = np.array(wrforch['u'])
wrforch['v'] = np.array(wrforch['v'])
quikscat['u'] = np.array(quikscat['u'])
quikscat['v'] = np.array(quikscat['v'])

wrforch['ws'] = (wrforch['u']**2 + wrforch['v']**2)**.5
quikscat['ws'] = (quikscat['u']**2 + quikscat['v']**2)**.5
# quikscat['ws'] = np.array(data_patch['quikscat']['ws'])

wrforch['wd'] = np.arctan2(wrforch['v'], wrforch['u'])
quikscat['wd'] = np.arctan2(quikscat['v'], quikscat['u'])

#  ──────────────────────────────────────────────────────────────────────────
# temporal relative bias

rbias = {}
rrmse = {}
rq95 = {}
for i in ['ws', 'wd', 'u', 'v']:
    rbias[i] = np.nanmean(wrforch[i] - quikscat[i], axis=0)/np.nanmean(quikscat[i], axis=0)
    rq95[i] = (np.nanquantile(wrforch[i], .95, axis=0) - np.nanquantile(quikscat[i], .95, axis=0))/np.nanquantile(quikscat[i], .95, axis=0)
    
    rrmse[i] = []
    for j in range(wrforch[i].shape[1]):
        tmp = np.nanmean(((wrforch[i][:, j] - quikscat[i][:, j])**2)**.5, axis=0)/np.nanmean(quikscat[i], axis=0)[j]
        rrmse[i].append(tmp)
    rrmse[i] = np.array(rrmse[i])

r = {'bias': rbias, 'rmse': rrmse, 'q95': rq95}

#  ──────────────────────────────────────────────────────────────────────────
# temporal correlation coefficient

corr = {}
for i in ['ws', 'wd', 'u', 'v']:
    corr[i] = []
    for j in range(wrforch[i].shape[1]):
        tmp = ma.corrcoef(ma.masked_invalid(wrforch[i][:, j]), ma.masked_invalid(quikscat[i][:, j]))
        tmp = tmp[0][1]
        
        if ma.is_masked(tmp):
            corr[i].append(np.nan)
        else:
            corr[i].append(tmp)
    corr[i] = np.array(corr[i])

#  ──────────────────────────────────────────────────────────────────────────
# domain

m = {}
b = {}
rmse = {}
std = {}
q95 = {}
bq95 = {}
for i in ['ws', 'wd', 'u', 'v']:
    m[i] = np.nanmean(quikscat[i])
    b[i] = np.nanmean(wrforch[i] - quikscat[i])
    rmse[i] = np.nanmean(np.square(wrforch[i] - quikscat[i]))**.5
    std[i] = np.nanstd(quikscat[i])
    q95[i] = np.nanquantile(quikscat[i], .95)
    bq95[i] = np.nanquantile(wrforch[i], .95) - q95[i]

mm = {'m': m, 'bias': b, 'rmse': rmse, 'std': std, 'q95': q95, 'bq95': bq95}

#  ──────────────────────────────────────────────────────────────────────────
# domain quantiles

qs = np.linspace(0, 1, 100)
wrforch_q = {}
quikscat_q = {}
for i in ['ws', 'wd', 'u', 'v']:
    wrforch_q[i] = np.zeros_like(qs)
    quikscat_q[i] = np.zeros_like(qs)

    for j, qi in enumerate(qs):
        wrforch_q[i][j] = np.nanquantile(wrforch[i], qi)
        quikscat_q[i][j] = np.nanquantile(quikscat[i], qi)

qq = {'wrforch': wrforch_q, 'quikscat': quikscat_q, 'q': qs}

#  ──────────────────────────────────────────────────────────────────────────
# save

save_data = {'relative': r, 'corr': corr, 'mean': mm, 'qq': qq, 'time': t, 'lat': lat, 'lon': lon}
with open('../data/comp_wrf_quik.pkl', 'wb') as file:
    pickle.dump(save_data, file)
