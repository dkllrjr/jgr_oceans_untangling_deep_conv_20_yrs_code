# area of interest: lon = 1, 9 E, lat = 39, 45
# ──────────────────────────────────────────────────────────────────────────

import xarray as xr
from glob import glob
from datetime import datetime
import numpy as np
import pickle
import gsw

# ──────────────────────────────────────────────────────────────────────────

host_data = '../../../host_data/phd/argo/'
paths = glob(host_data + '*/*.nc')
paths.sort()

# ──────────────────────────────────────────────────────────────────────────

beg = datetime(1993, 7, 1, 0)
end = datetime(2013, 7, 1, 0)

t = []
lat, lon = [], []
T, S, d = [], [], []

# TEMP, PSAL, PRES
for path in paths:
    datai = xr.open_dataset(path)
    ti = datai.JULD.values
    var_list = list(datai.variables)

    for j, tj in enumerate(ti):
        if np.datetime64(beg) <= tj < np.datetime64(end):  # selecting proper time

            latj = datai.LATITUDE[j].values
            lonj = datai.LONGITUDE[j].values

            if 39 <= latj <= 45 and 1 <= lonj <= 9:  # separating NW MED

                t.append(tj)
                lat.append(latj)
                lon.append(lonj)

                Pj = datai.PRES[j].values
                dj = gsw.conversions.z_from_p(Pj, latj)
                d.append(dj)

                if 'TEMP' in var_list and 'PSAL' in var_list:
                    Sj = datai.PSAL[j].values
                    S.append(Sj)

                    Tj_temp = datai.TEMP[j].values
                    Tj = gsw.conversions.pt0_from_t(gsw.SA_from_SP(Sj, Pj, lonj, latj), Tj_temp, Pj)
                    T.append(Tj)

                else:
                    T.append(None)
                    
                    if 'PSAL' in var_list:
                        Sj = datai.PSAL[j].values
                        S.append(Sj)

                    else:
                        S.append(None)

# ──────────────────────────────────────────────────────────────────────────
# cleaning nans

t = np.array(t)
lat, lon = np.array(lat), np.array(lon)
T, S, d = np.array(T), np.array(S), np.array(d)

ind = []
for i, Ti in enumerate(T):
    
    if np.isnan(Ti).all() or len(Ti[np.invert(np.isnan(Ti))]) < 3:
        pass
    elif isinstance(Ti, type(None)):
        pass
    else:
        ind.append(i)

t = t[ind]
lat, lon = lat[ind], lon[ind]
T, S, d = T[ind], S[ind], d[ind]

# ──────────────────────────────────────────────────────────────────────────
# sorting time

ind = np.argsort(t)

t = t[ind]
lat, lon = lat[ind], lon[ind]
T, S, d = T[ind], S[ind], d[ind]

# ──────────────────────────────────────────────────────────────────────────
# saving

data = {'t': t, 'lat': lat, 'lon': lon, 'T': T, 'S': S, 'd': d}

with open('../data/argo.pickle', 'wb') as file:
    pickle.dump(data, file)
