import xarray as xr
from glob import glob
import numpy as np
import pickle
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib.patches import Rectangle
from matplotlib import colors as mcolors

import compare_nemo_ctd_argo as cnca

#  ──────────────────────────────────────────────────────────────────────────

with open('../data/nemo.pickle', 'rb') as file:
    nemo_data = pickle.load(file)

with open('../data/argo.pickle', 'rb') as file:
    argo_data = pickle.load(file)

with open('../data/ctd.pickle', 'rb') as file:
    ctd_data = pickle.load(file)

# ──────────────────────────────────────────────────────────────────────────

argo_lats = argo_data['lat']
argo_lons = argo_data['lon']
ctd_lats = ctd_data['lat']
ctd_lons = ctd_data['lon']

# ──────────────────────────────────────────────────────────────────────────

print('plotting')
# plot_path = '../plots/ctd_argo_loc.png'
plot_path = '../plots/ctd_argo_loc.pdf'

fsize = 28
tsize = 34
pad = 0.05
ms = 35

fig, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=400, subplot_kw={'projection': ccrs.PlateCarree(5)})
    
europe_land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white') 

gl = ax.gridlines(crs=ccrs.PlateCarree(),draw_labels=True,linewidth=.75, color='black', alpha=0.35, linestyle='--')
gl.xlabels_top = False
gl.ylabels_right = False
gl.xlocator = mticker.FixedLocator([-90,2,5,8,90])
gl.ylocator = mticker.FixedLocator([0,40,42,44,60])
gl.xformatter = LONGITUDE_FORMATTER
gl.yformatter = LATITUDE_FORMATTER
gl.xlabel_style = {'size': fsize}
gl.ylabel_style = {'size': fsize}
gl.xpadding = 10
gl.ypadding = 10

ax.set_extent([1,9,39,45], crs=ccrs.PlateCarree());

ax.add_feature(europe_land_10m)

# x, y = 4.25, 42
# rect = Rectangle((x, y), 5-x, 42.5-y, fill=False, linewidth=2, edgecolor='black', transform=ccrs.PlateCarree(), label='Spatial Ave.', zorder=3)
# ax.add_patch(rect)

ax.scatter(argo_lons, argo_lats, ms, color='tab:blue', label='ARGO', transform=ccrs.PlateCarree())
ax.scatter(ctd_lons, ctd_lats, ms, marker='^', color='tab:red', label='CTD', transform=ccrs.PlateCarree())

ax.legend(fontsize=fsize-4)
ax.set_title('Profile Locations', fontsize=tsize, pad=20)

fig.tight_layout()
fig.savefig(plot_path)
plt.close()
