import xarray as xr
import pickle
import numpy as np

#  ──────────────────────────────────────────────────────────────────────────

d = xr.open_dataset('../data/NEMO_1993-2013_D.nc')

lats = d.nav_lat_grid_T.values
lons = d.nav_lon_grid_T.values

lon_lats = np.array(list(zip(lons.flatten(), lats.flatten())))

# ind = []
# for i, lon_lati in enumerate(lon_lats):
    # if 1 <= lon_lati[0] <= 9 and 39 <= lon_lati[1] <= 45:
        # ind.append(i)

#  ──────────────────────────────────────────────────────────────────────────

# with open('../data/nemo_lon_lats.pickle', 'wb') as file:
    # pickle.dump(lon_lats[ind], file)

with open('../data/nemo_lon_lats.pickle', 'wb') as file:
    pickle.dump(lon_lats, file)
