import pickle
import numpy as np
from sklearn.metrics import mean_squared_error
import pandas as pd

# ──────────────────────────────────────────────────────────────────────────

def temperature(T, z):
    Ta = T - 10 # C
    a0 = 1.6550e-1
    l1 = 5.9520e-2
    u1 = 1.4970e-4
    return -a0 * (1 + 0.5 * l1 * Ta + u1 * z) * Ta + 1026

def salinity(S, z):
    Sa = S - 35 # PSU
    b0 = 7.6554e-1
    l2 = 5.4914e-4
    u2 = 1.1090e-5
    return b0 * (1 - 0.5 * l2 * Sa - u2 * z) * Sa + 1026

def nonlinear(T, S):
    Ta = T - 10
    Sa = S - 35
    nu = 2.4341e-3
    return - nu * Ta * Sa + 1026

#  ──────────────────────────────────────────────────────────────────────────

with open('../data/nemo.pickle', 'rb') as file:
    nemo_data = pickle.load(file)

with open('../data/argo.pickle', 'rb') as file:
    argo_data = pickle.load(file)

with open('../data/ctd.pickle', 'rb') as file:
    ctd_data = pickle.load(file)

# argo + ctd in that order

# ──────────────────────────────────────────────────────────────────────────
# compiling observational data`

lats = np.append(argo_data['lat'], ctd_data['lat'], axis=0)
lons = np.append(argo_data['lon'], ctd_data['lon'], axis=0)

to = np.append(argo_data['t'], ctd_data['t'], axis=0)
do = np.append(argo_data['d'], ctd_data['d'], axis=0)
To = np.append(argo_data['T'], ctd_data['T'], axis=0)
So = np.append(argo_data['S'], ctd_data['S'], axis=0)

argo_ctd = np.arange(len(To))
argo_ctd = np.where(argo_ctd < len(argo_data['t']), True, False) # argo indices

# ──────────────────────────────────────────────────────────────────────────
# compiling nemo data

dn = nemo_data['T']['d']
Tn = np.array(nemo_data['T']['T'], dtype=object)
Sn = np.array(nemo_data['S']['S'], dtype=object)

# ──────────────────────────────────────────────────────────────────────────
# determining good data

ind = []
for i, Sni in enumerate(Sn):
    if not isinstance(Sni, type(None)):
        ind.append(i)

        Sn[i] = np.where(Sni < 1, np.nan, Sni)
        Tn[i] = np.where(Sni < 1, np.nan, Tn[i])

to = to[ind]
do = -do[ind]  #ctd and argo data have this as negative, model does not
To = To[ind]
So = So[ind]

argo_ctd = argo_ctd[ind]

Tn = Tn[ind]
Sn = Sn[ind]

lats = lats[ind]
lons = lons[ind]

# ──────────────────────────────────────────────────────────────────────────
# comparing data

# bias will be model deviation from observations, so model - obs
# relative bias will be (model - obs)/obs

Tbias, Trbias = [], []
Sbias, Srbias = [], []
dTbias, dTrbias = [], []
dSbias, dSrbias = [], []
dnonbias, dnonrbias = [], []

dobias = []

Trmseo, Trmsen = [], []
Srmseo, Srmsen = [], []

for i, Tni in enumerate(Tn):
    Sni = Sn[i]

    # interp
    not_nan = np.invert(np.isnan(To[i]))

    Toi = To[i][not_nan]
    Soi = So[i][not_nan]
    doi = do[i][not_nan]
    dobias.append(doi)

    Tn_int = np.interp(doi, dn, Tni)
    Sn_int = np.interp(doi, dn, Sni)

    Tbiasi = Tn_int - Toi
    Trbiasi = Tbiasi/Toi

    Tbias.append(Tbiasi)
    Trbias.append(Trbiasi)

    Sbiasi = Sn_int - Soi
    Srbiasi = Sbiasi/Soi

    Sbias.append(Sbiasi)
    Srbias.append(Srbiasi)

    # density
    dTbiasi = temperature(Tn_int, doi) - temperature(Toi, doi)
    dTrbiasi = dTbiasi/temperature(Toi, doi)

    dTbias.append(dTbiasi)
    dTrbias.append(dTrbiasi)

    dSbiasi = salinity(Sn_int, doi) - salinity(Soi, doi)
    dSrbiasi = dSbiasi/salinity(Soi, doi)

    dSbias.append(dSbiasi)
    dSrbias.append(dSrbiasi)

    dnonbiasi = nonlinear(Tn_int, Sn_int) - nonlinear(Toi, Soi)
    dnonrbiasi = dnonbiasi/nonlinear(Toi, Soi)

    dnonbias.append(dnonbiasi)
    dnonrbias.append(dnonrbiasi)

    Trmseo.append(Toi)
    Trmsen.append(Tn_int)

    Srmseo.append(Soi)
    Srmsen.append(Sn_int)

Tbias = np.array(Tbias, dtype=object)
Trbias = np.array(Trbias, dtype=object)

Sbias = np.array(Sbias, dtype=object)
Srbias = np.array(Srbias, dtype=object)

dTbias = np.array(dTbias, dtype=object)
dTrbias = np.array(dTrbias, dtype=object)

dSbias = np.array(dSbias, dtype=object)
dSrbias = np.array(dSrbias, dtype=object)

# ──────────────────────────────────────────────────────────────────────────
# total average bias

Tbias_tot = np.array([])
Trbias_tot = np.array([])
Sbias_tot = np.array([])
Srbias_tot = np.array([])

dTbias_tot = np.array([])
dTrbias_tot = np.array([])
dSbias_tot = np.array([])
dSrbias_tot = np.array([])
dnonbias_tot = np.array([])
dnonrbias_tot = np.array([])

for i, _ in enumerate(Tbias):
    Tbias_tot = np.append(Tbias_tot, Tbias[i])
    Trbias_tot = np.append(Trbias_tot, Trbias[i])
    Sbias_tot = np.append(Sbias_tot, Sbias[i])
    Srbias_tot = np.append(Srbias_tot, Srbias[i])

    dTbias_tot = np.append(dTbias_tot, dTbias[i])
    dTrbias_tot = np.append(dTrbias_tot, dTrbias[i])
    dSbias_tot = np.append(dSbias_tot, dSbias[i])
    dSrbias_tot = np.append(dSrbias_tot, dSrbias[i])
    dnonbias_tot = np.append(dnonbias_tot, dnonbias[i])
    dnonrbias_tot = np.append(dnonrbias_tot, dnonrbias[i])

Tbias_tot_std = np.nanstd(Tbias_tot)
Tbias_tot = np.nanmean(Tbias_tot)
Trbias_tot_std = np.nanstd(Trbias_tot)
Trbias_tot = np.nanmean(Trbias_tot)

Sbias_tot_std = np.nanstd(Sbias_tot)
Sbias_tot = np.nanmean(Sbias_tot)
Srbias_tot_std = np.nanstd(Srbias_tot)
Srbias_tot = np.nanmean(Srbias_tot)

dTbias_tot_std = np.nanstd(dTbias_tot)
dTbias_tot = np.nanmean(dTbias_tot)
dTrbias_tot_std = np.nanstd(dTrbias_tot)
dTrbias_tot = np.nanmean(dTrbias_tot)

dSbias_tot_std = np.nanstd(dSbias_tot)
dSbias_tot = np.nanmean(dSbias_tot)
dSrbias_tot_std = np.nanstd(dSrbias_tot)
dSrbias_tot = np.nanmean(dSrbias_tot)

dnonbias_tot_std = np.nanstd(dnonbias_tot)
dnonbias_tot = np.nanmean(dnonbias_tot)
dnonrbias_tot_std = np.nanstd(dnonrbias_tot)
dnonrbias_tot = np.nanmean(dnonrbias_tot)

# ──────────────────────────────────────────────────────────────────────────
# vertical binning

N = 56
vert_bin_edges = np.linspace(dn[0], dn[-1], N)
vert_bins = np.diff(vert_bin_edges)/2 + vert_bin_edges[0:-1]

T_vert_bins = []
Tr_vert_bins = []
Trmsevo, Trmsevn = [], []
for i in range(len(vert_bin_edges) - 1):
    T_vert_bins.append([])
    Tr_vert_bins.append([])
    Trmsevo.append([])
    Trmsevn.append([])

S_vert_bins = []
Sr_vert_bins = []
Srmsevo, Srmsevn = [], []
for i in vert_bin_edges:
    S_vert_bins.append([])
    Sr_vert_bins.append([])
    Srmsevo.append([])
    Srmsevn.append([])

dT_vert_bins = []
dTr_vert_bins = []
for i in range(len(vert_bin_edges) - 1):
    dT_vert_bins.append([])
    dTr_vert_bins.append([])

dS_vert_bins = []
dSr_vert_bins = []
for i in vert_bin_edges:
    dS_vert_bins.append([])
    dSr_vert_bins.append([])

Tbias_top, Tbias_upper, Tbias_lower = [], [], []
Sbias_top, Sbias_upper, Sbias_lower = [], [], []

top = []
upper = []
lower = []
argo_top = []
argo_upper = []
argo_lower = []
ctd_top = []
ctd_upper = []
ctd_lower = []

for i, dobiasi in enumerate(dobias):
    Tbias_top_temp = []
    Sbias_top_temp = []
    Tbias_upper_temp = []
    Sbias_upper_temp = []
    Tbias_lower_temp = []
    Sbias_lower_temp = []

    for j, dobiasj in enumerate(dobiasi):
        for k in range(len(vert_bin_edges) - 1):

            if vert_bin_edges[k] <= dobiasj < vert_bin_edges[k + 1]:
                T_vert_bins[k].append(Tbias[i][j])
                Tr_vert_bins[k].append(Trbias[i][j])
                S_vert_bins[k].append(Sbias[i][j])
                Sr_vert_bins[k].append(Srbias[i][j])

                dT_vert_bins[k].append(dTbias[i][j])
                dTr_vert_bins[k].append(dTrbias[i][j])
                dS_vert_bins[k].append(dSbias[i][j])
                dSr_vert_bins[k].append(dSrbias[i][j])

                Trmsevo[k].append(Trmseo[i][j])
                Trmsevn[k].append(Trmsen[i][j])
                Srmsevo[k].append(Srmseo[i][j])
                Srmsevn[k].append(Srmsen[i][j])

            if 150 >= dobiasj:
                top.append(i)
                if argo_ctd[i]:
                    argo_top.append(i)
                else:
                    ctd_top.append(i)

                Tbias_top_temp.append(Tbias[i][j])
                Sbias_top_temp.append(Sbias[i][j])
            elif 600 >= dobiasj > 150:
                upper.append(i)
                if argo_ctd[i]:
                    argo_upper.append(i)
                else:
                    ctd_upper.append(i)

                Tbias_upper_temp.append(Tbias[i][j])
                Sbias_upper_temp.append(Sbias[i][j])
            else:
                lower.append(i)
                if argo_ctd[i]:
                    argo_lower.append(i)
                else:
                    ctd_lower.append(i)

                Tbias_lower_temp.append(Tbias[i][j])
                Sbias_lower_temp.append(Sbias[i][j])

    Tbias_top.append(np.nanmean(Tbias_top_temp))
    Sbias_top.append(np.nanmean(Sbias_top_temp))
    Tbias_upper.append(np.nanmean(Tbias_upper_temp))
    Sbias_upper.append(np.nanmean(Sbias_upper_temp))
    Tbias_lower.append(np.nanmean(Tbias_lower_temp))
    Sbias_lower.append(np.nanmean(Sbias_lower_temp))

top = set(top)
upper = set(upper)
lower = set(lower)
argo_top = set(argo_top)
argo_upper = set(argo_upper)
argo_lower = set(argo_lower)
ctd_top = set(ctd_top)
ctd_upper = set(ctd_upper)
ctd_lower = set(ctd_lower)

total = set(list(top) + list(upper) + list(lower))
argo_total = set(list(argo_top) + list(argo_upper) + list(argo_lower))
ctd_total = set(list(ctd_top) + list(ctd_upper) + list(ctd_lower))

total_top = set(list(argo_top) + list(ctd_top))
total_upper = set(list(argo_upper) + list(ctd_upper))
total_lower = set(list(argo_lower) + list(ctd_lower))

Tbias_top = np.array(Tbias_top)
Tbias_upper = np.array(Tbias_upper)
Tbias_lower = np.array(Tbias_lower)

Sbias_top = np.array(Sbias_top)
Sbias_upper = np.array(Sbias_upper)
Sbias_lower = np.array(Sbias_lower)

Tv, Tv_std = np.zeros(len(T_vert_bins)), np.zeros(len(T_vert_bins))
Sv, Sv_std = np.zeros(len(T_vert_bins)), np.zeros(len(T_vert_bins))
Trv, Trv_std = np.zeros(len(T_vert_bins)), np.zeros(len(T_vert_bins))
Srv, Srv_std = np.zeros(len(T_vert_bins)), np.zeros(len(T_vert_bins))
Trmse, Srmse = np.zeros(len(T_vert_bins)), np.zeros(len(T_vert_bins))

dTv, dTv_std = np.zeros(len(T_vert_bins)), np.zeros(len(T_vert_bins))
dSv, dSv_std = np.zeros(len(T_vert_bins)), np.zeros(len(T_vert_bins))
dTrv, dTrv_std = np.zeros(len(T_vert_bins)), np.zeros(len(T_vert_bins))
dSrv, dSrv_std = np.zeros(len(T_vert_bins)), np.zeros(len(T_vert_bins))

for i, _ in enumerate(T_vert_bins):

    Tv[i] = np.nanmean(T_vert_bins[i])
    Trv[i] = np.nanmean(Tr_vert_bins[i]) * 100

    Sv[i] = np.nanmean(S_vert_bins[i])
    Srv[i] = np.nanmean(Sr_vert_bins[i]) * 100

    Tv_std[i] = np.nanstd(T_vert_bins[i])
    Trv_std[i] = np.nanstd(Tr_vert_bins[i]) * 100

    Sv_std[i] = np.nanstd(S_vert_bins[i])
    Srv_std[i] = np.nanstd(Sr_vert_bins[i]) * 100

    # density
    dTv[i] = np.nanmean(dT_vert_bins[i])
    dTrv[i] = np.nanmean(dTr_vert_bins[i]) * 100

    dSv[i] = np.nanmean(dS_vert_bins[i])
    dSrv[i] = np.nanmean(dSr_vert_bins[i]) * 100

    dTv_std[i] = np.nanstd(dT_vert_bins[i])
    dTrv_std[i] = np.nanstd(dTr_vert_bins[i]) * 100

    dSv_std[i] = np.nanstd(dS_vert_bins[i])
    dSrv_std[i] = np.nanstd(dSr_vert_bins[i]) * 100

    if len(Trmsevo[i]) > 0:

        Trmsevo_temp = np.array(Trmsevo[i])[np.invert(np.isnan(Trmsevo[i]))]
        Trmsevn_temp = np.array(Trmsevn[i])[np.invert(np.isnan(Trmsevo[i]))]
        Srmsevo_temp = np.array(Srmsevo[i])[np.invert(np.isnan(Srmsevo[i]))]
        Srmsevn_temp = np.array(Srmsevn[i])[np.invert(np.isnan(Srmsevo[i]))]

        Trmsevo_temp = Trmsevo_temp[np.invert(np.isnan(Trmsevn_temp))]
        Trmsevn_temp = Trmsevn_temp[np.invert(np.isnan(Trmsevn_temp))]
        Srmsevo_temp = Srmsevo_temp[np.invert(np.isnan(Srmsevn_temp))]
        Srmsevn_temp = Srmsevn_temp[np.invert(np.isnan(Srmsevn_temp))]

        Trmse[i] = mean_squared_error(Trmsevo_temp, Trmsevn_temp, squared=False)
        Srmse[i] = mean_squared_error(Srmsevo_temp, Srmsevn_temp, squared=False)

Trmse = np.where(Trmse > 0, Trmse, np.nan)
Srmse = np.where(Srmse > 0, Srmse, np.nan)

#  ──────────────────────────────────────────────────────────────────────────
# temporal bias

Tmonths = []
Smonths = []
Tmonths = []
Smonths = []
for i in range(12):
    Tmonths.append([])
    Smonths.append([])

for i, toi in enumerate(to):
    month = pd.Timestamp(toi).month
    Tmonths[month - 1] = np.append(Tmonths[month - 1], Tbias[i])
    Smonths[month - 1] = np.append(Smonths[month - 1], Sbias[i])

months = np.arange(1, 13)
Tmonths_std = np.zeros(12)
Smonths_std = np.zeros(12)
Tmonths_m = np.zeros(12)
Smonths_m = np.zeros(12)
for i in range(len(months)):
    Tmonths_m[i] = np.nanmean(Tmonths[i])
    Smonths_m[i] = np.nanmean(Smonths[i])
    Tmonths_std[i] = np.nanstd(Tmonths[i])
    Smonths_std[i] = np.nanstd(Smonths[i])

