import xarray as xr
from glob import glob
import numpy as np
import pickle
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib.patches import Rectangle
from matplotlib import colors as mcolors
import matplotlib.dates as mdates
import matplotlib.tri as tri
from scipy.ndimage import generic_filter

import compare_nemo_argo_mld as cnam

#  ──────────────────────────────────────────────────────────────────────────

def signed_log(x):
    return np.sign(x) * np.log10(np.abs(x))

# ──────────────────────────────────────────────────────────────────────────

def gol_bias_plot(i, ax, lats, lons, bias, var_min, var_max, levels, cmap):

    fsize = 28
    tsize = 34
        
    europe_land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white') 
    
    gl = ax.gridlines(crs=ccrs.PlateCarree(),draw_labels=True,linewidth=.75, color='black', alpha=0.35, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-90,2,5,8,90])
    gl.ylocator = mticker.FixedLocator([0,40,42,44,60])
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': fsize}
    gl.ylabel_style = {'size': fsize}
    gl.xpadding = 10
    gl.ypadding = 10

    if i in [1]:
        gl.ylabels_left = False
    
    ax.set_extent([1,9,39,45], crs=ccrs.PlateCarree());

    ax.add_feature(europe_land_10m)

    x, y = 4.25, 42
    rect = Rectangle((x, y), 5-x, 42.5-y, fill=False, linewidth=2, edgecolor='black', transform=ccrs.PlateCarree(), label='Spatial Ave.', zorder=3)
    ax.add_patch(rect)

    with open('../data/nemo_lon_lats.pickle', 'rb') as file:
        lon_lats = pickle.load(file)

    triang = tri.Triangulation(lons, lats)
    tri_interp = tri.LinearTriInterpolator(triang, bias)
    bias_interp = tri_interp(lon_lats[:,0], lon_lats[:,1])

    d = xr.open_dataset('../data/NEMO_1993-2013_D.nc')

    nlats = d.nav_lat_grid_T.values
    nlons = d.nav_lon_grid_T.values

    bias_interp = bias_interp.reshape(nlons.shape)
    mean_bias_interp = generic_filter(bias_interp, np.nanmean, size=5)

    gol_map = ax.contourf(nlons, nlats, mean_bias_interp, transform=ccrs.PlateCarree(), vmin=var_min, vmax=var_max, levels=levels, extend='both', cmap=cmap)  # plotting filled contours of the data

    return ax, gol_map


def gol_bias_plot_2(i, ax, lats, lons, bias, var_min, var_max, levels, si_locs, cmap):

    fsize = 28
    tsize = 34

    europe_land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white') 

    gl = ax.gridlines(crs=ccrs.PlateCarree(),draw_labels=True,linewidth=.75, color='black', alpha=0.35, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-90,2,5,8,90])
    gl.ylocator = mticker.FixedLocator([0,40,42,44,60])
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': fsize}
    gl.ylabel_style = {'size': fsize}
    gl.xpadding = 10
    gl.ypadding = 10

    if i in [1]:
        gl.ylabels_left = False

    ax.set_extent([1,9,39,45], crs=ccrs.PlateCarree());

    ax.add_feature(europe_land_10m)

    x, y = 4.25, 42
    rect = Rectangle((x, y), 5-x, 42.5-y, fill=False, linewidth=2, edgecolor='black', transform=ccrs.PlateCarree(), label='Spatial Ave.', zorder=3)
    ax.add_patch(rect)

    for loc in si_locs:
        ax.scatter(loc[1], loc[0], transform=ccrs.PlateCarree(), marker='x', zorder=10, color='k', s=100)

    gol_map = ax.contourf(lons, lats, bias, transform=ccrs.PlateCarree(), vmin=var_min, vmax=var_max, levels=levels, extend='both', cmap=cmap)  # plotting filled contours of the data

    return ax, gol_map
    
# ──────────────────────────────────────────────────────────────────────────

print('plotting')
plot_path = '../plots/bias_mld_spatial'

fsize = 28
tsize = 34
pad = 0.05

#  ──────────────────────────────────────────────────────────────────────────

# fig, ax = plt.subplots(2, 2, figsize=(17, 12), dpi=400, subplot_kw={'projection': ccrs.PlateCarree(5)})
# ax = ax.flatten()

# # setting up the fig for the plot colorbar
# fig.subplots_adjust(top=0.92, bottom=0.05, left=0.08, right=0.87, wspace=0.03, hspace=0.001)
# levels = 200
# cmap = 'bwr'

# #  ──────────────────────────────────────────────────────────────────────────

# # print(np.max(cnam.bias), np.min(cnam.bias))

# # bias
# var_min = -2500
# var_max = 2500

# ax[0], gol_map1 = gol_bias_plot(0, ax[0], cnam.lats, cnam.lons, cnam.bias[0][0], var_min, var_max, levels, cmap=cmap)
# ax[1], gol_map1 = gol_bias_plot(1, ax[1], cnam.lats, cnam.lons, cnam.bias[0][1], var_min, var_max, levels, cmap=cmap)

# ax[2], gol_map1 = gol_bias_plot(2, ax[2], cnam.lats, cnam.lons, cnam.bias[1][0], var_min, var_max, levels, cmap=cmap)
# ax[3], gol_map1 = gol_bias_plot(3, ax[3], cnam.lats, cnam.lons, cnam.bias[1][1], var_min, var_max, levels, cmap=cmap)

# # setting up the plot colorbar
# cax = fig.add_axes([0.88, 0.05, 0.02, 0.875])
# cmap = plt.cm.bwr
# norm = mcolors.Normalize(vmin=var_min, vmax=var_max)

# cbar = plt.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), extend='both', orientation='vertical', cax=cax, pad=pad)
# # units = '$\\log{m}$'
# units = '$m$'
# cbar.set_label(units, fontsize=fsize - 4)
# cbar.ax.tick_params(labelsize=fsize - 4)

# #  ──────────────────────────────────────────────────────────────────────────

# fig.suptitle('Mixed Layer Depth Bias', fontsize=tsize)

# x1, x2 = 0.085, 0.485
# y1, y2 = 0.87, 0.43
# fig.text(x1, y1, '(a)', fontweight='bold', fontsize=fsize)
# fig.text(x2, y1, '(b)', fontweight='bold', fontsize=fsize)
# fig.text(x1, y2, '(c)', fontweight='bold', fontsize=fsize)
# fig.text(x2, y2, '(d)', fontweight='bold', fontsize=fsize)

# fig.savefig(plot_path + '.png')
# fig.savefig(plot_path + '.pdf')
# plt.close()

#  ══════════════════════════════════════════════════════════════════════════

fig = plt.figure(figsize=(22, 14), dpi=400)

subfigs = fig.subfigures(2, 1)

#  ──────────────────────────────────────────────────────────────────────────

axbig = subfigs[1].subplots(1, 1) 

x = np.arange(np.datetime64('2000-01-01'), np.datetime64('2001-01-01'), np.timedelta64(1, 'M'), dtype='datetime64[M]')

i = 0

axbig.plot(x, cnam.monthly_m[i], color='tab:red', label='Mean')
axbig.fill_between(x, cnam.monthly_m[i] - cnam.monthly_std[i], cnam.monthly_m[i] + cnam.monthly_std[i], color='tab:red', alpha=0.2, label='Std')

axbig.tick_params(labelsize=fsize - 4)
axbig.grid()
axbig.set_xlim(x[0], x[-1])
axbig.set_ylim(-850, 1250)
axbig.set_ylabel('$m$', fontsize=fsize)

axbig.legend(fontsize=fsize-4)
axbig.set_xlabel('Time', fontsize=fsize)

dateformatter = mdates.DateFormatter('%b.')
axbig.xaxis.set_major_formatter(dateformatter)

# subfigs[1].suptitle('Bias Timing', fontsize=tsize, y=0.93)

#  ──────────────────────────────────────────────────────────────────────────

ax = subfigs[0].subplots(1, 2, subplot_kw={'projection': ccrs.PlateCarree(5)})
# fig, ax = plt.subplots(1, 2, figsize=(17, 7), dpi=400, subplot_kw={'projection': ccrs.PlateCarree(5)})

# setting up the fig for the plot colorbar
# subfigs[0].subplots_adjust(top=0.92, bottom=0.10, left=0.08, right=0.87, wspace=0.03, hspace=0.001)
levels = 200
cmap = 'bwr'

#  ──────────────────────────────────────────────────────────────────────────

# print(np.max(cnam.bias), np.min(cnam.bias))

# bias
# var_min = -2000
# var_max = 2000

var_min = -900
var_max = 900

ax[0], gol_map = gol_bias_plot(0, ax[0], cnam.lats, cnam.lons, cnam.bias[0][0], var_min, var_max, levels, cmap=cmap)

with open('../data/mld_max.pkl', 'rb') as file:
    data = pickle.load(file)

with open('../data/si_min_location.pickle', 'rb') as file:
    si_data = pickle.load(file)

cmap2 = 'cool'

ax[1], gol_map = gol_bias_plot_2(1, ax[1], data['lats'], data['lons'], data['max'], 0, np.nanmax(data['max']), levels, si_data['locs'], cmap=cmap2)

# setting up the plot colorbar bias
cax = subfigs[0].add_axes([0.91, 0.07, 0.02, 0.89])
cmap = plt.cm.bwr
norm = mcolors.Normalize(vmin=0, vmax=np.nanmax(data['max']))

cbar = plt.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap2), extend='max', orientation='vertical', cax=cax, pad=pad)
# units = '$\\log{m}$'
units = '$m$'
cbar.set_label(units, fontsize=fsize - 4)
cbar.ax.tick_params(labelsize=fsize - 4)

# setting up the plot colorbar max
cax = subfigs[0].add_axes([0.45, 0.07, 0.02, 0.89])
cmap = plt.cm.bwr
norm = mcolors.Normalize(vmin=var_min, vmax=var_max)

cbar = plt.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), extend='both', orientation='vertical', cax=cax, pad=pad)
cbar.ax.tick_params(labelsize=fsize - 4)

# subfigs[0].suptitle('Mixed Layer Depth Bias', fontsize=tsize, y=0.93)
# subfigs[0].text(0.085, 0.77, 'Bias', fontsize=tsize)
# subfigs[0].text(0.485, 0.77, 'Mean Yearly Max', fontsize=tsize)

#  ──────────────────────────────────────────────────────────────────────────

x1, x2 = 0.07, 0.527
y1, y2 = 0.87, 0.43
subfigs[0].text(x1, y1, '(a)', fontweight='bold', fontsize=fsize)
subfigs[0].text(x2, y1, '(b)', fontweight='bold', fontsize=fsize)

fig.tight_layout()
subfigs[0].subplots_adjust(bottom=0.04, left=0.06, right=0.9, wspace=0.21)
subfigs[1].subplots_adjust(top=0.95, bottom=0.15, left=0.09, right=0.96, hspace=0.2)
subfigs[1].text(x1+0.03, 0.88, '(c)', fontweight='bold', fontsize=fsize)

fig.savefig(plot_path + '.png')
fig.savefig(plot_path + '.pdf')
plt.close()
