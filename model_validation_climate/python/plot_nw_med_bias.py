import xarray as xr
from glob import glob
import numpy as np
import pickle
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib.patches import Rectangle
from matplotlib import colors as mcolors
import matplotlib.tri as tri
from scipy.ndimage import generic_filter

import compare_nemo_ctd_argo as cnca

#  ──────────────────────────────────────────────────────────────────────────

def gol_bias_plot(i, ax, lats, lons, bias, var_min, var_max, levels, cmap):

    fsize = 28
    tsize = 34
        
    europe_land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white') 
    
    gl = ax.gridlines(crs=ccrs.PlateCarree(),draw_labels=True,linewidth=.75, color='black', alpha=0.35, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-90,2,5,8,90])
    gl.ylocator = mticker.FixedLocator([0,40,42,44,60])
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': fsize}
    gl.ylabel_style = {'size': fsize}
    gl.xpadding = 10
    gl.ypadding = 10

    if i in [0, 1, 2]:
        gl.xlabels_bottom = False

    if i in [1, 2, 4, 5]:
        gl.ylabels_left = False
    
    ax.set_extent([1,9,39,45], crs=ccrs.PlateCarree());

    ax.add_feature(europe_land_10m)

    x, y = 4.25, 42
    rect = Rectangle((x, y), 5-x, 42.5-y, fill=False, linewidth=2, edgecolor='black', transform=ccrs.PlateCarree(), label='Spatial Ave.', zorder=3)
    ax.add_patch(rect)

    with open('../data/nemo_lon_lats.pickle', 'rb') as file:
        lon_lats = pickle.load(file)

    triang = tri.Triangulation(lons, lats)
    tri_interp = tri.LinearTriInterpolator(triang, bias)
    bias_interp = tri_interp(lon_lats[:,0], lon_lats[:,1])

    d = xr.open_dataset('../data/NEMO_1993-2013_D.nc')

    nlats = d.nav_lat_grid_T.values
    nlons = d.nav_lon_grid_T.values
    # print(np.mean(nlats[1] - nlats[0])*5, np.mean(nlons[:,1] - nlons[:,0])*5)
    # lat: 39km, lon: 35km ~ 37km2 area

    bias_interp = bias_interp.reshape(nlons.shape)
    mean_bias_interp = generic_filter(bias_interp, np.nanmean, size=5)

    gol_map = ax.contourf(nlons, nlats, mean_bias_interp, transform=ccrs.PlateCarree(), vmin=var_min, vmax=var_max, levels=levels, extend='both', cmap=cmap)  # plotting filled contours of the data

    return ax, gol_map
    
# ──────────────────────────────────────────────────────────────────────────

inds_lower = [np.invert(np.isnan(cnca.Tbias_lower)),
        np.invert(np.isnan(cnca.Sbias_lower))]

inds_upper = [np.invert(np.isnan(cnca.Tbias_upper)),
        np.invert(np.isnan(cnca.Sbias_upper))]

inds_top = [np.invert(np.isnan(cnca.Tbias_top)),
        np.invert(np.isnan(cnca.Sbias_upper))]


ind = np.empty(len(inds_lower[0]), dtype=bool)
ind.fill(True)
for i, indi in enumerate(inds_lower):
    for j, indj in enumerate(indi):
        if not indj:
            ind[j] = False

lats_lower = cnca.lats[ind]
lons_lower = cnca.lons[ind]

Tbias_lower = cnca.Tbias_lower[ind]
Sbias_lower = cnca.Sbias_lower[ind]

argo_ctd_lower = cnca.argo_ctd[ind]


ind = np.empty(len(inds_upper[0]), dtype=bool)
ind.fill(True)
for i, indi in enumerate(inds_upper):
    for j, indj in enumerate(indi):
        if not indj:
            ind[j] = False

lats_upper = cnca.lats[ind]
lons_upper = cnca.lons[ind]

Tbias_upper = cnca.Tbias_upper[ind]
Sbias_upper = cnca.Sbias_upper[ind]

argo_ctd_upper = cnca.argo_ctd[ind]


ind = np.empty(len(inds_top[0]), dtype=bool)
ind.fill(True)
for i, indi in enumerate(inds_top):
    for j, indj in enumerate(indi):
        if not indj:
            ind[j] = False

lats_top = cnca.lats[ind]
lons_top = cnca.lons[ind]

Tbias_top = cnca.Tbias_top[ind]
Sbias_top = cnca.Sbias_top[ind]

argo_ctd_top = cnca.argo_ctd[ind]

# ──────────────────────────────────────────────────────────────────────────

print('plotting')
plot_path = '../plots/bias_ctd_argo_spatial.pdf'

fsize = 28
tsize = 34
pad = 0.05

fig, ax = plt.subplots(2, 3, figsize=(24, 12), dpi=400, subplot_kw={'projection': ccrs.PlateCarree(5)})
ax = ax.flatten()

# Temperature
var_min = -0.5
var_max = 0.5
levels = 200

ax[0], gol_map1 = gol_bias_plot(0, ax[0], lats_top, lons_top, Tbias_top, var_min, var_max, levels, cmap='coolwarm')
ax[1], gol_map1 = gol_bias_plot(1, ax[1], lats_upper, lons_upper, Tbias_upper, var_min, var_max, levels, cmap='coolwarm')
ax[2], gol_map = gol_bias_plot(2, ax[2], lats_lower, lons_lower, Tbias_lower, var_min, var_max, levels=levels, cmap='coolwarm')


# setting up the fig for the plot colorbar
fig.subplots_adjust(top=0.92, bottom=0.05, left=0.08, right=0.91, wspace=0.03, hspace=0.01)

# setting up the plot colorbar
cax = fig.add_axes([0.92, 0.50, 0.02, 0.4])
cmap = plt.cm.coolwarm
norm = mcolors.Normalize(vmin=var_min, vmax=var_max)

cbar = plt.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), extend='both', orientation='vertical', cax=cax, pad=pad)
units = '$^\\circ C$'
cbar.set_label(units, fontsize=fsize - 4)
cbar.ax.tick_params(labelsize=fsize - 4)


var_min = -0.25
var_max = 0.25

# Salinity
cmap = 'bwr'
ax[3], gol_map = gol_bias_plot(3, ax[3], lats_top, lons_top, Sbias_top, var_min, var_max, levels, cmap=cmap)
ax[4], gol_map = gol_bias_plot(4, ax[4], lats_upper, lons_upper, Sbias_upper, var_min, var_max, levels, cmap=cmap)
ax[5], gol_map = gol_bias_plot(5, ax[5], lats_lower, lons_lower, Sbias_lower, var_min, var_max, levels, cmap=cmap)

# setting up the plot colorbar
cax = fig.add_axes([0.92, 0.065, 0.02, 0.4])
cmap = plt.cm.bwr
norm = mcolors.Normalize(vmin=var_min, vmax=var_max)

cbar = plt.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), extend='both', orientation='vertical', cax=cax, pad=pad)
units = '$PSU$'
cbar.set_label(units, fontsize=fsize - 4)
cbar.ax.tick_params(labelsize=fsize - 4)


ax[0].set_title('Above 150 $m$', fontsize=fsize+4, pad=20)
ax[1].set_title('150 to 600 $m$', fontsize=fsize+4, pad=20)
ax[2].set_title('Below 600 $m$', fontsize=fsize+4, pad=20)

ax[0].legend(loc='upper right', fontsize=fsize-4)

x1, x2, x3 = 0.085, 0.3625, 0.645
y1, y2 = 0.87, 0.43
fig.text(x1, y1, '(a)', fontweight='bold', fontsize=fsize)
fig.text(x2, y1, '(b)', fontweight='bold', fontsize=fsize)
fig.text(x3, y1, '(c)', fontweight='bold', fontsize=fsize)
fig.text(x1, y2, '(d)', fontweight='bold', fontsize=fsize)
fig.text(x2, y2, '(e)', fontweight='bold', fontsize=fsize)
fig.text(x3, y2, '(f)', fontweight='bold', fontsize=fsize)

fig.savefig(plot_path)
plt.close()
