import xarray as xr
from glob import glob
from datetime import datetime
import numpy as np
import pickle

count = 0
count_in_bounds = 0
count_in_time = 0

beg = datetime(1993, 7, 1, 0)
end = datetime(2013, 7, 1, 0)

profile_paths = []

# TEMP, PSAL, PRES
for path in sorted(glob('../../../host_data/phd/new_gol_climate/DataSelection_fb1e/*.nc')):
    datai = xr.open_dataset(path)
    ti = datai.TIME.values
    var_list = list(datai.variables)
    # print(ti, datai.attrs)

    if len(datai.DEPTH) > 10:
        for j, tj in enumerate(ti):
            count += 1
            if np.datetime64(beg) <= tj < np.datetime64(end):  # selecting proper time

                count_in_time += 1

                latj = datai.LATITUDE[j].values
                lonj = datai.LONGITUDE[j].values

                if 39 <= latj <= 45 and 1 <= lonj <= 9:  # separating NW MED
                    count_in_bounds += 1
                    profile_paths.append(path)


profile_paths = sorted(set(profile_paths))

with open('../data/new_data_profile_paths.pkl', 'wb') as file:
    pickle.dump(profile_paths, file)
