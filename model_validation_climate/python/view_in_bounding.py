import pickle
import numpy as np
from numpy import ma
import compare_nemo_ctd_argo as cnca
import compare_nemo_argo_mld as cnam
import compare_wrforch_quikscat as cwq

#  ──────────────────────────────────────────────────────────────────────────

ind_cnca = []
for i, _ in enumerate(cnca.lats):
    if 4.25 <= cnca.lons[i] <= 5 and 42 <= cnca.lats[i] <= 42.5:
        ind_cnca.append(i)

#  ──────────────────────────────────────────────────────────────────────────

ind_cnam = []
for i, _ in enumerate(cnam.lats):
    if 4.25 <= cnam.lons[i] <= 5 and 42 <= cnam.lats[i] <= 42.5:
        ind_cnam.append(i)

#  ──────────────────────────────────────────────────────────────────────────

vname = ['T', 'Tr', 'S', 'Sr']
v = [cnca.Tbias[ind_cnca], cnca.Trbias[ind_cnca], cnca.Sbias[ind_cnca], cnca.Srbias[ind_cnca]]
for i, vi in enumerate(v):
    tmp = []
    for ai in vi:
        tmp = ai
        tmp = np.append(tmp, tmp)

    print(vname[i], np.nanmean(tmp))

#  ──────────────────────────────────────────────────────────────────────────

print()
print('mld d', np.nanmean(cnam.bias[0][0][ind_cnam]))
# print('mld d r', np.nanmean(cnam.rbias[0][0][ind_cnam]))
print('mld t', np.nanmean(cnam.bias[0][1][ind_cnam]))
# print('mld t r', np.nanmean(cnam.rbias[0][1][ind_cnam]))

#  ──────────────────────────────────────────────────────────────────────────

lat, lon = np.meshgrid(cwq.lat, cwq.lon)

lat = lat.flatten()
lon = lon.flatten()

ind_cwq = []
for i, _ in enumerate(lat):
    if 4.25 <= lon[i] <= 5 and 42 <= lat[i] <= 42.5:
        ind_cwq.append(i)

#  ──────────────────────────────────────────────────────────────────────────
# temporal relative bias

rbias = {}
rrmse = {}
rq95 = {}
for i in ['ws', 'wd', 'u', 'v']:
    rbias[i] = np.nanmean(cwq.wrforch[i][:, ind_cwq] - cwq.quikscat[i][:, ind_cwq])/np.nanmean(cwq.quikscat[i][:, ind_cwq])
    rq95[i] = (np.nanquantile(cwq.wrforch[i][:, ind_cwq], .95) - np.nanquantile(cwq.quikscat[i][:, ind_cwq], .95))/np.nanquantile(cwq.quikscat[i][:, ind_cwq], .95)
    
    rrmse[i] = []
    for j in range(cwq.wrforch[i][:,ind_cwq].shape[1]):
        tmp = np.nanmean(((cwq.wrforch[i][:, ind_cwq[j]] - cwq.quikscat[i][:, ind_cwq[j]])**2)**.5, axis=0)/np.nanmean(cwq.quikscat[i], axis=0)[ind_cwq[j]]
        rrmse[i].append(tmp)
    rrmse[i] = np.array(rrmse[i])

print()
print('wind speed')
print('rbias', rbias['ws'])
print('rrmse', np.mean(rrmse['ws']))
print('rq95', rq95['ws'])

#  ──────────────────────────────────────────────────────────────────────────
# temporal correlation coefficient

corr = {}
for i in ['ws', 'wd', 'u', 'v']:
    corr[i] = []
    for j in range(cwq.wrforch[i][:,ind_cwq].shape[1]):
        tmp = ma.corrcoef(ma.masked_invalid(cwq.wrforch[i][:, ind_cwq[j]]), ma.masked_invalid(cwq.quikscat[i][:, ind_cwq[j]]))
        tmp = tmp[0][1]
        
        if ma.is_masked(tmp):
            corr[i].append(np.nan)
        else:
            corr[i].append(tmp)
    corr[i] = np.nanmean(corr[i])

print('corr', corr['ws'])
