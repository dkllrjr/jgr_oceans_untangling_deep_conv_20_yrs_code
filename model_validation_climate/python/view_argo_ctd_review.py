import pickle
import numpy as np
import matplotlib.pyplot as plt

# ──────────────────────────────────────────────────────────────────────────

with open('../data/argo.pickle', 'rb') as file:
    data = pickle.load(file)

#  ──────────────────────────────────────────────────────────────────────────
# area of interest: lon = 1, 9 E, lat = 39, 45
# ──────────────────────────────────────────────────────────────────────────

import xarray as xr
from glob import glob
from datetime import datetime
import numpy as np
import pickle
import gsw

# ──────────────────────────────────────────────────────────────────────────

host_data = '../../../host_data/phd/argo/'
argo_paths = glob(host_data + '*/*.nc')
argo_paths.sort()

host_data = '../../../host_data/phd/ctd/'
ctd_paths = glob(host_data + '*/*.nc') 
ctd_paths.sort()

paths = argo_paths + ctd_paths

# ──────────────────────────────────────────────────────────────────────────

count = 0
count_in_bounds = 0
count_in_time = 0

beg = datetime(1993, 7, 1, 0)
end = datetime(2013, 7, 1, 0)

attrs = []

# TEMP, PSAL, PRES
for path in paths:
    datai = xr.open_dataset(path)
    ti = datai.JULD.values
    var_list = list(datai.variables)
    # print(ti, datai.attrs)
    attrs.append(datai.attrs)

    for j, tj in enumerate(ti):
        count += 1
        if np.datetime64(beg) <= tj < np.datetime64(end):  # selecting proper time

            count_in_time += 1

            latj = datai.LATITUDE[j].values
            lonj = datai.LONGITUDE[j].values

            if 39 <= latj <= 45 and 1 <= lonj <= 9:  # separating NW MED
                count_in_bounds += 1

institutions = set()
for i, attri in enumerate(attrs):
    if len(attri['institution']) > 0:
        institutions.add(attri['institution'])
