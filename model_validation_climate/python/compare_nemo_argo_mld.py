import pickle
import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error

# ──────────────────────────────────────────────────────────────────────────

with open('../data/nemo_mld.pickle', 'rb') as file:
    nemo_data = pickle.load(file)

with open('../data/argo_mld.pickle', 'rb') as file:
    argo_data = pickle.load(file)

# ──────────────────────────────────────────────────────────────────────────
# compiling observational data`

lats = np.array(argo_data['lat'])
lons = np.array(argo_data['lon'])
time = np.array(argo_data['t'])

amld = [] # density 1, temperature 2
amld.append(np.array(argo_data['da_mld']))
amld.append(np.array(argo_data['ta_mld']))

# ──────────────────────────────────────────────────────────────────────────
# compiling nemo data

nmld = [] # turbocline 1, mixing 2
nmld.append(np.array(nemo_data['turbocline']))
nmld.append(np.array(nemo_data['mixing']))

# ──────────────────────────────────────────────────────────────────────────
# comparing data

# bias will be model deviation from observations, so model - obs
# relative bias will be (model - obs)/obs

bias = []
rbias = []
for i, amldi in enumerate(amld):
    bias.append([])
    rbias.append([])
    for j, nmldj in enumerate(nmld):
        bias[i].append([])
        rbias[i].append([])
        for t, _ in enumerate(amldi):
            bias[i][j].append(nmldj[t]-amldi[t])
            rbias[i][j].append((nmldj[t]-amldi[t])/amldi[t])
        bias[i][j] = np.array(bias[i][j])
        rbias[i][j] = np.array(rbias[i][j])

#  ──────────────────────────────────────────────────────────────────────────

dates = []
for i, amldi in enumerate(amld):
    dates.append([])
    for j, nmldj in enumerate(nmld):
        dates[i].append([])
        tmp_ind = np.where(bias[i][j] > 100, True, False)
        tmp_dates = time[tmp_ind]
        new_dates = []
        for k in tmp_dates:
            new_dates.append((pd.Timestamp(k).year, pd.Timestamp(k).month))

        dates[i][j].append(new_dates)
        dates[i][j] = np.array(dates[i][j])

date_set = []
for i, amldi in enumerate(amld):
    date_set.append([])
    for j, nmldj in enumerate(nmld):
        date_set[i].append([])
        date_set[i][j] = (set(dates[i][j][0][:, 0]), set(dates[i][j][0][:, 1]))

#  ──────────────────────────────────────────────────────────────────────────

ms = time.astype('datetime64[M]').astype(int) % 12 + 1
monthly_m = [[], []]
monthly_std = [[], []]
for m in np.arange(12):
    ind = np.where(ms == m + 1, True, False)
    for i in range(len(bias[0])):
        monthly_m[i].append(np.mean(bias[0][i][ind]))
        monthly_std[i].append(np.std(bias[0][i][ind]))

monthly_m = np.array(monthly_m)
monthly_std = np.array(monthly_std)
