import xarray as xr
from glob import glob
import numpy as np
import pickle
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib.patches import Rectangle
from matplotlib import colors as mcolors
import matplotlib.tri as tri

# import compare_wrforch_quikscat as cwq

#  ──────────────────────────────────────────────────────────────────────────

with open('../data/comp_wrf_quik.pkl', 'rb') as file:
    data = pickle.load(file)

#  ──────────────────────────────────────────────────────────────────────────

def gol_bias_plot(i, ax, lats, lons, bias, var_min, var_max, levels, cmap):

    fsize = 28
    tsize = 34
        
    europe_land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white') 
    
    gl = ax.gridlines(crs=ccrs.PlateCarree(),draw_labels=True,linewidth=.75, color='black', alpha=0.35, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-90,2,5,8,90])
    gl.ylocator = mticker.FixedLocator([0,40,42,44,60])
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': fsize}
    gl.ylabel_style = {'size': fsize}
    gl.xpadding = 10
    gl.ypadding = 10

    if i in [0, 1]:
        gl.xlabels_bottom = False

    if i in [1, 3]:
        gl.ylabels_left = False
    
    ax.set_extent([1,9,39,45], crs=ccrs.PlateCarree());

    ax.add_feature(europe_land_10m)

    x, y = 4.25, 42
    rect = Rectangle((x, y), 5-x, 42.5-y, fill=False, linewidth=2, edgecolor='black', transform=ccrs.PlateCarree(), label='Spatial Ave.', zorder=3)
    ax.add_patch(rect)

    # gol_map = ax.contourf(lons, lats, bias, transform=ccrs.PlateCarree(), vmin=var_min, vmax=var_max, levels=levels, extend='both', cmap=cmap)  # plotting filled contours of the data
    # gol_map = ax.pcolormesh(lons, lats, bias, transform=ccrs.PlateCarree(), vmin=var_min, vmax=var_max, cmap=cmap)  # plotting filled contours of the data
    # gol_map = ax.tricontourf(lons[~np.isnan(bias)], lats[~np.isnan(bias)], bias[~np.isnan(bias)], transform=ccrs.PlateCarree(), vmin=var_min, vmax=var_max, levels=levels, extend='both', cmap=cmap)  # plotting filled contours of the data
    gol_map = ax.tricontourf(lons[~np.isnan(bias)], lats[~np.isnan(bias)], bias[~np.isnan(bias)], transform=ccrs.PlateCarree(), vmin=var_min, vmax=var_max, levels=levels, extend='both', cmap=cmap)  # plotting filled contours of the data

    # with open('../data/nemo_lon_lats.pickle', 'rb') as file:
        # lon_lats = pickle.load(file)

    # triang = tri.Triangulation(lons, lats)
    # tri_interp = tri.LinearTriInterpolator(triang, bias)
    # bias_interp = tri_interp(lon_lats[:,0], lon_lats[:,1])

    # triang = tri.Triangulation(lon_lats[:,0][~bias_interp.mask], lon_lats[:,1][~bias_interp.mask])

    # gol_map = ax.tricontourf(triang, bias_interp[~bias_interp.mask], transform=ccrs.PlateCarree(), vmin=var_min, vmax=var_max, levels=levels, extend='both', cmap=cmap)  # plotting filled contours of the data

    return ax, gol_map
    
#  ──────────────────────────────────────────────────────────────────────────

fsize = 28
tsize = 34
pad = 10

# ──────────────────────────────────────────────────────────────────────────
# q-q plot

plot_path = '../plots/wrforch_quikscat_qq.pdf'

q_wrf = data['qq']['wrforch']['ws']
q_quik = data['qq']['quikscat']['ws']

fig, ax = plt.subplots(1, 1, figsize=(12, 9), dpi=400)

line = np.linspace(0, q_quik.max(), len(q_quik))

ax.plot(line, line, ls='--', c='k')
ax.plot(q_quik, q_wrf, c='tab:blue')

ax.grid()

ax.tick_params(axis='both', labelsize=fsize-4)

ax.set_xlabel('QuikSCAT $m/s$', fontsize=fsize)
ax.set_ylabel('WRF-ORCHIDEE $m/s$', fontsize=fsize)

ax.set_xlim(line[0], line[-1])
ax.set_ylim(line[0], line[-1])

ax.set_title('Q-Q Wind Speed', fontsize=tsize, pad=20)

fig.tight_layout()
fig.savefig(plot_path)

#  ──────────────────────────────────────────────────────────────────────────

levels = 200
lat = data['lat']
lon = data['lon']

lat, lon = np.meshgrid(lat, lon)
lat = lat.transpose()
lon = lon.transpose()

rbias = data['relative']['bias']['ws']
rrmse = data['relative']['rmse']['ws']
rq95 = data['relative']['q95']['ws']
corr = data['corr']['ws']

rbias = rbias.reshape(lat.shape)
rrmse = rrmse.reshape(lat.shape)
rq95 = rq95.reshape(lat.shape)
corr = corr.reshape(lat.shape)

plot_path = '../plots/wrforch_quikscat_wind'

fig, ax = plt.subplots(2, 2, figsize=(19, 13), dpi=400, subplot_kw={'projection': ccrs.PlateCarree(5)})
ax = ax.flatten()

# setting up the fig for the plot colorbar
fig.subplots_adjust(top=0.97, bottom=0.03, left=0.08, right=0.93, wspace=0.23, hspace=0.06)

# bias
# var_min = -0.44
# var_max = 0.44
# var_min = -2
# var_max = 0
var_min = -2
var_max = -var_min

cmap = plt.cm.coolwarm
ax[0], gol_map1 = gol_bias_plot(0, ax[0], lat, lon, rbias, var_min, var_max, levels, cmap=cmap)

ax[0].set_title('Bias', fontsize=fsize, pad=pad)

caxx = 0.4667
caxy = 0.546
cax = fig.add_axes([caxx, caxy, 0.02, 0.4])
norm = mcolors.Normalize(vmin=var_min, vmax=var_max)

cbar = plt.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), extend='both', orientation='vertical', cax=cax, pad=pad)
cbar.ax.tick_params(labelsize=fsize - 4)

# rmse
# var_min = 0
# var_max = 0.4

var_min = 0
var_max = 2

cmap = plt.cm.viridis
ax[1], gol_map = gol_bias_plot(1, ax[1], lat, lon, rrmse, var_min, var_max, levels=levels, cmap=cmap)

ax[1].set_title('RMSE', fontsize=fsize, pad=pad)

caxxx = 0.938
cax = fig.add_axes([caxxx, caxy, 0.02, 0.4])
norm = mcolors.Normalize(vmin=var_min, vmax=var_max)

cbar = plt.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), extend='both', orientation='vertical', cax=cax, pad=pad)
cbar.ax.tick_params(labelsize=fsize - 4)

# rq95
# var_min = -0.24
# var_max = 0.24
var_min = -4
var_max = -var_min

cmap = plt.cm.coolwarm
ax[2], gol_map = gol_bias_plot(2, ax[2], lat, lon, rq95, var_min, var_max, levels=levels, cmap=cmap)

ax[2].set_title('95th Quantile', fontsize=fsize, pad=pad)

caxyy = 0.06
cax = fig.add_axes([caxx, caxyy, 0.02, 0.4])
norm = mcolors.Normalize(vmin=var_min, vmax=var_max)

cbar = plt.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), extend='both', orientation='vertical', cax=cax, pad=pad)
cbar.ax.tick_params(labelsize=fsize - 4)

# corr
var_min = 0.5
var_max = 1

cmap = plt.cm.viridis
ax[3], gol_map = gol_bias_plot(3, ax[3], lat, lon, corr, var_min, var_max, levels=levels, cmap=cmap)

ax[3].set_title('Correlation', fontsize=fsize, pad=pad)

cax = fig.add_axes([caxxx, caxyy, 0.02, 0.4])
norm = mcolors.Normalize(vmin=var_min, vmax=var_max)

cbar = plt.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), extend='both', orientation='vertical', cax=cax, pad=pad)
cbar.ax.tick_params(labelsize=fsize - 4)

ax[0].legend(loc='upper right', fontsize=fsize-4)

x1, x2 = 0.085, 0.56
y1, y2 = 0.9, 0.42
fig.text(x1, y1, '(a)', fontweight='bold', fontsize=fsize)
fig.text(x2, y1, '(b)', fontweight='bold', fontsize=fsize)
fig.text(x1, y2, '(c)', fontweight='bold', fontsize=fsize)
fig.text(x2, y2, '(d)', fontweight='bold', fontsize=fsize)

fig.savefig(plot_path + '.png')
fig.savefig(plot_path + '.pdf')
plt.close()

#  ──────────────────────────────────────────────────────────────────────────

ws = data['relative']['mean']['ws']
ws = ws.reshape(lat.shape)

plot_path = '../plots/wrforch_quikscat_wind_speed_only'

fig, ax = plt.subplots(1, 1, figsize=(19, 13), dpi=400, subplot_kw={'projection': ccrs.PlateCarree(5)})

# setting up the fig for the plot colorbar
fig.subplots_adjust(top=0.97, bottom=0.03, left=0.08, right=0.93, wspace=0.23, hspace=0.06)

# wind speed
var_min = 0
var_max = 9

cmap = plt.cm.coolwarm
ax, gol_map1 = gol_bias_plot(0, ax, lat, lon, ws, var_min, var_max, levels, cmap=cmap)

ax.set_title('Wind Speed', fontsize=fsize, pad=pad)

caxx = 0.4667
caxy = 0.546
cax = fig.add_axes([caxx, caxy, 0.02, 0.4])
norm = mcolors.Normalize(vmin=var_min, vmax=var_max)

cbar = plt.colorbar(plt.cm.ScalarMappable(norm=norm, cmap=cmap), extend='both', orientation='vertical', cax=cax, pad=pad)
cbar.ax.tick_params(labelsize=fsize - 4)

fig.savefig(plot_path + '.png')
plt.close()
