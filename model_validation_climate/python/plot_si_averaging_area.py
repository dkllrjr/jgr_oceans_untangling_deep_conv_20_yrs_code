import xarray as xr
from glob import glob
import numpy as np
import pickle
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib.patches import Rectangle

# ──────────────────────────────────────────────────────────────────────────

with open('../data/argo.pickle', 'rb') as file:
    argo_data = pickle.load(file)

with open('../data/ctd.pickle', 'rb') as file:
    ctd_data = pickle.load(file)

lats = np.append(argo_data['lat'], ctd_data['lat'], axis=0)
lons = np.append(argo_data['lon'], ctd_data['lon'], axis=0)

argo_ctd = np.arange(len(lats))
argo = np.where(argo_ctd < len(argo_data['t']), True, False)
ctd = np.invert(argo)

# ──────────────────────────────────────────────────────────────────────────

def gol_si_plot(lats, lons, argo, ctd, plot_path):

    fig, ax = plt.subplots(1, 1, figsize=(15, 12), dpi=400, subplot_kw={'projection': ccrs.PlateCarree(5)})
    fsize = 28
    tsize = 34
        
    europe_land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white') 
    
    gl = ax.gridlines(crs=ccrs.PlateCarree(),draw_labels=True,linewidth=.75, color='black', alpha=0.35, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-90,2,5,8,90])
    gl.ylocator = mticker.FixedLocator([0,40,42,44,60])
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': fsize}
    gl.ylabel_style = {'size': fsize}
    gl.xpadding = 30
    gl.ypadding = 30
    
    ax.set_extent([1,9,39,45], crs=ccrs.PlateCarree());

    ax.add_feature(europe_land_10m)

    x, y = 4.25, 42
    rect = Rectangle((x, y), 5-x, 42.5-y, fill=False, linewidth=2, edgecolor='black', transform=ccrs.PlateCarree(), label='Spatial Ave.', zorder=3)
    ax.add_patch(rect)

    ms = 3
    ax.plot(lons[argo], lats[argo], marker='o', color='tab:blue', markersize=ms, ls='', transform=ccrs.PlateCarree())
    ax.plot(lons[ctd], lats[ctd], marker='o', color='tab:red', markersize=ms, ls='', transform=ccrs.PlateCarree())

    msbig = 10
    ax.plot(np.nan, np.nan, marker='o', color='tab:red', ms=msbig, ls='', label='ARGO')
    ax.plot(np.nan, np.nan, marker='o', color='tab:blue', ms=msbig, ls='', label='CTD')

    ax.set_title('NW Mediterranean Sea', pad=35, fontsize=tsize+4)
    ax.legend(fontsize=fsize)
    
    fig.subplots_adjust(bottom=0.1, right=0.98)
    fig.savefig(plot_path)
    plt.close()

# ──────────────────────────────────────────────────────────────────────────

print('plotting')

gol_si_plot(lats, lons, argo, ctd, '../plots/si_averaging_area_ctd_argo.pdf')
