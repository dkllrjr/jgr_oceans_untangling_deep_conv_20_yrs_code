import matplotlib.pyplot as plt
import numpy as np

import compare_nemo_ctd_argo as cnca

#  ──────────────────────────────────────────────────────────────────────────

fsize = 20
fig, ax = plt.subplots(2, 1, figsize=(12, 10), dpi=400)

alpha = 0.2

ax[0].fill_between(cnca.months, cnca.Tmonths_m + cnca.Tmonths_std, cnca.Tmonths_m - cnca.Tmonths_std, color='tab:red', alpha=alpha, label='Std')
ax[0].plot(cnca.months, cnca.Tmonths_m, color='tab:red', label='Mean')

ax[1].fill_between(cnca.months, cnca.Smonths_m + cnca.Smonths_std, cnca.Smonths_m - cnca.Smonths_std, color='tab:red', alpha=alpha)
ax[1].plot(cnca.months, cnca.Smonths_m, color='tab:red')

for axi in ax:
    axi.set_xlim(cnca.months[0], cnca.months[-1])
    axi.set_xticks(cnca.months, cnca.months)
    axi.tick_params(axis='both', labelsize=fsize-2)
    axi.grid()
    axi.axhline(0, color='k')

ax[0].legend(ncol=2, fontsize=fsize-4)
ax[0].set_ylabel('Bias $^\\circ C$', fontsize=fsize)
ax[1].set_ylabel('Bias $PSU$', fontsize=fsize)

ax[-1].set_xlabel('Months', fontsize=fsize)

fig.tight_layout()
fig.savefig('../plots/temporal_argo_ctd_bias.png')

