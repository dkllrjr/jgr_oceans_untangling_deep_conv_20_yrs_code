import xarray as xr
import pickle
import pandas as pd
from datetime import datetime, timedelta
import numpy as np

#  ──────────────────────────────────────────────────────────────────────────

data = xr.open_dataset('../../../host_data/phd/Argo_mixedlayers_all_04142022.nc')

#  ──────────────────────────────────────────────────────────────────────────

da_mld = data.da_mld
ta_mld = data.ta_mld
time = data.profiledate
lat = data.profilelat.values
lon = data.profilelon.values

epoch = np.datetime64('0000-01-01T00:00', 'D')

#  ──────────────────────────────────────────────────────────────────────────

lat_bound = [39, 45]
lon_bound = [1, 9]

#  ──────────────────────────────────────────────────────────────────────────

ind = []
for i, li in enumerate(lat):
    if lat_bound[0] <= li <= lat_bound[1]:
        if lon_bound[0] <= lon[i] <= lon_bound[1]:
            ind.append(i)

da_mld = da_mld[ind].values
ta_mld = ta_mld[ind].values
time = time[ind].values
lat = lat[ind]
lon = lon[ind]

#  ──────────────────────────────────────────────────────────────────────────

t = []
for ti in time:
    t.append(epoch + np.timedelta64(timedelta(days=ti-1)))

t = np.array(t)

#  ──────────────────────────────────────────────────────────────────────────

ind = []
for i, ti in enumerate(t):
    if np.datetime64('1993-06-01T00:00') <= ti < np.datetime64('2013-06-01T00:00'):
        ind.append(i)

da_mld = da_mld[ind]
ta_mld = ta_mld[ind]
t = t[ind]
lat = lat[ind]
lon = lon[ind]

#  ──────────────────────────────────────────────────────────────────────────

argo_mld = {'da_mld': da_mld, 'ta_mld': ta_mld, 't': t, 'lat': lat, 'lon': lon}

with open('../data/argo_mld.pickle', 'wb') as file:
    pickle.dump(argo_mld, file)
