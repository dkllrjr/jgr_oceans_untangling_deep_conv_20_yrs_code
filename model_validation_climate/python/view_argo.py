import pickle
import numpy as np
import matplotlib.pyplot as plt

# ──────────────────────────────────────────────────────────────────────────

with open('../data/argo.pickle', 'rb') as file:
    data = pickle.load(file)

# ──────────────────────────────────────────────────────────────────────────

# lats = [41, 43]
# lons = [3.5, 6.5]

# lats = [41, 42.5]
# lons = [4, 7]

# ndata = {}
# ind = []
# for ti, _ in enumerate(data['t']):
    # if lats[0] < data['lat'][ti] < lats[1]:
        # if lons[0] < data['lon'][ti] < lons[1]:
            # ind.append(ti)

# for key in data.keys():
    # ndata[key] = data[key][ind]

# data = ndata

# ──────────────────────────────────────────────────────────────────────────

mawi = []
liwi = []
wmdwi = []

for ti, _ in enumerate(data['t']):
    mawi.append(np.where(-data['d'][ti] <= 150, True, False))
    liwi.append(np.where((150 < -data['d'][ti]) & (-data['d'][ti] <= 600), True, False))
    wmdwi.append(np.where(600 < -data['d'][ti], True, False))

# ──────────────────────────────────────────────────────────────────────────

mawT = np.array([])
liwT = np.array([])
wmdwT = np.array([])

mawS = np.array([])
liwS = np.array([])
wmdwS = np.array([])

for ti, _ in enumerate(data['t']):
    mawT = np.append(mawT, data['T'][ti][mawi[ti]])
    liwT = np.append(liwT, data['T'][ti][liwi[ti]])
    wmdwT = np.append(wmdwT, data['T'][ti][wmdwi[ti]])

    mawS = np.append(mawS, data['S'][ti][mawi[ti]])
    liwS = np.append(liwS, data['S'][ti][liwi[ti]])
    wmdwS = np.append(wmdwS, data['S'][ti][wmdwi[ti]])


print('Temperature')
print("maw", np.nanmean(mawT), np.nanstd(mawT))
print("liw", np.nanmean(liwT), np.nanstd(liwT))
print("wmdw", np.nanmedian(wmdwT), np.nanstd(wmdwT))
print()
print('Salinity')
print("maw", np.nanmean(mawS), np.nanstd(mawS))
print("liw", np.nanmean(liwS), np.nanstd(liwS))
print("wmdw", np.nanmean(wmdwS), np.nanstd(wmdwS))

print(data['t'][0], data['t'][-1])

# ──────────────────────────────────────────────────────────────────────────

# bins = 100
# plt.hist(mawT, bins=bins)
# plt.show()

# plt.hist(liwT, bins=bins)
# plt.show()

# plt.hist(wmdwT, bins=bins)
# plt.show()

# for ti, _ in enumerate(data['t']):
    # plt.plot(data['T'][ti], data['d'][ti])
    # plt.show()

#  ──────────────────────────────────────────────────────────────────────────

dt = 0
du = 0
dl = 0
for ti, _ in enumerate(data['t']):
    if np.nanmax(-data['d'][ti]) > 600:
        dt += 1
        du += 1
        dl += 1
    elif np.nanmax(-data['d'][ti]) > 150:
        dt += 1
        du += 1
    else:
        dt += 1

print(dt, du, dl)
