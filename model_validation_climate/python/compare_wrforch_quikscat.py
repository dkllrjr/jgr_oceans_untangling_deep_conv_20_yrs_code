import pickle
import numpy as np
from numpy import ma
from sklearn.metrics import mean_squared_error
from scipy.interpolate import griddata, RectSphereBivariateSpline, interpn, RegularGridInterpolator

#  ──────────────────────────────────────────────────────────────────────────

with open('../data/gol_wrforch_quikscat.pkl', 'rb') as file:
    data = pickle.load(file)

#  ──────────────────────────────────────────────────────────────────────────

t = data['t']

wrf_lat = data['wrforch']['lat']
wrf_lon = data['wrforch']['lon']

wrf_lat_lon = list(zip(wrf_lat, wrf_lon))

quik_lat = data['quikscat']['lat']
quik_lon = data['quikscat']['lon']

quik_lon_mesh, quik_lat_mesh = np.meshgrid(quik_lon, quik_lat)
quik_lat_lon = list(zip(quik_lat_mesh.flatten(), quik_lon_mesh.flatten()))

wrforch = data['wrforch']
quikscat = data['quikscat']

wrforch['u'] = np.array(wrforch['u'])
wrforch['v'] = np.array(wrforch['v'])
quikscat['u'] = np.array(quikscat['u'])
quikscat['v'] = np.array(quikscat['v'])

wrforch['ws'] = (wrforch['u']**2 + wrforch['v']**2)**.5
quikscat['ws'] = (quikscat['u']**2 + quikscat['v']**2)**.5

wrforch['wd'] = np.arctan2(wrforch['v'], wrforch['u'])
quikscat['wd'] = np.arctan2(quikscat['v'], quikscat['u'])

# N = 0.125
N = 0.25
lat = np.arange(quik_lat[0], quik_lat[-1] - N, -N) 
lon = np.arange(quik_lon[0], quik_lon[-1] + N, N) 
lon_mesh, lat_mesh = np.meshgrid(lon, lat)
lat_lon = list(zip(lat_mesh.flatten(), lon_mesh.flatten()))

lat = quik_lat
lon = quik_lon
lat_lon = quik_lat_lon

#  ──────────────────────────────────────────────────────────────────────────

# var_list = ['u', 'v', 'ws', 'wd']
var_list = ['ws']

print('interpolate')

# for key in var_list:
    # print(key)
    # tmp = []
    # for qi in quikscat[key]:

        # interp = RegularGridInterpolator((quik_lat, quik_lon), qi.reshape(len(quik_lat), len(quik_lon)))
        # tmpi = interp((lat_mesh, lon_mesh))
        # tmp.append(tmpi.flatten())

    # quikscat[key] = np.array(tmp)

for key in var_list:
    tmp = []
    for wi in wrforch[key]:
        tmpi = griddata(wrf_lat_lon, wi, lat_lon, method='linear')
        tmp.append(tmpi)

    wrforch[key] = np.array(tmp)

#  ──────────────────────────────────────────────────────────────────────────
# temporal relative bias

print('bias')
rbias = {}
rrmse = {}
rq95 = {}
mean = {}
for i in var_list:
    # rbias[i] = np.nanmean(wrforch[i] - quikscat[i], axis=0)/np.nanmean(quikscat[i], axis=0)
    # rq95[i] = (np.nanquantile(wrforch[i], .95, axis=0) - np.nanquantile(quikscat[i], .95, axis=0))/np.nanquantile(quikscat[i], .95, axis=0)
    
    # rrmse[i] = []
    # for j in range(wrforch[i].shape[1]):
        # tmp = np.nanmean(((wrforch[i][:, j] - quikscat[i][:, j])**2)**.5, axis=0)/np.nanmean(quikscat[i], axis=0)[j]
        # rrmse[i].append(tmp)
    # rrmse[i] = np.array(rrmse[i])
    # mean[i] = np.nanmean(quikscat[i], axis=0)

    # regular bias => removed normalizing factor
    rbias[i] = np.nanmean(wrforch[i] - quikscat[i], axis=0)
    rq95[i] = (np.nanquantile(wrforch[i], .95, axis=0) - np.nanquantile(quikscat[i], .95, axis=0))
    
    rrmse[i] = []
    for j in range(wrforch[i].shape[1]):
        tmp = np.nanmean(((wrforch[i][:, j] - quikscat[i][:, j])**2)**.5, axis=0)
        rrmse[i].append(tmp)
    rrmse[i] = np.array(rrmse[i])
    mean[i] = np.nanmean(quikscat[i], axis=0)

r = {'bias': rbias, 'rmse': rrmse, 'q95': rq95, 'mean': mean}
for key in r.keys():
    print(key, np.nanmean(r[key]['ws']))

#  ──────────────────────────────────────────────────────────────────────────
# temporal correlation coefficient

print('correlations')
corr = {}
for i in var_list:
    corr[i] = []
    for j in range(wrforch[i].shape[1]):
        tmp = ma.corrcoef(ma.masked_invalid(wrforch[i][:, j]), ma.masked_invalid(quikscat[i][:, j]))
        tmp = tmp[0][1]
        
        if ma.is_masked(tmp):
            corr[i].append(np.nan)
        else:
            corr[i].append(tmp)
    corr[i] = np.array(corr[i])

#  ──────────────────────────────────────────────────────────────────────────
# domain

print('domain')
m = {}
b = {}
rmse = {}
std = {}
q95 = {}
bq95 = {}
for i in var_list:
    m[i] = np.nanmean(quikscat[i])
    b[i] = np.nanmean(wrforch[i] - quikscat[i])
    rmse[i] = np.nanmean(np.square(wrforch[i] - quikscat[i]))**.5
    std[i] = np.nanstd(quikscat[i])
    q95[i] = np.nanquantile(quikscat[i], .95)
    bq95[i] = np.nanquantile(wrforch[i], .95) - q95[i]

mm = {'m': m, 'bias': b, 'rmse': rmse, 'std': std, 'q95': q95, 'bq95': bq95}

#  ──────────────────────────────────────────────────────────────────────────
# domain quantiles

print('quantiles')
qs = np.linspace(0, 1, 100)
wrforch_q = {}
quikscat_q = {}
for i in var_list:
    wrforch_q[i] = np.zeros_like(qs)
    quikscat_q[i] = np.zeros_like(qs)

    for j, qi in enumerate(qs):
        wrforch_q[i][j] = np.nanquantile(wrforch[i], qi)
        quikscat_q[i][j] = np.nanquantile(quikscat[i], qi)

qq = {'wrforch': wrforch_q, 'quikscat': quikscat_q, 'q': qs}

#  ──────────────────────────────────────────────────────────────────────────
# save

save_data = {'relative': r, 'corr': corr, 'mean': mm, 'qq': qq, 'time': t, 'lat': lat, 'lon': lon}
with open('../data/comp_wrf_quik.pkl', 'wb') as file:
    pickle.dump(save_data, file)

print()
