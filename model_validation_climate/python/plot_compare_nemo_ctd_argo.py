import matplotlib.pyplot as plt
import numpy as np

import compare_nemo_ctd_argo as cnca

# ──────────────────────────────────────────────────────────────────────────

fsize = 18

alpha = 0.5

fig, ax = plt.subplots(3, 2, figsize=(10, 15), dpi=400)
ax = ax.flatten()

ind = np.invert(np.isnan(cnca.Tv))

# T
ax[0].plot(cnca.Tv, cnca.vert_bins, color='tab:red', label='Mean')
ax[0].fill_betweenx(cnca.vert_bins, x1=cnca.Tv + cnca.Tv_std, x2=cnca.Tv - cnca.Tv_std, alpha=alpha, color='tab:red', label='Std')

# ax[2].plot(cnca.Trv, cnca.vert_bins, color='tab:red', label='')
# ax[2].fill_betweenx(cnca.vert_bins, x1=cnca.Trv + cnca.Trv_std, x2=cnca.Trv - cnca.Trv_std, alpha=alpha, color='tab:red', label='')

ax[2].plot(cnca.Trmse, cnca.vert_bins, color='tab:blue', label='')

# S
ax[1].plot(cnca.Sv, cnca.vert_bins, color='tab:red', label='')
ax[1].fill_betweenx(cnca.vert_bins, x1=cnca.Sv + cnca.Sv_std, x2=cnca.Sv - cnca.Sv_std, alpha=alpha, color='tab:red', label='')

# ax[3].plot(cnca.Srv, cnca.vert_bins, color='tab:red', label='')
# ax[3].fill_betweenx(cnca.vert_bins, x1=cnca.Srv + cnca.Srv_std, x2=cnca.Srv - cnca.Srv_std, alpha=alpha, color='tab:red', label='')

ax[3].plot(cnca.Srmse, cnca.vert_bins, color='tab:blue', label='')

# density
ax[4].plot(cnca.dTv, cnca.vert_bins, color='tab:red', label='Mean')
ax[4].fill_betweenx(cnca.vert_bins, x1=cnca.dTv + cnca.dTv_std, x2=cnca.dTv - cnca.dTv_std, alpha=alpha, color='tab:red', label='Std')

ax[5].plot(cnca.dSv, cnca.vert_bins, color='tab:red', label='')
ax[5].fill_betweenx(cnca.vert_bins, x1=cnca.dSv + cnca.dSv_std, x2=cnca.dSv - cnca.dSv_std, alpha=alpha, color='tab:red', label='')

ytemp = ax[1].get_yticks()
ax[1].set_yticks(ytemp, '')
ax[3].set_yticks(ytemp, '')

for axi in ax:
    axi.set_ylim(cnca.vert_bins[ind][0], cnca.vert_bins[ind][-1])
    axi.invert_yaxis()
    axi.tick_params(which='both', labelsize=fsize-4)
    axi.axvline(0, color='k', ls='-')
    axi.grid(alpha=0.5)

ax[0].set_title('Potential Temperature', fontsize=fsize+4, pad=18)
ax[1].set_title('Salinity', fontsize=fsize+4, pad=18)

ax[0].set_xlabel('Bias $^\\circ C$', fontsize=fsize)
ax[1].set_xlabel('Bias $PSU$', fontsize=fsize)

# ax[2].set_xlabel('% Relative Bias', fontsize=fsize)
# ax[3].set_xlabel('% Relative Bias', fontsize=fsize)

ax[2].set_xlabel('RMSE $^\\circ C$', fontsize=fsize)
ax[3].set_xlabel('RMSE $PSU$', fontsize=fsize)

ax[4].set_xlabel('Bias $\\rho_T$', fontsize=fsize)
ax[5].set_xlabel('Bias $\\rho_S$', fontsize=fsize)

px1, px2 = -.41, .35
ax[4].set_xlim(px1, px2)
ax[5].set_xlim(px1, px2)

ax[0].legend(loc='lower right', ncol=1, fontsize=fsize-4)

fig.supylabel('Depth', fontsize=fsize+4)

x1, x2 = 0.06, 0.53
y1, y2, y3 = 0.95, 0.635, 0.315
fig.text(x1, y1, '(a)', fontweight='bold', fontsize=fsize)
fig.text(x2, y1, '(b)', fontweight='bold', fontsize=fsize)
fig.text(x1, y2, '(c)', fontweight='bold', fontsize=fsize)
fig.text(x2, y2, '(d)', fontweight='bold', fontsize=fsize)
fig.text(x1, y3, '(e)', fontweight='bold', fontsize=fsize)
fig.text(x2, y3, '(f)', fontweight='bold', fontsize=fsize)

for axi in ax:
    axi.axhline(150, ls='--', c='k')
    axi.axhline(600, ls=':', c='k')

fig.tight_layout()

fig.savefig('../plots/argo_ctd_bias_vertical.pdf')
