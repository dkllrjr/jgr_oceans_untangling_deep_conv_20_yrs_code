import xarray as xr
from glob import glob
import numpy as np
import pickle
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
# from matplotlib.patches import Rectangle


##############################################################################

def gol_si_plot(si, plot_path):

    fig, ax = plt.subplots(1, 1, figsize=(15, 10), dpi=200, subplot_kw={'projection': ccrs.PlateCarree(5)})
    fsize = 28
    tsize = 34
        
    europe_land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white') 
    # ax.add_feature(europe_land_10m)
    
    gl = ax.gridlines(crs=ccrs.PlateCarree(),draw_labels=True,linewidth=.75, color='black', alpha=0.35, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-90,2,5,8,90])
    gl.ylocator = mticker.FixedLocator([0,40,42,44,60])
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': fsize}
    gl.ylabel_style = {'size': fsize}
    gl.xpadding = 30
    gl.ypadding = 30
    
    ax.set_extent([0,10,38,45], crs=ccrs.PlateCarree());

    ax.add_feature(europe_land_10m)
    
    si_min, si_max = 0, 3
    
    levels = np.round(np.linspace(si_min, si_max, 20), 2)

    # m = ax.contourf(si.nav_lon_grid_W, si.nav_lat_grid_W, si, transform=ccrs.PlateCarree(), extend='both', cmap='gist_rainbow_r')
    m = ax.contourf(si.nav_lon_grid_W, si.nav_lat_grid_W, si, transform=ccrs.PlateCarree(), vmin=si_min, vmax=si_max, levels=levels, extend='both', cmap='gist_rainbow_r')
    cbar = plt.colorbar(m, orientation='vertical')
    cbar.ax.tick_params(labelsize=fsize-4)
    ax.set_title(str(si.time_counter.values)[0:10], pad=20, fontsize=tsize)
    
    # fig.subplots_adjust(top = .85, wspace = .33, hspace = .3)
    fig.subplots_adjust(top = .96, left = 0.09, right = 1.04, bottom = 0.04)
    fig.savefig(plot_path)
    plt.close()


##############################################################################

data_path = '/bdd/MEDI/workspaces/dkeller/DWF_GOL_SENSITIVITY/NEMO/CONT_SEAS_CLIM/SI/'

data_paths = glob(data_path + 'N-C*')
data_paths.sort()

data = xr.open_mfdataset(data_paths, combine='by_coords')
data = data.__xarray_dataarray_variable__

print('loaded data')

##############################################################################

# plot_root_path = '/bdd/MEDI/workspaces/dkeller/DWF_GOL_SENSITIVITY/PLOTS/si_past/'
plot_root_path = '/bdd/MEDI/workspaces/dkeller/DWF_GOL_SENSITIVITY/PLOTS/si_past_prime_time/'

time = data.time_counter
# time = time[::30]

# time_ind = [0]
time_ind = []
cal = [31, 31, 30, 31, 30, 31, 31, 28, 31, 30, 31, 30]
leap = [1996, 2000, 2004, 2008, 2012]

year = 1994
i = 0
j = 0
while i < len(time):
    if j > 11:
        j = 0
        year += 1
    
    temp = cal
    if year in leap:
        temp[7] = 29
    else:
        temp[7] = 28

    i = i + temp[j]
    if i < len(time):
        # time_ind.append(i)

        if j in [5, 6, 7, 8]:
            k = 0
            while k < temp[j]:
                time_ind.append(i + k)
                k += 1

    j += 1

    # print(time[time_ind[-1]].values)
    # print(i, j, temp[7], year)

print('plotting')

for i, ind in enumerate(time_ind):
    print(time[ind].values)
    gol_si_plot(data[ind], plot_root_path + str(time[ind].values)[0:13])
