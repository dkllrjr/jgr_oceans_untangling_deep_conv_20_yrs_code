import xarray as xr
from glob import glob
import pickle
import numpy as np

###############################################################################

def find_lat_lon(loc, lat, lon):

    def archav(rad):
        return np.arccos(1 - 2*rad)
        
    def hav(rad):
        return (1 - np.cos(rad))/2

    def dist_sphere(r0, the0, phi0, r1, the1, phi1):
        return r0*archav(hav(np.abs(phi0-phi1)) + np.cos(phi0)*np.cos(phi1)*hav(np.abs(the0-the1)))

    def dist_sphere_deg(r0, the0, phi0, r1, the1, phi1):
        the0 = np.deg2rad(the0)
        phi0 = np.deg2rad(phi0)
        the1 = np.deg2rad(the1)
        phi1 = np.deg2rad(phi1)
        return dist_sphere(r0, the0, phi0, r1, the1, phi1)

    def lat_lon_near(lat, lon, loc):
        E_r = 6371000  # Earth's radius
        d = dist_sphere_deg(E_r, lon, lat, E_r, loc[1], loc[0])
        ind = np.unravel_index(d.argmin(), d.shape)
        return (lat[ind], lon[ind]), ind

    ind = []
    
    for i, loci in enumerate(loc):
        _, tmp = lat_lon_near(lat, lon, loci)
        ind.append(tmp)
    
    return ind 

###############################################################################

base_path = '/bdd/MEDI/workspaces/dkeller/DWF_GOL_1993-2013/NEMO/CONT_SEAS_CLIM/'

S_paths = glob(base_path + 'S/*CONT*.nc')
S_paths.sort()

T_paths = glob(base_path + 'T/*CONT*.nc')
T_paths.sort()

###############################################################################

with open('/bdd/MEDI/workspaces/dkeller/DWF_GOL_1993-2013/PICKLES/argo.pickle', 'rb') as file:
    argo_data = pickle.load(file)

with open('/bdd/MEDI/workspaces/dkeller/DWF_GOL_1993-2013/PICKLES/ctd.pickle', 'rb') as file:
    ctd_data = pickle.load(file)

###############################################################################

loc_data = xr.open_dataset(S_paths[0])
lat = loc_data.nav_lat.values
lon = loc_data.nav_lon.values

loc = []
for i, lati in enumerate(argo_data['lat']):
    loni = argo_data['lon'][i]
    loc.append([lati, loni])

for i, lati in enumerate(ctd_data['lat']):
    loni = ctd_data['lon'][i]
    loc.append([lati, loni])

ind = find_lat_lon(loc, lat, lon)

t = np.append(argo_data['t'], ctd_data['t'])

print('found locs')

###############################################################################

S_data = xr.open_mfdataset(S_paths)
T_data = xr.open_mfdataset(T_paths)

print('loaded nc data')

S_data = S_data.vosaline
T_data = T_data.votemper

dS = S_data.deptht.values
dT = T_data.deptht.values

S, T = [], []
tS, tT = [], []

wrong = []
for i, indi in enumerate(ind):
    print(i)
    S_temp = S_data.sel(time_counter=str(t[i]), method='nearest')

    if len(S_temp.shape) > 3:
        wrong.append(i)
        print('wrong')
        S.append(None)
        T.append(None)
        tS.append(None)
        tT.append(None)
    else:

        S_prof = S_temp[:, indi[0], indi[1]]
        tS.append(S_prof.time_counter.values)
        S.append(S_prof.values)

        T_temp = T_data.sel(time_counter=str(t[i]), method='nearest')
        T_prof = T_temp[:, indi[0], indi[1]]
        tT.append(T_prof.time_counter.values)
        T.append(T_prof.values)

print('found all profiles')
print('wrong', len(wrong))

###############################################################################

data = {'S': {'t': tS, 'd': dS, 'S': S}, 'T': {'t': tT, 'd': dT, 'T': T}, 'wrong': wrong}

with open('/bdd/MEDI/workspaces/dkeller/DWF_GOL_1993-2013/PICKLES/nemo.pickle', 'wb') as file:
    pickle.dump(data, file)
