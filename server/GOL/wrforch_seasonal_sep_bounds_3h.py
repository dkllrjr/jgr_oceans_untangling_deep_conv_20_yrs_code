import xarray as xr
from glob import glob
import numpy as np
import pickle
from dask.diagnostics import ProgressBar
import os
import multiprocessing as mp


##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/DWF_GOL_1993-2013/'

atmos_data_path = root_path + 'WRFORCH/SEASONAL_1990-2013/'
humidity_data_paths = glob(atmos_data_path + '*HUSS.nc')
T_data_paths = glob(atmos_data_path + '*TAS.nc')
u_data_paths = glob(atmos_data_path + '*UAS.nc')
v_data_paths = glob(atmos_data_path + '*VAS.nc')

humidity_data_paths.sort()
T_data_paths.sort()
u_data_paths.sort()
v_data_paths.sort()

atmos_data_paths = [humidity_data_paths, T_data_paths, u_data_paths, v_data_paths]

data_names = ['huss', 'tas', 'uas', 'vas']

print('loaded data')

##############################################################################

def find_ave(root_path, data_data_paths, data_name, i, ind):

    print(i)
    data = xr.open_dataset(data_data_paths[i], chunks='auto')
    
    tdata = data.Time.values
    print(i)
    
    data_names = [data_name]
    data_dict = {}
    print(i)

    for name in data_names:
        data_dict[name] = []
        for indi in ind:
            data_dict[name].append(data[name][:, indi[0], indi[1]])
    
        data_dict[name] = np.mean(data_dict[name], axis=0)
        print(name)

    data_dict['t'] = tdata

    with open(root_path + 'GOL/2D/SEAS_' + os.path.basename(data_data_paths[i])[0:-3] + '.pickle', 'wb') as file:
        pickle.dump(data_dict, file)


##############################################################################

# lat_bounds = [40.6, 42.5]
# lon_bounds = [4, 6.5]
lat_bounds = [42, 42.5]
lon_bounds = [4.25, 5]

pool = mp.Pool(10)
print(mp.cpu_count())

for n, data_paths in enumerate(atmos_data_paths):

    for i, path in enumerate(data_paths):
        
        if path == data_paths[0]:

            data = xr.open_dataset(path, chunks='auto')
            lat = data.nav_lat_grid_M.values
            lon = data.nav_lon_grid_M.values

            ind = []

            for j in range(lat.shape[0]):
                for k in range(lat.shape[1]):
                    if lat_bounds[0] < lat[j, k] < lat_bounds[1] and lon_bounds[0] < lon[j, k] < lon_bounds[1]:
                        ind.append((j, k))

            print('found indices')

        pool.apply_async(find_ave, args=(root_path, data_paths, data_names[n], i, ind))

pool.close()
pool.join()
