##############################################################################
#   Created: Thu Jun  4 12:11:38 2020
#   Author: mach
##############################################################################

import os
import xarray as xr
from glob import glob
from dask.diagnostics import ProgressBar
import numpy as np
from scipy.integrate import simps

##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/DWF_GOL_1993-2013/NEMO/CONT_SEAS_CLIM/'
data_path = [root_path + 'RHO/N-SEAS-']

save_path = root_path + 'D/'

for i, path in enumerate(data_path):
    
    print(i)

    rho_path = glob(path + '*rho.nc')
    
    rho_path.sort()
    
    for _, path in enumerate([rho_path[0]]):

        print(path)
        rho = xr.open_dataset(path)

        dens = rho.vodensity
        d = dens.deptht

        ind = dens[0].values > 0

        depth = np.zeros(ind[0].shape)

        for j in range(depth.shape[0]):
            for k in range(depth.shape[1]):
                print(j)
                if len(d[ind[:, j, k]]) > 0:
                    depth[j, k] = np.nanmax(d[ind[:, j, k]])

        D = xr.DataArray(depth, dims=['y_grid_T', 'x_grid_T'])
        D.coords['nav_lat_grid_T'] = dens.nav_lat_grid_T
        D.coords['nav_lon_grid_T'] = dens.nav_lon_grid_T
        
        filename = save_path + 'NEMO_1993-2013_D.nc'
        delayed = D.to_netcdf(filename, compute=False)

        print('Saving: ', filename)
        with ProgressBar():
            results = delayed.compute()
