##############################################################################
# By Doug Keller
##############################################################################

import numpy as np
import xarray as xr
from glob import glob
import pickle
from dask.diagnostics import ProgressBar
import os
from os.path import basename
import multiprocessing as mp

##############################################################################

def ellipse(x, y, theta, a, b, x0, y0):
    return ((x - x0) * np.cos(theta) - (y - y0) *np.sin(theta))**2/a**2 + ((x - x0) * np.sin(theta) + (y - y0) * np.cos(theta))**2/b**2
    
def is_in_ellipse(x, y, theta, a, b, x0, y0):
    
    if ellipse(x, y, theta, a, b, x0, y0) <= 1:
        return True
    else:
        return False

##############################################################################

# x0 = 5.175
# y0 = 41.6

# a = 1.75
# b = 1.375

# deg = 0
# theta = -deg * np.pi/180

##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/DWF_GOL_1993-2013/'

data_path = root_path + 'MED/SI/'
data_paths = glob(data_path + 'N-*')
data_paths.sort()

ddata_path = root_path + 'NEMO/CONT_SEAS_CLIM/RHO/'
ddata_paths = glob(ddata_path + 'N-*.nc')
ddata_paths.sort()

sdata_path = root_path + 'NEMO/CONT_SEAS_CLIM/S/'
sdata_paths = glob(sdata_path + 'N-*.nc')
sdata_paths.sort()

tdata_path = root_path + 'NEMO/CONT_SEAS_CLIM/T/'
tdata_paths = glob(tdata_path + 'N-*.nc')
tdata_paths.sort()

print('loaded data')

##############################################################################

def find_ind(root_path, path, ddata_path, sdata_path, tdata_path, i):
    
#    print(i)
#    data = xr.open_dataarray(path, chunks='auto')
#    ddata = xr.open_dataset(ddata_path, chunks='auto')
#    sdata = xr.open_dataset(sdata_path, chunks='auto')
#    tdata = xr.open_dataset(tdata_path, chunks='auto')
#    
#    # ellipse search
#    x0 = 5.2
#    y0 = 41.6
#
#    a = 1.85
#    b = 1.375
#
#    deg = 17
#    theta = -deg * np.pi/180
#
#    nddata = ddata.where(ellipse(ddata.nav_lon_grid_T, ddata.nav_lat_grid_T, theta, a, b, x0, y0) <= 1)
#    nsdata = sdata.where(ellipse(sdata.nav_lon, sdata.nav_lat, theta, a, b, x0, y0) <= 1)
#    ntdata = tdata.where(ellipse(tdata.nav_lon, tdata.nav_lat, theta, a, b, x0, y0) <= 1)
#
#    new_ddata = nddata.where(data <= 0)
#    new_sdata = nsdata.where(data <= 0)
#    new_tdata = ntdata.where(data <= 0)
#
#    delayed = new_ddata.to_netcdf(root_path + 'GOL_DWF/RHO/' + basename(ddata_path), compute=False)
#    print('rho')
#    with ProgressBar():
#        results = delayed.compute()
#
#    delayed = new_sdata.to_netcdf(root_path + 'GOL_DWF/S/' + basename(sdata_path), compute=False)
#    print('salinity')
#    with ProgressBar():
#        results = delayed.compute()
#
#    delayed = new_tdata.to_netcdf(root_path + 'GOL_DWF/T/' + basename(tdata_path), compute=False)
#    print('temperature')
#    with ProgressBar():
#        results = delayed.compute()

    print(i)
    data = xr.open_dataarray(path, chunks='auto')
    ddata = xr.open_dataset(ddata_paths[i], chunks='auto')
    sdata = xr.open_dataset(sdata_paths[i], chunks='auto')
    tdata = xr.open_dataset(tdata_paths[i], chunks='auto')
    
    # ellipse search
    x0 = 5.2
    y0 = 41.6

    a = 1.85
    b = 1.375

    deg = 17
    theta = -deg * np.pi/180

    nd = ddata.vodensity.where(ellipse(ddata.nav_lon_grid_T, ddata.nav_lat_grid_T, theta, a, b, x0, y0) <= 1)
    nN = ddata.vovaisala.where(ellipse(ddata.nav_lon_grid_W, ddata.nav_lat_grid_W, theta, a, b, x0, y0) <= 1)

    nsdata = sdata.where(ellipse(sdata.nav_lon, sdata.nav_lat, theta, a, b, x0, y0) <= 1)
    ntdata = tdata.where(ellipse(tdata.nav_lon, tdata.nav_lat, theta, a, b, x0, y0) <= 1)

    nd = nd.rename({'nav_lat_grid_T':'nav_lat', 'nav_lon_grid_T':'nav_lon'})
    nN = nN.rename({'nav_lat_grid_W':'nav_lat', 'nav_lon_grid_W':'nav_lon'})
    nd = nd.swap_dims({'y_grid_T':'y', 'x_grid_T':'x'})
    nN = nN.swap_dims({'y_grid_W':'y', 'x_grid_W':'x'})

    data = data.rename({'nav_lat_grid_W':'nav_lat', 'nav_lon_grid_W':'nav_lon'})
    data = data.swap_dims({'y_grid_W':'y', 'x_grid_W':'x'})

    new_d = nd.where(data <= 0)
    new_N = nN.where(data <= 0)
    new_sdata = nsdata.where(data <= 0)
    new_tdata = ntdata.where(data <= 0)

    delayed = new_d.to_netcdf(root_path + 'GOL_DWF/RHO/' + basename(ddata_path)[0:-4] + 'd.nc', compute=False)
    print('rho')
    with ProgressBar():
        results = delayed.compute()

    del new_d, nd

    delayed = new_N.to_netcdf(root_path + 'GOL_DWF/N/' + basename(ddata_path)[0:-4] + 'N.nc', compute=False)
    print('N2')
    with ProgressBar():
        results = delayed.compute()

    del new_N, nN

    delayed = new_sdata.to_netcdf(root_path + 'GOL_DWF/S/' + basename(sdata_path), compute=False)
    print('salinity')
    with ProgressBar():
        results = delayed.compute()

    del new_sdata, nsdata

    delayed = new_tdata.to_netcdf(root_path + 'GOL_DWF/T/' + basename(tdata_path), compute=False)
    print('temperature')
    with ProgressBar():
        results = delayed.compute()

##############################################################################

pool = mp.Pool(10)

#for i, path in enumerate(data_paths):
for i, path in enumerate([data_paths[0]]):
    
#    pool.apply_async(find_ind, args=(root_path, path, ddata_paths[i], sdata_paths[i], tdata_paths[i], i))

    print(i)
    data = xr.open_dataarray(path, chunks='auto')
    ddata = xr.open_dataset(ddata_paths[i], chunks='auto')
    sdata = xr.open_dataset(sdata_paths[i], chunks='auto')
    tdata = xr.open_dataset(tdata_paths[i], chunks='auto')
    
    # ellipse search
    x0 = 5.2
    y0 = 41.6

    a = 1.85
    b = 1.375

    deg = 17
    theta = -deg * np.pi/180

    data = data.rename({'nav_lat_grid_W':'nav_lat', 'nav_lon_grid_W':'nav_lon'})
    data = data.swap_dims({'y_grid_W':'y', 'x_grid_W':'x'})

    ndata = data.where(ellipse(data.nav_lon, data.nav_lat, theta, a, b, x0, y0)<=1)

    ind = np.where(ndata<=0)


#    nd = ddata.vodensity.where(ellipse(ddata.nav_lon_grid_T, ddata.nav_lat_grid_T, theta, a, b, x0, y0) <= 1)
#    nN = ddata.vovaisala.where(ellipse(ddata.nav_lon_grid_W, ddata.nav_lat_grid_W, theta, a, b, x0, y0) <= 1)
#
#    nsdata = sdata.where(ellipse(sdata.nav_lon, sdata.nav_lat, theta, a, b, x0, y0) <= 1)
#    ntdata = tdata.where(ellipse(tdata.nav_lon, tdata.nav_lat, theta, a, b, x0, y0) <= 1)
#
#    nd = nd.rename({'nav_lat_grid_T':'nav_lat', 'nav_lon_grid_T':'nav_lon'})
#    nN = nN.rename({'nav_lat_grid_W':'nav_lat', 'nav_lon_grid_W':'nav_lon'})
#    nd = nd.swap_dims({'y_grid_T':'y', 'x_grid_T':'x'})
#    nN = nN.swap_dims({'y_grid_W':'y', 'x_grid_W':'x'})
#
#    data = data.rename({'nav_lat_grid_W':'nav_lat', 'nav_lon_grid_W':'nav_lon'})
#    data = data.swap_dims({'y_grid_W':'y', 'x_grid_W':'x'})
#
#    new_d = nd.where(data <= 0)
#    new_N = nN.where(data <= 0)
#    new_sdata = nsdata.where(data <= 0)
#    new_tdata = ntdata.where(data <= 0)
#
#    delayed = nd.to_netcdf(root_path + 'GOL_DWF/RHO/' + basename(ddata_path)[0:-4] + 'd.nc', compute=False)
#    print('rho')
#    with ProgressBar():
#        results = delayed.compute()
#
#    delayed = nN.to_netcdf(root_path + 'GOL_DWF/RHO/' + basename(ddata_path)[0:-4] + 'N.nc', compute=False)
#    print('N2')
#    with ProgressBar():
#        results = delayed.compute()
#
#    delayed = new_sdata.to_netcdf(root_path + 'GOL_DWF/S/' + basename(sdata_path), compute=False)
#    print('salinity')
#    with ProgressBar():
#        results = delayed.compute()
#
#    delayed = new_tdata.to_netcdf(root_path + 'GOL_DWF/T/' + basename(tdata_path), compute=False)
#    print('temperature')
#    with ProgressBar():
#        results = delayed.compute()

pool.close()
pool.join()
