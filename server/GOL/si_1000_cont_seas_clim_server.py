##############################################################################
#   Created: Thu Jun  4 12:11:38 2020
#   Author: mach
##############################################################################

import os
import xarray as xr
from glob import glob
from dask.diagnostics import ProgressBar
import numpy as np
from scipy.integrate import simps

##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/DWF_GOL_1993-2013/NEMO/CONT_SEAS_CLIM/'
data_path = [root_path + 'RHO/N-C', root_path + 'RHO/N-S']
#data_path = [root_path + 'RHO/N-SEAS-']

save_path = root_path + '../../MED/SI1000/'

for i, path in enumerate(data_path):
    
    print(i)

    rho_path = glob(path + '*rho.nc')
#    rho_path = glob(path + '1999*rho.nc')
    
    rho_path.sort()
    
    for _, path in enumerate(rho_path):

        print(path)
        rho = xr.open_dataset(path)

        N = rho.vovaisala
        d = N.depthw

        ind = d <= 1000

#        SI = N*d
#        SI = simps(SI, d, axis=1)
        SI = simps(N[:,ind,:,:]*d[ind], d[ind], axis=1)
        SI = xr.DataArray(SI, dims=['time_counter', 'y_grid_W', 'x_grid_W'])
        SI.coords['time_counter'] = N.time_counter
        SI.coords['nav_lat_grid_W'] = N.nav_lat_grid_W
        SI.coords['nav_lon_grid_W'] = N.nav_lon_grid_W
        
        filename = save_path + os.path.basename(path)[0:-6] + 'SI1000.nc'
        delayed = SI.to_netcdf(filename, compute=False)

        print('Saving: ', filename)
        with ProgressBar():
            results = delayed.compute()
