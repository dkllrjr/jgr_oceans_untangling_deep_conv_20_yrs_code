import xarray as xr
from glob import glob
import numpy as np
import pickle
from dask.diagnostics import ProgressBar
import os
import multiprocessing as mp


##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/DWF_GOL_1993-2013/'
data_path = root_path + 'MED/SI/'

data_paths = glob(data_path + 'N-*')
data_paths.sort()

grid_data_path = root_path + 'NEMO/CONT_SEAS_CLIM/2D/'
grid_data_paths = glob(grid_data_path + 'N-*3H_2D.nc')
grid_data_paths.sort()

#data = xr.open_mfdataset(data_paths, combine='by_coords')
#data = data.__xarray_dataarray_variable__

print('loaded data')

##############################################################################

def find_ave(root_path, grid_data_paths, i, ind):

    print(i)
    grid = xr.open_dataset(grid_data_paths[i], chunks='auto')
    
    tgrid = grid.time_counter.values
    print(i)

    grid_names = ['somixhgt', 'soprecip', 'sosalflx']
    grid_dict = {}
    print(i)

    for name in grid_names:
        grid_dict[name] = []
        for indi in ind:
            grid_dict[name].append(grid[name][:, indi[0], indi[1]])
    
        grid_dict[name] = np.mean(grid_dict[name], axis=0)
        print(name)

    grid_dict['t'] = tgrid

    with open(root_path + 'GOL/2D/' + os.path.basename(grid_data_paths[i])[0:-3] + '.pickle', 'wb') as file:
        pickle.dump(grid_dict, file)


##############################################################################

# lat_bounds = [40.6, 42.5]
# lon_bounds = [4, 6.5]
# lat_bounds = [41, 42.7]
# lon_bounds = [4, 6.5]
# lat_bounds = [41.3, 42.7]
# lon_bounds = [3.8, 6]
# lat_bounds = [41.75, 42.7]
# lon_bounds = [4, 5.5]
# lat_bounds = [42, 42.5]
# lon_bounds = [4.25, 5]
lat_bounds = [42, 42.25]
lon_bounds = [4.5, 5]

pool = mp.Pool(10)
print(mp.cpu_count())

for i, path in enumerate(data_paths):
    
    if path == data_paths[0]:

        data = xr.open_dataarray(path, chunks='auto')
        lat = data.nav_lat_grid_W.values
        lon = data.nav_lon_grid_W.values

        ind = []

        for j in range(lat.shape[0]):
            for k in range(lat.shape[1]):
                if lat_bounds[0] < lat[j, k] < lat_bounds[1] and lon_bounds[0] < lon[j, k] < lon_bounds[1]:
                    ind.append((j, k))

        print('found indices')

    pool.apply_async(find_ave, args=(root_path, grid_data_paths, i, ind))

pool.close()
pool.join()

