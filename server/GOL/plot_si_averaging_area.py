import xarray as xr
from glob import glob
import numpy as np
import pickle
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib.patches import Rectangle


##############################################################################

def gol_si_plot(si, plot_path):

    fig, ax = plt.subplots(1, 1, figsize=(15, 12), dpi=400, subplot_kw={'projection': ccrs.PlateCarree(5)})
    fsize = 28
    tsize = 34
        
    europe_land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white') 
    # ax.add_feature(europe_land_10m)
    
    gl = ax.gridlines(crs=ccrs.PlateCarree(),draw_labels=True,linewidth=.75, color='black', alpha=0.35, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-90,2,5,8,90])
    gl.ylocator = mticker.FixedLocator([0,40,42,44,60])
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': fsize}
    gl.ylabel_style = {'size': fsize}
    gl.xpadding = 30
    gl.ypadding = 30
    
    ax.set_extent([1,9,39,45], crs=ccrs.PlateCarree());

    ax.add_feature(europe_land_10m)
    
    m = ax.contourf(si.nav_lon, si.nav_lat, si, transform=ccrs.PlateCarree(), colors='grey', shading='flat', alpha=0.8, vmax=500, vmin=400)

    x, y = 4.25, 42
    rect = Rectangle((x, y), 5-x, 42.5-y, fill=False, linewidth=2, edgecolor='black', transform=ccrs.PlateCarree(), label='Spatial Ave.', zorder=3)
    ax.add_patch(rect)

    ax.set_title('NW Mediterranean Sea', pad=35, fontsize=tsize+4)
    ax.legend(fontsize=fsize)
    
    fig.subplots_adjust(bottom=0.1, right=0.98)
    fig.savefig(plot_path)
    plt.close()

##############################################################################

data_path = '/bdd/MEDI/workspaces/dkeller/DWF_GOL_1993-2013/NEMO/CONT_SEAS_CLIM/2D/'

data_paths = glob(data_path + 'N-C*')
data_paths.sort()
data_paths = data_paths[0]

data = xr.open_dataset(data_paths)
data = data.sosstsst

print('loaded data')

##############################################################################

plot_root_path = '/bdd/MEDI/workspaces/dkeller/DWF_GOL_1993-2013/PLOTS/'

print('plotting')

gol_si_plot(data[0], plot_root_path + 'si_averaging_area.pdf')
