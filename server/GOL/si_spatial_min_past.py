import xarray as xr
from glob import glob
import numpy as np
import pickle


##############################################################################

def find_lat_lon(loc,lat,lon):

    def dist_sphere(r0, the0, phi0, r1, the1, phi1):
        #    (r0**2 + r1**2 - 2*r0*r1*(np.sin(the0)*np.sin(the1)*np.cos(phi0-phi1) + np.cos(the0)*np.cos(the1)))**.5
        a = r0**2 + r1**2
        b = 2*r0*r1
        c = np.sin(the0)*np.sin(the1)*np.cos(phi0-phi1) + np.cos(the0)*np.cos(the1)
        d = (a - b*c)
        return d**.5
    
    def dist_sphere_deg(r0, the0, phi0, r1, the1, phi1):
        the0 = np.deg2rad(the0)
        phi0 = np.deg2rad(phi0)
        the1 = np.deg2rad(the1)
        phi1 = np.deg2rad(phi1)
        return dist_sphere(r0, the0, phi0, r1, the1, phi1)
    
    def lat_lon_near(lat_lon, loc):
        E_r = 6371000  # Earth's radius
        dist = []
        for i in range(len(lat_lon)):
            # print(i,len(lat_lon))
            dist.append(dist_sphere_deg(E_r, lat_lon[i][1], lat_lon[i][0], E_r, loc[1], loc[0]))
        ind = np.where(np.array(dist) == np.min(np.array(dist)))[0]
        return np.array(lat_lon)[ind]

    lat_lon = []
    
    for i in range(lat.shape[0]):
        for j in range(lat.shape[1]):
            lat_lon.append([lat[i, j], lon[i, j]])
    
    ind_loc = []
    
    for i in range(len(loc)):
        ind_loc.append(lat_lon_near(lat_lon, loc[i]))
    
    lat_lon_np = np.array(lat_lon)
    lat_lon_np = lat_lon_np.reshape(lat.shape[0], lat.shape[1], 2)
    ind_loc_np = np.array(ind_loc)
    ind_loc_np = ind_loc_np.reshape(len(loc), 2)
    
    ind = []
    
    for k in range(ind_loc_np.shape[0]):
        for i in range(lat_lon_np.shape[0]):
            for j in range(lat_lon_np.shape[1]):
                if tuple(lat_lon_np[i, j]) == tuple(ind_loc_np[k]):
                    ind.append([i, j])
    
    ind = np.array(ind)
    
    return ind


##############################################################################

data_path = '/bdd/MEDI/workspaces/dkeller/DWF_GOL_SENSITIVITY/NEMO/CONT_SEAS_CLIM/SI/'

data_paths = glob(data_path + 'N-C*')
data_paths.sort()

#data = xr.open_mfdataset(data_paths, combine='by_coords')
#data = data.__xarray_dataarray_variable__

print('loaded data')

##############################################################################

bounds = [[42.35, 40.9], [6.5, 3.8]]
diff_bounds = [bounds[0][0]-bounds[0][1], bounds[1][0]-bounds[1][1]]

loc = [[diff_bounds[0]/2 + bounds[0][1], diff_bounds[1]/2 + bounds[1][1]]]

si_loc = []

for path in data_paths:
    
    data = xr.open_dataarray(path)
    
#    print('actually loaded data')
    
    if path == data_paths[0]:
        lat = data.nav_lat_grid_W.values
        lon = data.nav_lon_grid_W.values

        ind = find_lat_lon(loc, lat, lon)
        ind = ind[0]

        space_y = .08*2
        inc_y = int(diff_bounds[1]//space_y)

        space_x = .08*2
        inc_x = int(diff_bounds[1]//space_x)

        ind_y = [ind[0]-inc_y, ind[0]+inc_y+1]
        ind_x = [ind[1]-inc_x, ind[1]+inc_x+1]

        sind_y = slice(ind_y[0], ind_y[1], 1)
        sind_x = slice(ind_x[0], ind_x[1], 1)

        print('found indices')

    sdata = data.isel(y_grid_W=sind_y, x_grid_W=sind_x)

    if path == data_paths[0]:
        sbounds = {'lat_max': sdata.nav_lat_grid_W.max().values, 'lat_min': sdata.nav_lat_grid_W.min().values, 'lon_max': sdata.nav_lon_grid_W.max().values, 'lon_min': sdata.nav_lon_grid_W.min().values}

    si_min = sdata.min(dim=('y_grid_W', 'x_grid_W'))
    si_arg = sdata.argmin(dim=('time_counter', 'y_grid_W', 'x_grid_W'))

    si_loc.append([sdata.isel(time_counter=si_arg['time_counter'].values, y_grid_W=si_arg['y_grid_W'].values, x_grid_W=si_arg['x_grid_W'].values).nav_lat_grid_W.values, sdata.isel(time_counter=si_arg['time_counter'].values, y_grid_W=si_arg['y_grid_W'].values, x_grid_W=si_arg['x_grid_W'].values).nav_lon_grid_W.values])


##############################################################################

ndata = {'locs': si_loc, 'bounds': sbounds}

with open(data_path + 'si_min_location.pickle', 'wb') as file:
    pickle.dump(ndata, file)
