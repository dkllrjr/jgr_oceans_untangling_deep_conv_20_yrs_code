import xarray as xr
from glob import glob
import numpy as np
import pickle
from dask.diagnostics import ProgressBar
import os
import multiprocessing as mp


##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/DWF_GOL_1993-2013/'
data_path = root_path + 'MED/SI/'
data1000_path = root_path + 'MED/SI1000/'

depth_path = glob(root_path + 'MED/D/*')

data_paths = glob(data_path + 'N-*')
data_paths.sort()

data1000_paths = glob(data_path + 'N-*')
data1000_paths.sort()

grid_data_path = root_path + 'NEMO/CONT_SEAS_CLIM/2D/'
grid_data_paths = glob(grid_data_path + 'N-*1D_2D.nc')
grid_data_paths.sort()

#data = xr.open_mfdataset(data_paths, combine='by_coords')
#data = data.__xarray_dataarray_variable__

print('loaded data')

##############################################################################

def find_ave(root_path, path, grid_data_paths, data1000_paths, depth_path, i, ind):
    
    print(i)
    data = xr.open_dataarray(path, chunks='auto')
    data1000 = xr.open_dataarray(data1000_paths[i], chunks='auto')
    if i == 0:
        depth = xr.open_dataarray(depth_path[i], chunks='auto')
    grid = xr.open_dataset(grid_data_paths[i], chunks='auto')

    tsi = data.time_counter.values
    tgrid = grid.time_counter.values

    si = []
    si1000 = []
    for indi in ind:
        si.append(data[:, indi[0], indi[1]])
        si1000.append(data1000[:, indi[0], indi[1]])

    si = np.mean(si, axis=0)
    si1000 = np.mean(si1000, axis=0)
    sdata = {'t': tsi, 'si': si, 'si1000': si1000}

    print('si')

    if i == 0:
        d = []
        for indi in ind:
            d.append(depth[indi[0], indi[1]])
        d = np.mean(d)
        depth_data = {'d': d}

    grid_names = ['sosstsst', 'sosaline', 'sossheig', 'soshfldo', 'sohefldo', 'somxl010', 'sowaflup']
    grid_dict = {}
    print(i)
    
    for name in grid_names:
        grid_dict[name] = []
        for indi in ind:
            grid_dict[name].append(grid[name][:, indi[0], indi[1]])
    
        grid_dict[name] = np.mean(grid_dict[name], axis=0)
        print(name)

    grid_dict['t'] = tgrid

    with open(root_path + 'GOL/SI/' + os.path.basename(path)[0:-3] + '.pickle', 'wb') as file:
        pickle.dump(sdata, file)
    
    if i == 0:
        with open(root_path + 'GOL/D/' + os.path.basename(depth_path[i])[0:-3] + '.pickle', 'wb') as file:
            pickle.dump(depth_data, file)

        print('here')
    
    with open(root_path + 'GOL/2D/' + os.path.basename(grid_data_paths[i])[0:-3] + '.pickle', 'wb') as file:
        pickle.dump(grid_dict, file)

##############################################################################

# lat_bounds = [40.6, 42.5]
# lon_bounds = [4, 6.5]
# lat_bounds = [41, 42.7]
# lon_bounds = [4, 6.5]
# lat_bounds = [41.3, 42.7]
# lon_bounds = [3.8, 6]
# lat_bounds = [41.75, 42.7]
# lon_bounds = [4, 5.5]
# lat_bounds = [42, 42.5]
# lon_bounds = [4.25, 5]
lat_bounds = [42, 42.25]
lon_bounds = [4.5, 5]

pool = mp.Pool(40)

# for i, path in enumerate([data_paths[0]]):
# for i, path in enumerate(data_paths[0]):
for i, path in enumerate(data_paths):
    
    if path == data_paths[0]:
        
        data = xr.open_dataarray(path, chunks='auto')
        lat = data.nav_lat_grid_W.values
        lon = data.nav_lon_grid_W.values

        ind = []

        for j in range(lat.shape[0]):
            for k in range(lat.shape[1]):
                if lat_bounds[0] < lat[j, k] < lat_bounds[1] and lon_bounds[0] < lon[j, k] < lon_bounds[1]:
                    ind.append((j, k))

    print('found indices')
    
    pool.apply_async(find_ave, args=(root_path, path, grid_data_paths, data1000_paths, depth_path, i, ind))
    # find_ave(root_path, path, grid_data_paths, data1000_paths, depth_path, i, ind)

pool.close()
pool.join()
