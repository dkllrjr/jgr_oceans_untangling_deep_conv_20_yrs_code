##############################################################################
#   Created: Thu Jun  4 12:11:38 2020
#   Author: mach
##############################################################################

import os
import xarray as xr
from glob import glob
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib.patches import Rectangle
import warnings
warnings.filterwarnings("ignore")

##############################################################################
# Plotting

def nemomed_plot(grid, wrfgrid, plot_path):

    fig, axs = plt.subplots(1, 2, figsize=(16, 4.75), dpi=400, subplot_kw={'projection': ccrs.PlateCarree(10)})

    ax = axs[::-1]
    
    fsize = 18
    tsize = 22
    tpad = 15

    europe_land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white') 
    
    gl = ax[0].gridlines(crs=ccrs.PlateCarree(),draw_labels=True,linewidth=.75, color='black', alpha=0.35, linestyle='--', zorder=4)
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-90, -10, 0, 10, 20, 30, 90])
    gl.ylocator = mticker.FixedLocator([0, 30, 35, 40, 45, 90])
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': fsize}
    gl.ylabel_style = {'size': fsize}
    
    #ax[0].plot(5, 42, transform=ccrs.PlateCarree(), marker='x', color='black', markersize=5, linestyle='', label='Golf of Lion')
    
    #ax[0].plot(4.7495, 44.5569, transform=ccrs.PlateCarree(), marker='o', color='black', markersize=5, linestyle='', label='Montélimar, FR', zorder=3)
    
    m = ax[0].pcolor(grid.nav_lon, grid.nav_lat, grid, transform=ccrs.PlateCarree(), color='grey', shading='flat', zorder=1)
    
    ax[0].set_extent([-15,40,27.5,47.5], crs=ccrs.PlateCarree());
    
    ax[0].add_feature(europe_land_10m, zorder=2)
    
    x, y = 1, 39
    rect = Rectangle((x,y), 9-x, 45-y, fill=False, edgecolor='black', transform=ccrs.PlateCarree(), label='NW Med.', zorder=3)
    ax[0].add_patch(rect)

    ax[0].set_title('NEMOMED12', fontsize=tsize, pad=tpad)
    ax[0].legend(fontsize=fsize-4)
    
    europe_land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='none')
    
    ax[1].add_feature(europe_land_10m)
    
    gl = ax[1].gridlines(crs=ccrs.PlateCarree(),draw_labels=True,linewidth=.75, color='black', alpha=0.35, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-90, -30, -15, 0, 15, 30, 45, 60, 90])
    gl.ylocator = mticker.FixedLocator([0, 30, 45, 60, 90])
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': fsize}
    gl.ylabel_style = {'size': fsize}
    
    m = ax[1].pcolor(wrfgrid.nav_lon_grid_M, wrfgrid.nav_lat_grid_M, wrfgrid, transform=ccrs.PlateCarree(), color='grey', shading='flat', alpha=.8)
    
    ax[1].set_extent([-37.5,62.5,17.5,65], crs=ccrs.PlateCarree());
    
    ax[1].set_title('RegIPSL WRF/ORCHIDEE', fontsize=tsize, pad=tpad)

    fig.subplots_adjust(top = 1.1, bottom = -.1, right=.985, left=0.05)
    
    fig.text(.0925, .9, '(a)', fontweight='bold', fontsize=fsize)
    fig.text(.5, .9, '(b)', fontweight='bold', fontsize=fsize)
    
    fig.savefig(plot_path)
    plt.close()

##############################################################################

root_path = '/bdd/MEDI/workspaces/dkeller/DWF_GOL_SENSITIVITY/'
data_path = glob(root_path + 'NEMO/NEMO-BULK-CONT/OCE/Output/HF/*2D.nc')
data_path = data_path[0]
wrf_path = root_path + 'WRFORCH/RAW/MEDCORDEXB_WRFGRD_Y2012_TAS.nc'

data = xr.open_dataset(data_path)
data = data.somixhgt
data = data[0]
# nemomap = data.where(~np.isnan(data), 1)
nemomap = data

wrf = xr.open_dataset(wrf_path)
wrf = wrf.tas
wrf = wrf[0]
wrfgrid = wrf

plot_path = '../PLOTS/nemomed12.pdf'

nemomed_plot(nemomap, wrfgrid, plot_path)
