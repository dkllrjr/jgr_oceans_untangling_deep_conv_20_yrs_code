def find_lat_lon(loc, lat, lon):

    def archav(rad):
        return np.arccos(1 - 2*rad)
        
    def hav(rad):
        return (1 - np.cos(rad))/2

    def dist_sphere(r0, the0, phi0, r1, the1, phi1):
        return r0*archav(hav(np.abs(phi0-phi1)) + np.cos(phi0)*np.cos(phi1)*hav(np.abs(the0-the1)))

    def dist_sphere_deg(r0, the0, phi0, r1, the1, phi1):
        the0 = np.deg2rad(the0)
        phi0 = np.deg2rad(phi0)
        the1 = np.deg2rad(the1)
        phi1 = np.deg2rad(phi1)
        return dist_sphere(r0, the0, phi0, r1, the1, phi1)

    def lat_lon_near(lat, lon, loc):
        E_r = 6371000  # Earth's radius
        d = dist_sphere_deg(E_r, lon, lat, E_r, loc[1], loc[0])
        ind = np.unravel_index(d.argmin(), d.shape)
        return (lat[ind], lon[ind]), ind

    ind = []
    
    for i, loci in enumerate(loc):
        _, tmp = lat_lon_near(lat, lon, loci)
        ind.append(tmp)
    
    return ind 
