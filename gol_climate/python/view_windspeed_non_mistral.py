import pickle
import numpy as np
import matplotlib.pyplot as plt
from glob import glob
from datetime import datetime
import pandas as pd

#  ──────────────────────────────────────────────────────────────────────────

data_paths = []

# atmos paths
data_paths.append(glob('../data/GOL5/M*HUSS.pickle'))  # huss
data_paths.append(glob('../data/GOL5/M*TAS.pickle'))  # tas
data_paths.append(glob('../data/GOL5/M*UAS.pickle'))  # uas
data_paths.append(glob('../data/GOL5/M*VAS.pickle'))  # vas

for i, data_path in enumerate(data_paths):
    data_paths[i].sort()

# load paths
data = []

for data_path in data_paths:
    data.append([])
    for path in data_path:
        with open(path, 'rb') as file:
            data[-1].append(pickle.load(file))

#  ──────────────────────────────────────────────────────────────────────────

d = {}
names = ['q', 'T', 'u', 'v']
var_names = ['huss', 'tas', 'uas', 'vas']

for i, datai in enumerate(data):
    for j, dataj in enumerate(datai):
        if j == 0:
            d[names[i]] = np.array(dataj[var_names[i]])
        else:
            d[names[i]] = np.append(d[names[i]], dataj[var_names[i]])

        if i == 0:
            if j == 0:
                d['t'] = np.array(dataj['t'])
            else:
                d['t'] = np.append(d['t'], dataj['t'])

u = d['u']
v = d['v']
q = d['q']
T = d['T']
t = d['t']

ws = (u**2 + v**2)**.5
wd = np.rad2deg(np.arctan2(v, u))

#  ──────────────────────────────────────────────────────────────────────────

with open('../data/mistral_attributes_1993-2013.pickle', 'rb') as file:
    mistral = pickle.load(file)

#  ──────────────────────────────────────────────────────────────────────────

mistral_ind = np.full(len(t), False)

for event in mistral['events']:
    td = np.timedelta64(12, 'h')
    beg = np.datetime64(event[0]) - td
    end = np.datetime64(event[-1]) + td

    mistral_ind = np.where((beg <= t) & (t < end), True, mistral_ind)

non_mistral_ind = np.invert(mistral_ind)

#  ──────────────────────────────────────────────────────────────────────────

months = np.arange(12) + 1
mistral_winds = []
non_mistral_winds = []

for month in months:
    mistral_winds.append([])
    non_mistral_winds.append([])

# mistral

for i, ti in enumerate(t[mistral_ind]):
    m = pd.Timestamp(ti).month
    mistral_winds[m - 1] = np.append(mistral_winds[m - 1], ws[mistral_ind][i])

# non mistral

for i, ti in enumerate(t[non_mistral_ind]):
    m = pd.Timestamp(ti).month
    non_mistral_winds[m - 1] = np.append(non_mistral_winds[m - 1], ws[non_mistral_ind][i])

#  ──────────────────────────────────────────────────────────────────────────
# winter vs summer

winter = [1, 2, 3, 10, 11, 12]
summer = [4, 5, 6, 7, 8, 9]

mw = [[], []]
nmw = [[], []]
for m in months:
    if m in winter:
        mw[0] = np.append(mw[0], mistral_winds[m - 1])
        nmw[0] = np.append(nmw[0], non_mistral_winds[m - 1])
    else:
        mw[1] = np.append(mw[1], mistral_winds[m - 1])
        nmw[1] = np.append(nmw[1], non_mistral_winds[m - 1])

for i in range(2):
    mw[i] = np.mean(mw[i])
    nmw[i] = np.mean(nmw[i])

#  ──────────────────────────────────────────────────────────────────────────

mwa = []
nmwa = []
for m in months:
    mwa.append(np.mean(mistral_winds[m - 1]))
    nmwa.append(np.mean(non_mistral_winds[m - 1]))

plt.bar(months - 0.25, mwa, width=0.5)
plt.bar(months + 0.25, nmwa, width=0.5)
plt.show()
