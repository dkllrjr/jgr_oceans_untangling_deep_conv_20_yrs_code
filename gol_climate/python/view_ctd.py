import xarray as xr
import numpy as np
from glob import glob
import matplotlib.pyplot as plt

# ──────────────────────────────────────────────────────────────────────────

host_data = '../../../host_data/phd/ctd/'

# ──────────────────────────────────────────────────────────────────────────

dir_paths = glob(host_data + '*/')

files = []
for dir_path in dir_paths:
    files += glob(dir_path + '/ctd*.nc')

files.sort()

ctds = []
times = np.array([], dtype='datetime64[ns]')
for file in files:
    ctds.append(xr.open_dataset(file))
    times = np.append(times, ctds[-1]['JULD'].values)

plt.plot(times, np.ones(len(times)))
plt.show()


