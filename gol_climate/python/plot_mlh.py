##############################################################################
# By Doug Keller
# 
# This script plots the figure showing the difference betweent the control or 
# total stratification (SI) NEMO results and the seasonal SI results.
# Mistral events and important time stamps are overlaid for connectivity.
##############################################################################

import pickle
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import ticker
import matplotlib.dates as mdates
from glob import glob
import latent_heat_control as lhc
import latent_heat_seasonal as lhs
import numpy as np
from scipy import signal
import plot_si_bars as psb

##############################################################################
# load data

# data_paths = []
# data_paths.append(glob('../data/GOL/N-CONT*SI.pickle'))
# data_paths.append(glob('../data/GOL/N-SEAS*SI.pickle'))

# for i, data_path in enumerate(data_paths):
    # data_paths[i].sort()

# data = []

# for data_path in data_paths:
    # data.append([])
    # for path in data_path:
        # with open(path, 'rb') as file:
            # data[-1].append(pickle.load(file))

with open('../data/GOL1/NEMO_1993-2013_D.pickle', 'rb') as file:
    depth_data = pickle.load(file)

d = depth_data['d']

dc_years = np.array([1999, 2000, 2005, 2009, 2011, 2012, 2013])
years = np.arange(1994, 2014)

with open('../data/mistral_attributes_1993-2013.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

##############################################################################

t = np.array(psb.time) # time
t1_c = np.array(psb.inddc) # control SI minimum
t1 = np.array(psb.si_s_min_ind) # seasonal SI minimum

##############################################################################
# plot

# Consider adding Mistral events

width, height = 14, 18

fsize = 18
tsize = 22

# SI plot

fig, ax = plt.subplots(10, 2, dpi=400, figsize=(width, height))
ax = ax.flatten('F')

label_on = True

for i, (cont, seas) in enumerate(zip(lhc.mlh, lhs.mlh)):

    if years[i] in dc_years:

        peaks, _ = signal.find_peaks(cont, height=1000, prominence=1000)
        peaks = peaks[0]
        abv250 = np.where(cont > 250)[0][0]

        ms = 10

        ax[i].plot(lhc.thf[i][abv250], cont[abv250], color='red', linestyle='', marker='o', markersize=ms, label='>250m')
        ax[i].plot(lhc.thf[i][peaks], cont[peaks], color='red', linestyle='', marker='^', markersize=ms, label='Peak')

        lw = 2
        ax[i].axvline(t[i][t1_c[i]], color='black', linewidth=lw, linestyle='--', label='$SI_{min,Cont}$')
        ax[i].axvline(t[i][t1[i]], color='tab:blue', linewidth=lw, linestyle='--', label='$SI_{min,Seas}$')

        if label_on:
            ax[i].legend(ncol=2, loc='lower left', fontsize=fsize-4)
            label_on = False

    ax[i].plot(lhc.thf[i], cont, color='black', label='Control')
    ax[i].plot(lhs.thf[i], seas, color='tab:blue', label='Seasonal')
    
    ax[i].set_ylim(0, d)
    ax[i].invert_yaxis()
    ax[i].set_xlim(lhc.thf[i][0], lhc.thf[i][-1])
    ax[i].tick_params(axis='both', labelsize=fsize-4)

    # xaxis stuff
    fmt_half_year = mdates.MonthLocator(interval=6)
    ax[i].xaxis.set_major_locator(fmt_half_year)
    
    fmt_month = mdates.MonthLocator()
    ax[i].xaxis.set_minor_locator(fmt_month)

    ax[i].xaxis.set_minor_formatter(mdates.DateFormatter('%m'))

    ax[i].grid(axis='x', which='minor', alpha=.5)
    ax[i].grid(axis='x', which='major', alpha=.5)

    # yaxis stuff
    ax[i].yaxis.set_minor_locator(ticker.AutoMinorLocator())
    
    ax[i].grid(axis='y', which='major', alpha=.5)

    if i < 10:
        ax[i].set_ylabel('$m$', fontsize=fsize)

    if i == 9 or i == 19:
        ax[i].set_xlabel('Time', fontsize=fsize)

    if years[i] in dc_years:
        ax[i].tick_params(axis='x', labelsize=fsize-4, labelcolor='tab:green')
    else:
        ax[i].tick_params(axis='x', labelsize=fsize-4)

    mweak = 0
    mstr = 0
    h0, h1 = 0, d
    # for j, event in enumerate(mistral_data['events']):
        # # if j == 0:
            # # rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:green', alpha=.25, label='Mistral')
            # # p3 = rect
        # # else:
            # # rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:green', alpha=.25)
        # if mistral_data['strength'][j] == 0:
            # if mweak == 0:
                # rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:red', alpha=.25, label='Weak Mistral')
                # p3 = rect
                # mweak = 1
            # else:
                # rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:red', alpha=.25)
        # else:
            # if mstr == 0:
                # rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:blue', alpha=.25, label='Strong Mistral')
                # p4 = rect
                # mstr = 1
            # else:
                # rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:blue', alpha=.25)
        # ax[i].add_patch(rect)

    for j, event in enumerate(mistral_data['events']):
        if j == 0:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:green', alpha=.25, label='Mistral')
            p3 = rect
        else:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:green', alpha=.25)
        ax[i].add_patch(rect)

    if i == 0:
        ax[i].legend(ncol=3, loc='lower right', fontsize=fsize-4)

fig.suptitle('Gulf of Lion $MLD$', fontsize=tsize, y=.99)
fig.tight_layout()
fig.savefig('../plots/mlh_gol.pdf')
fig.savefig('../plots/mlh_gol.png')

# ──────────────────────────────────────────────────────────────────────────

fig, ax = plt.subplots(1, 1, dpi=400, figsize=(18, 6))

max_mlh_c = []
max_mlh_s = []

for i, (cont, seas) in enumerate(zip(lhc.mlh, lhs.mlh)):
    max_mlh_c.append(np.max(cont))
    max_mlh_s.append(np.max(seas))
    
ax.plot(years, max_mlh_c, color='black', label='Control')
ax.plot(years, max_mlh_s, color='tab:blue', label='Seasonal')

ax.set_ylim(0, d)
ax.tick_params(axis='both', labelsize=fsize-4)
ax.set_xticks(years)

ax.grid(axis='x', which='major', alpha=.5)

ax.yaxis.set_minor_locator(ticker.AutoMinorLocator())
ax.grid(axis='y', which='major', alpha=.5)
ax.set_ylabel('$m$', fontsize=fsize)
ax.set_xlabel('Time', fontsize=fsize)

ax.tick_params(axis='x', labelsize=fsize-4)
ax.invert_yaxis()
ax.legend(ncol=3, loc='lower right', fontsize=fsize-4)

fig.suptitle('Gulf of Lion $MLD$ Max', fontsize=tsize)
fig.tight_layout()
fig.savefig('../plots/mlh_gol_max.png')
