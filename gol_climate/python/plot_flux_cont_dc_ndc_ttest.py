##############################################################################
# By Doug Keller
# 
# This script plots the figure showing the difference betweent the control or 
# total stratification (SI) NEMO results and the seasonal SI results.
# Mistral events and important time stamps are overlaid for connectivity.
##############################################################################

import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import ticker
import matplotlib.dates as mdates
from matplotlib import cm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import FormatStrFormatter

import numpy as np
import plot_flux_cont_dc_ndc as pfcdn

#  ──────────────────────────────────────────────────────────────────────────

fig, ax = plt.subplots(3, 1, figsize=(17, 12), dpi=300)

fsize = 18
tsize = 28

#  ──────────────────────────────────────────────────────────────────────────
# finding passing tests

ind = []
for i, item in enumerate([pfcdn.dqtt, pfcdn.dTtt, pfcdn.wstt]):
    ind.append({})
    tmp = np.where(item > 0.05, True, False)

    ind[i]['beg'] = []
    ind[i]['end'] = []
    for j, tmpj in enumerate(tmp):
        if j < len(tmp) - 1:
            if tmp[j-1] and not tmpj:
                ind[i]['beg'].append(j)
            elif tmp[j+1] and not tmpj:
                ind[i]['end'].append(j)
                
#  ──────────────────────────────────────────────────────────────────────────

for i, axi in enumerate(ax):
    axi.tick_params(axis='both', labelsize=fsize)
    axi.set_yticks(np.arange(8))
    axi.set_ylim(0, 1)

tx = pfcdn.ta[0]
dateformatter = mdates.DateFormatter('%b. %d')

for j, axj in enumerate(ax):
    axj.xaxis.set_major_formatter(dateformatter)

linewidth = 3

ax[0].plot(tx, pfcdn.dqtt, linewidth=linewidth, color='k')
ax[1].plot(tx, pfcdn.dTtt, linewidth=linewidth, color='k')
ax[2].plot(tx, pfcdn.wstt, linewidth=linewidth, color='k')

labels=['$\\Delta q$', '$\\Delta \\theta$', '$|u_z|$']

for i, axi in enumerate(ax):
    axi.set_ylabel(labels[i], fontsize=fsize)
    axi.set_xlim(tx[0], tx[-1])
    axi.axvline(tx[int(np.mean(pfcdn.smaxm))], color='k', linestyle=':', label='${t_0}$')
    axi.axvline(tx[int(np.mean(pfcdn.dcm))], color='k', linestyle='--', label='${t_1}$')
    axi.axhline(0.05, color='tab:red', label='0.05')

    n = 0.25
    axi.set_yticks(np.arange(0, 1 + n, n))

    axi.yaxis.set_minor_locator(ticker.AutoMinorLocator())

    axi.tick_params(axis='both', labelsize=fsize)

    for j, _ in enumerate(ind[i]['beg']):
        if i == 1 and j == 0:
            axi.axvspan(tx[ind[i]['beg'][j]], tx[ind[i]['end'][j]], color='tab:red', alpha=0.3, label='<0.05')
        else:
            axi.axvspan(tx[ind[i]['beg'][j]], tx[ind[i]['end'][j]], color='tab:red', alpha=0.3)

ax[-1].set_xlabel('Date', fontsize=fsize+4)
ax[1].legend(ncol=6, fontsize=fsize-4)

textx = 0.003

fig.text(textx, 0.95, '(a)', fontweight='bold', fontsize=fsize+2)
fig.text(textx, 0.6425, '(b)', fontweight='bold', fontsize=fsize+2)
fig.text(textx, 0.335, '(c)', fontweight='bold', fontsize=fsize+2)

fig.suptitle('p-values', fontsize=tsize)

fig.tight_layout()

fig.savefig('../plots/flux_cont_dc_ndc_ttest.png')
fig.savefig('../plots/flux_cont_dc_ndc_ttest.pdf')

plt.close()
