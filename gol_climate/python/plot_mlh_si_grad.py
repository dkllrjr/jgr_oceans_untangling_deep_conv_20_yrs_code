import matplotlib.pyplot as plt
import pickle
import numpy as np

#  ──────────────────────────────────────────────────────────────────────────

with open('../data/mlh_si_gradients.pkl', 'rb') as file:
    data = pickle.load(file)

years = data['years']

#  ──────────────────────────────────────────────────────────────────────────

fsize = 14

fig, ax = plt.subplots(1, 1, figsize=(12, 6), dpi=400)

x = np.arange(1, len(years) + 1)
width = 0.39
shift = width/2 + 0.01
ax.bar(x + shift, data['sis'], width=width, color='tab:blue', label='$\\partial_t SI_S}$')
ax.bar(x - shift, data['dsi'], width=width, color='tab:green', label='$\\partial_t \\delta SI$')


shifty = 0.0001
for i, _ in enumerate(data['dsi']):
    ax.text(x[i] + shift, data['sis'][i] - shifty, str(np.round(data['sis'][i], 4)), horizontalalignment='center', verticalalignment='top')
    ax.text(x[i] - shift, data['dsi'][i] - shifty, str(np.round(data['dsi'][i], 4)), horizontalalignment='center', verticalalignment='top')

ax.set_xticks(x, years)
ax.tick_params(axis='both', labelsize=fsize-2)

ax.grid(which='major', axis='x')

ax.set_xlabel('Deep Convection Years', fontsize=fsize)
ax.set_ylabel('SI Gradient $m^2/s^3$', fontsize=fsize)

ax.set_title('Vertical Mixing Drivers', fontsize=fsize+4, pad=20)

ax.text(0.2, 0, 'Ratio:', fontweight='bold', fontsize=fsize, verticalalignment='bottom', horizontalalignment='left')

ax.legend(ncol=2)

fig.tight_layout()

for i, rat in enumerate(data['ratio']):
    tmp = np.round(rat,2)
    if tmp > 1:
        ax.text(x[i], 0, str(tmp), horizontalalignment='center', fontweight='bold', verticalalignment='bottom', fontsize=fsize, color='tab:green')
    else:
        ax.text(x[i], 0, str(tmp), horizontalalignment='center', fontweight='bold', verticalalignment='bottom', fontsize=fsize, color='tab:blue')


fig.savefig('../plots/mlh_si_gradients.png')
fig.savefig('../plots/mlh_si_gradients.pdf')
