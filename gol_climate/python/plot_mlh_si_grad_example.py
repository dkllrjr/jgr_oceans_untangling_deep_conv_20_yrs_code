import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib import ticker
import pickle
import numpy as np
from scipy import signal
import pandas

import plot_si as ps
import plot_mlh_peaks as pmp

#  ──────────────────────────────────────────────────────────────────────────

with open('../data/mlh_si_gradients.pkl', 'rb') as file:
    data = pickle.load(file)

#  ──────────────────────────────────────────────────────────────────────────

# year = 1999, j = 5
years = np.arange(1994, 2014)

#  ──────────────────────────────────────────────────────────────────────────

fsize = 20

fig, ax = plt.subplots(1, 1, figsize=(12, 6), dpi=400)

j = 5
cont, seas = pmp.lhc.mlh[j], pmp.lhs.mlh[j]

ax.plot(pmp.lhc.thf[j], cont, color='black', label='Control')

peaks, _ = signal.find_peaks(cont, height=1000, prominence=1000)
abv250 = np.where(cont > 250)[0][0]

beg = pandas.to_datetime(pandas.Timestamp(pmp.lhc.thf[j][abv250]))
end = pandas.to_datetime(pandas.Timestamp(pmp.lhc.thf[j][peaks[0]]))

ax.plot(pmp.lhc.thf[j][peaks], cont[peaks], color='red', linestyle='', marker='^', label='Peaks')
ax.plot(pmp.lhc.thf[j][abv250], cont[abv250], color='red', linestyle='', marker='o', label='>250')

ax.set_ylim(0, pmp.d)
ax.invert_yaxis()
ax.set_xlim(pmp.lhc.thf[j][1480], pmp.lhc.thf[j][-800])
ax.tick_params(axis='both', labelsize=fsize-4)

# xaxis stuff
fmt_half_year = mdates.MonthLocator(interval=6)
ax.xaxis.set_major_locator(fmt_half_year)

fmt_month = mdates.MonthLocator()
ax.xaxis.set_minor_locator(fmt_month)

ax.xaxis.set_minor_formatter(mdates.DateFormatter('%m'))

ax.grid(axis='x', which='minor', alpha=.5)
ax.grid(axis='x', which='major', alpha=.5)

# yaxis stuff
ax.yaxis.set_minor_locator(ticker.AutoMinorLocator())

ax.grid(axis='y', which='major', alpha=.5)

ax.set_ylabel('$m$', fontsize=fsize)
ax.set_xlabel('Time', fontsize=fsize)

ax.tick_params(axis='x', labelsize=fsize-4)

k = 0

dy = 0.3
axsis = ax.twinx()
[p1] = axsis.plot(ps.data[0][j]['t'], ps.data[1][j]['si'], color='tab:blue')
y = 0.4
axsis.set_ylim(y, y + dy)
axsis.yaxis.label.set_color(p1.get_color())
axsis.tick_params(axis='y', colors=p1.get_color(), labelsize=fsize-4)
axsis.set_ylabel('$SI_S$', fontsize=fsize)

axdsi = ax.twinx()
[p2] = axdsi.plot(ps.data[0][j]['t'], ps.data[0][j]['si'] - ps.data[1][j]['si'], color='tab:red', ls='--')
y = -0.5
axdsi.set_ylim(y, y + dy)
axdsi.yaxis.label.set_color(p2.get_color())
axdsi.tick_params(axis='y', colors=p2.get_color(), labelsize=fsize-4)
axdsi.spines.right.set_position(("axes", 1.13))
axdsi.set_ylabel('$\\delta SI$', fontsize=fsize)

ax.axvspan(beg, end, color='k', alpha=0.2)
t = ps.data[0][j]['t']
ind = np.where((beg <= t) & (t <= end), True, False)

x = np.arange(len(t))
y = np.polyval([data['sis'][k], 0], x)
x_tmp = t.astype('datetime64[s]').astype('int')
tmp_b = np.interp(np.datetime64(beg).astype('datetime64[s]').astype('int'), x_tmp, ps.data[1][j]['si'])
b = ps.data[1][j]['si'][224] - y[224]
# y += b
# y += tmp_b
# axsis.plot(t[ind], y[ind], c='tab:blue', ls=':', label='$\\overline{\\partial_t SI_S}$')
axsis.plot(t[ind], y[ind] - y[ind][0] + tmp_b, c='tab:blue', ls=':', label='$\\overline{\\partial_t SI_S}$')

y = np.polyval([data['dsi'][k], 0], x)
tmp_b = np.interp(np.datetime64(beg).astype('datetime64[s]').astype('int'), x_tmp, ps.data[0][j]['si'] - ps.data[1][j]['si'])
# b = (ps.data[0][j]['si'][224] - ps.data[1][j]['si'][224]) - y[224]
# y += b
axdsi.plot(t[ind], y[ind] - y[ind][0] + tmp_b, c='tab:red', ls=':', label='$\\overline{\\partial_t \\delta SI}$')

axdsi.legend(loc='lower left')
axsis.legend(loc='lower right')

fig.tight_layout()
fig.savefig('../plots/mlh_si_grad_example.png')
