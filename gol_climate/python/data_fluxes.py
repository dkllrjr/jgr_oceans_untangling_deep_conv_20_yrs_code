##############################################################################
# By Doug Keller
# 
# This script plots the figure showing the difference betweent the control or 
# total stratification (SI) NEMO results and the seasonal SI results.
# Mistral events and important time stamps are overlaid for connectivity.
##############################################################################

import pickle
from datetime import datetime, timedelta
from glob import glob
import numpy as np
import latent_heat_control as lhc
import latent_heat_seasonal as lhs
from scipy.io import savemat

##############################################################################
# load data

data_paths = []
data_paths.append(glob('../data/GOL5/N-CONT*SI.pickle'))
data_paths.append(glob('../data/GOL5/N-SEAS*SI.pickle'))

for i, data_path in enumerate(data_paths):
    data_paths[i].sort()

data = []

for data_path in data_paths:
    data.append([])
    for path in data_path:
        with open(path, 'rb') as file:
            data[-1].append(pickle.load(file))

qnet = np.array([])
qsh = np.array([])
qlh = np.array([])
qsw = np.array([])
qlw = np.array([])
t = np.array([])

for i, _ in enumerate(lhs.qnet):

    qnet = np.append(qnet, lhs.qnet[i])
    qsh = np.append(qsh, lhs.qsh[i])
    qlh = np.append(qlh, lhs.qlh[i])
    qsw = np.append(qsw, lhs.qsw[i])
    qlw = np.append(qlw, lhs.qlw[i])

    t_temp = []
    for j, d in enumerate(lhs.tda[i]):
        t_temp.append(np.datetime_as_string(d))

    t = np.append(t, t_temp)
    
q_data = {'qnet': qnet, 'qsh': qsh, 'qlh': qlh, 'qsw': qsw, 'qlw': qlw, 't': t}

savemat('../data/seasonal_surface_flux_components_42_5_1993-2013.mat', q_data)

##############################################################################

qnet = np.array([])
qsh = np.array([])
qlh = np.array([])
qsw = np.array([])
qlw = np.array([])
t = np.array([])

for i, _ in enumerate(lhc.qnet):

    qnet = np.append(qnet, lhc.qnet[i])
    qsh = np.append(qsh, lhc.qsh[i])
    qlh = np.append(qlh, lhc.qlh[i])
    qsw = np.append(qsw, lhc.qsw[i])
    qlw = np.append(qlw, lhc.qlw[i])

    t_temp = []
    for j, d in enumerate(lhc.tda[i]):
        t_temp.append(np.datetime_as_string(d))

    t = np.append(t, t_temp)

q_data = {'qnet': qnet, 'qsh': qsh, 'qlh': qlh, 'qsw': qsw, 'qlw': qlw, 't': t}

savemat('../data/control_surface_flux_components_42_5_1993-2013.mat', q_data)
