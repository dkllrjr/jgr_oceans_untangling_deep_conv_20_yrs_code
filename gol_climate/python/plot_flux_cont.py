##############################################################################
# By Doug Keller
# 
# This script plots the figure showing the difference betweent the control or 
# total stratification (SI) NEMO results and the seasonal SI results.
# Mistral events and important time stamps are overlaid for connectivity.
##############################################################################

import pickle
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import ticker
import matplotlib.dates as mdates
from glob import glob
import numpy as np
# import latent_heat_control as lhc
import latent_heat_seasonal as lhs
import plot_si_bars as psb
from matplotlib import cm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import FormatStrFormatter

##############################################################################
# load data

data_paths = []
data_paths.append(glob('../data/GOL5/N-CONT*SI.pickle'))
data_paths.append(glob('../data/GOL5/N-SEAS*SI.pickle'))

for i, data_path in enumerate(data_paths):
    data_paths[i].sort()

data = []

for data_path in data_paths:
    data.append([])
    for path in data_path:
        with open(path, 'rb') as file:
            data[-1].append(pickle.load(file))

temp_data_path = glob('../data/GOL5/N-SEAS*1D_2D.pickle')

temp_data = []
for path in temp_data_path:
    with open(path, 'rb') as file:
        temp_data.append(pickle.load(file))

sst = []
for tdata in temp_data:
    sst = np.append(sst, tdata['sosstsst'])

dc_years = np.array([1999, 2000, 2005, 2009, 2011, 2012, 2013])
years = np.arange(1994, 2014)

ind_dc = []
ind_not_dc = []
dyears = []
for i, year in enumerate(years):
    if year in dc_years:
        ind_dc.append(i)
    else:
        ind_not_dc.append(i)

    dyears.append(datetime(year=year, month=1, day=1))

ind_dc = np.array(ind_dc)
ind_not_dc = np.array(ind_not_dc)
dyears = np.array(dyears)

##############################################################################

data = data[1]

# qsw,          qlw,        qsh,        qlh
# shortwave,    longwave,   sensible,   latent  heat flux down

si_sw = []
si_lw = []
si_sh = []
si_lh = []

sm_sw = []
sm_lw = []
sm_sh = []
sm_lh = []

sd_sw = []
sd_lw = []
sd_sh = []
sd_lh = []

adj1 = 9.81/(2 * 1000 * 4184 * (sst.mean() + 273.15))
adj2 = 10**-9
for i, d in enumerate(data):
    si_sw.append(np.trapz(lhs.qsw[i][psb.si_s_max_ind[i]:psb.inddc[i]+1], dx=86400))
    si_lw.append(np.trapz(lhs.qlw[i][psb.si_s_max_ind[i]:psb.inddc[i]+1], dx=86400))
    si_sh.append(np.trapz(lhs.qsh[i][psb.si_s_max_ind[i]:psb.inddc[i]+1], dx=86400))
    si_lh.append(np.trapz(lhs.qlh[i][psb.si_s_max_ind[i]:psb.inddc[i]+1], dx=86400))

    sm_sw.append(np.trapz(lhs.qsw[i][0:psb.si_s_max_ind[i]+1], dx=86400))
    sm_lw.append(np.trapz(lhs.qlw[i][0:psb.si_s_max_ind[i]+1], dx=86400))
    sm_sh.append(np.trapz(lhs.qsh[i][0:psb.si_s_max_ind[i]+1], dx=86400))
    sm_lh.append(np.trapz(lhs.qlh[i][0:psb.si_s_max_ind[i]+1], dx=86400))

    sd_sw.append(np.trapz(lhs.qsw[i][0:psb.inddc[i]+1], dx=86400))
    sd_lw.append(np.trapz(lhs.qlw[i][0:psb.inddc[i]+1], dx=86400))
    sd_sh.append(np.trapz(lhs.qsh[i][0:psb.inddc[i]+1], dx=86400))
    sd_lh.append(np.trapz(lhs.qlh[i][0:psb.inddc[i]+1], dx=86400))

# max to min
si_sw = np.array(si_sw)
si_lw = np.array(si_lw)
si_sh = np.array(si_sh)
si_lh = np.array(si_lh)

# init to max
sm_sw = np.array(sm_sw)
sm_lw = np.array(sm_lw)
sm_sh = np.array(sm_sh)
sm_lh = np.array(sm_lh)

# init to 
sd_sw = np.array(sd_sw)
sd_lw = np.array(sd_lw)
sd_sh = np.array(sd_sh)
sd_lh = np.array(sd_lh)

totalm = sm_sw + sm_lw + sm_sh + sm_lh
total = si_sw + si_lw + si_sh + si_lh
totald = sd_sw + sd_lw + sd_sh + sd_lh

adj = np.mean((psb.si_s_mindc - psb.si_init)/totald)
adj_temp = (psb.si_s_mindc - psb.si_init)/totald
# adj_temp = abs((psb.si_s_mindc - psb.si_init)/totald)
adj = adj2

si_sw *= adj
si_lw *= adj
si_sh *= adj
si_lh *= adj

sm_sw *= adj
sm_lw *= adj
sm_sh *= adj
sm_lh *= adj

sd_sw *= adj
sd_lw *= adj
sd_sh *= adj
sd_lh *= adj

total *= adj
totalm *= adj
totald *= adj

# si_sw *= adj_temp
# si_lw *= adj_temp
# si_sh *= adj_temp
# si_lh *= adj_temp

# sm_sw *= adj_temp
# sm_lw *= adj_temp
# sm_sh *= adj_temp
# sm_lh *= adj_temp

# sd_sw *= adj_temp
# sd_lw *= adj_temp
# sd_sh *= adj_temp
# sd_lh *= adj_temp

# total *= adj_temp
# totalm *= adj_temp
# totald *= adj_temp

rel_sw = si_sw/abs(total)
rel_lw = si_lw/abs(total)
rel_sh = si_sh/abs(total)
rel_lh = si_lh/abs(total)

##############################################################################

# fig, ax = plt.subplots(5, 1, figsize=(12, 12), dpi=200)

# beg = datetime(year=1993, month=6, day=1)
# end = datetime(year=2013, month=6, day=1)

# bar_width = timedelta(days=100)

# ax[0].axhline(, color='tab:red', label='$\overline{SI_{S,max}}$')

# ax[0].bar(dyears, si_sw, width=bar_width, color='tab:blue', label='$Q_{sw}$')
# ax[0].bar(dyears[ind_dc], si_sw[ind_dc], width=bar_width, color='none', hatch='//', label='Deep Conv.')

# ax[1].bar(dyears, si_lw, width=bar_width, color='tab:blue', label='$Q_{lw}$')
# ax[1].bar(dyears[ind_dc], si_lw[ind_dc], width=bar_width, color='none', hatch='//', label='Deep Conv.')

# ax[2].bar(dyears, si_sh, width=bar_width, color='tab:blue', label='$Q_{sh}$')
# ax[2].bar(dyears[ind_dc], si_sh[ind_dc], width=bar_width, color='none', hatch='//', label='Deep Conv.')

# ax[3].bar(dyears, si_lh, width=bar_width, color='tab:blue', label='$Q_{lh}$')
# ax[3].bar(dyears[ind_dc], si_lh[ind_dc], width=bar_width, color='none', hatch='//', label='Deep Conv.')

# ax[4].bar(dyears, total, width=bar_width, color='tab:blue', label='$Q_{net}$')
# ax[4].bar(dyears[ind_dc], total[ind_dc], width=bar_width, color='none', hatch='//', label='Deep Conv.')

# fmt_year = mdates.YearLocator()
# for axis in ax:
    # axis.xaxis.set_major_locator(fmt_year)
    # axis.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
    # axis.set_xlim(beg, end)
    # axis.legend(ncol=2)

# plt.show()

##############################################################################

# def prob_dc(q, ind_dc):
    # counts, bins = np.histogram(q)
    # counts_dc, _ = np.histogram(q[ind_dc], bins=bins)
    # probs = counts_dc/counts
    # # probs = np.where(np.isnan(probs), 0, probs)
    
    # for i, prob in enumerate(probs):
        # if np.isnan(prob):
            # probs[i] = np.mean([probs[i-1], probs[i+1]]) 

    # return counts, bins, probs

# fig, ax = plt.subplots(1, 4, figsize=(12, 4), dpi=200)

# fsize = 14
# tsize = 24

# cmap = cm.get_cmap('plasma')

# q = [si_sw, si_lw, si_sh, si_lh]
# labels = ['$Q_{sw}$', '$Q_{lw}$', '$Q_{sh}$', '$Q_{lh}$']

# for i, qi in enumerate(q):
    
    # N = 25

    # counts, bins, probs = prob_dc(qi, ind_dc)
    # nbins = np.repeat(bins[:-1:], N)
    # nbins = np.arange(bins[0], bins[-1], (bins[-1]-bins[0])/nbins.size)
    # nbins = np.append(nbins, bins[-1])
    # ncounts = np.repeat(counts, N)
    # nprobs = np.interp(nbins[:-1:]+np.diff(nbins)/2, bins[:-1:]+np.diff(bins)/2, probs)

    # colors = []
    # for nprob in nprobs:
        # colors.append(cmap(nprob))

    # bar_width = bins[1] - bins[0]
    # nbar_width = nbins[1] - nbins[0]
    # # ax[i].bar(bins[:-1:], counts, width=bar_width, align='edge', color='none')
    # ax[i].bar(nbins[:-1:], ncounts, width=nbar_width, align='edge', color=colors)

    # ax[i].set_xlabel('$10^9 \\times \int^{t_{SI_{min}}}_{t_0} Q_{' + labels[i][4:6] + '} \; d t$   $m^2/s^2$', fontsize=fsize-1)

    # ax[i].set_title(labels[i], fontsize=tsize)
    # ax[i].yaxis.set_major_formatter(FormatStrFormatter('%d'))

# ax[0].set_ylabel('$N_{years}$', fontsize=fsize)

# fig.subplots_adjust(bottom=0.185, left=0.05, right=0.93, top=0.9)
# cax = fig.add_axes([0.94, 0.22, 0.0125, 0.65])
# cs = cm.ScalarMappable(cmap=cmap)
# fig.colorbar(cs, cax=cax, label='$P(DC)$')

# # fig.tight_layout()

# fig.savefig('../plots/flux_cont.png')

# plt.close()

##############################################################################
# percentage of years deep convection

def per_dc(q, ind_dc):
    counts, bins = np.histogram(q)
    counts_dc, _ = np.histogram(q[ind_dc], bins=bins)
    probs = counts_dc/counts
    probs = np.where(np.isnan(probs), 0, probs)
    
    # for i, prob in enumerate(probs):
        # if np.isnan(prob):
            # probs[i] = np.mean([probs[i-1], probs[i+1]]) 

    return counts, bins, probs

# fig, ax = plt.subplots(2, 4, figsize=(12, 8), dpi=200)
fig, ax = plt.subplots(1, 4, figsize=(12, 4), dpi=200)
ax = ax.flatten('C')

fsize = 14
tsize = 24

cmap = cm.get_cmap('plasma')

# q = [si_sw, si_lw, si_sh, si_lh]
# q = [sd_sw, sd_lw, sd_sh, sd_lh]
# q = [si_sw, si_lw, si_sh, si_lh]
q = [sm_sw, sm_lw, sm_sh, sm_lh]

labels = ['$Q_{SW}$', '$Q_{LW}$', '$Q_{H}$', '$Q_{E}$']

# # 0 to max
# for i, qi in enumerate(q):
    
    # N = 25

    # counts, bins, probs = per_dc(qi, ind_dc)

    # colors = []
    # for prob in probs:
        # colors.append(cmap(prob))

    # bar_width = bins[1] - bins[0]
    # ax[i].bar(bins[:-1:], counts, width=bar_width, align='edge', color=colors)

    # ax[i].set_xlabel('$10^{-9} \\times \int^{t_{SI_{S,max}}}_{0} Q_{' + labels[i][4:6] + '} \; d t$   $m^2/s^2$', fontsize=fsize-1)

    # ax[i].set_title(labels[i], fontsize=tsize)
    # ax[i].yaxis.set_major_formatter(FormatStrFormatter('%d'))

    # ax[i].set_ylim([0, 12.25])

# q = [sm_sw, sm_lw, sm_sh, sm_lh]
q = [si_sw, si_lw, si_sh, si_lh]

# max to min
for i, qi in enumerate(q):

    N = 25

    counts, bins, probs = per_dc(qi, ind_dc)

    colors = []
    for prob in probs:
        colors.append(cmap(prob))

    bar_width = bins[1] - bins[0]
    ax[i].bar(bins[:-1:], counts, width=bar_width, align='edge', color=colors)

    ax[i].set_xlabel('$SI_{est.,' + labels[i][4:6] + '}$ $m^2/s^2$', fontsize=fsize-1)

    # ax[i+4].set_title(labels[i], fontsize=tsize)
    ax[i].yaxis.set_major_formatter(FormatStrFormatter('%d'))

    ax[i].set_ylim([0, 7.25])
    ax[i].set_title(labels[i], fontsize=tsize)

# if two rows
    # ax[i+4].bar(bins[:-1:], counts, width=bar_width, align='edge', color=colors)

    # ax[i+4].set_xlabel('$10^{-9} \\times \int^{t_{SI_{min}}}_{t_{SI_{S,max}}} Q_{' + labels[i][4:6] + '} \; d t$   $m^2/s^2$', fontsize=fsize-1)

    # # ax[i+4].set_title(labels[i], fontsize=tsize)
    # ax[i+4].yaxis.set_major_formatter(FormatStrFormatter('%d'))

    # ax[i+4].set_ylim([0, 7.25])

ax[0].set_ylabel('$N_{years}$', fontsize=fsize)
# ax[4].set_ylabel('$N_{years}$', fontsize=fsize)

for i, _ in enumerate(ax):
    ax[i].tick_params(axis='both', labelsize=fsize-2)

fig.tight_layout()

# fig.subplots_adjust(bottom=0.185, left=0.05, right=0.92, top=0.9, wspace=0.15)
fig.subplots_adjust(right=0.92)
cax = fig.add_axes([0.93, 0.22, 0.0125, 0.65])
cs = cm.ScalarMappable(cmap=cmap)
cbar = fig.colorbar(cs, cax=cax, ticks=[0, 0.25, 0.5, 0.75, 1])
cbar.ax.set_yticklabels(['0', '25', '50', '75', '100'])
cbar.ax.tick_params(axis='both', labelsize=fsize-2)
cbar.ax.set_ylabel('% Years deep convection', fontsize=fsize)

# textx = 0.000
# fig.text(textx, 0.92, '(a)', fontweight='bold', fontsize=fsize+2)
# fig.text(textx, 0.45, '(b)', fontweight='bold', fontsize=fsize+2)

fig.savefig('../plots/flux_cont.png')

plt.close()

##############################################################################

# plt.plot(dyears, psb.si_init, marker='o')

# plt.plot(dyears, psb.si_init+total, marker='o')

# plt.show()

# fluxes = {'rsw': rel_sw, 'rlw': rel_lw, 'rsh': rel_sh, 'rlh': rel_lh}
# fluxes = {'rsw': sd_sw, 'rlw': sd_lw, 'rsh': sd_sh, 'rlh': sd_lh}
fluxes = {'rsw': si_sw, 'rlw': si_lw, 'rsh': si_sh, 'rlh': si_lh}

std_fluxes = np.std(q, axis=1)
mu_fluxes = np.mean(q, axis=1)

with open('../data/flux_contributions.pickle', 'wb') as file:
    pickle.dump(fluxes, file)
