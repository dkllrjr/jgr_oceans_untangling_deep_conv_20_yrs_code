import xarray as xr
import numpy as np
import pickle

##############################################################################

data = xr.open_dataarray('../data/NEMO_1993-2013_D.nc')

# gol 1
lat_bounds = [40.6, 42.5]
lon_bounds = [4, 6.5]

# gol 5
lat_bounds = [42, 42.5]
lon_bounds = [4.25, 5]

lat = data.nav_lat_grid_T.values 
lon = data.nav_lon_grid_T.values 
 
ind = [] 
 
for j in range(lat.shape[0]): 
    for k in range(lat.shape[1]): 
        if lat_bounds[0] < lat[j, k] < lat_bounds[1] and lon_bounds[0] < lon[j, k] < lon_bounds[1]: 
            ind.append((j, k)) 
 
print('found indices')

d = []
for indi in ind:
    d.append(np.max(data[indi[0], indi[1]].values))

d = np.mean(d)

print(d)

with open('../data/depth.pickle', 'wb') as file:
    pickle.dump(d, file)
