##############################################################################
# By Doug Keller
# 
# This script plots the figure showing the difference betweent the control or 
# total stratification (SI) NEMO results and the seasonal SI results.
# Mistral events and important time stamps are overlaid for connectivity.
##############################################################################

import pickle
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import ticker
import matplotlib.dates as mdates
from glob import glob
import numpy as np
# import latent_heat_control as lhc
import latent_heat_seasonal as lhs
import plot_si_bars as psb
from matplotlib import cm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import FormatStrFormatter
import core

from scipy.stats import ttest_ind

##############################################################################

def mean_lp(arr, ind):
    temp = []
    # lp = []
    for i in ind:
        if len(arr[i]) > 365:
            temp.append(np.concatenate([arr[i][0:243], arr[i][244::]]))
            # lp.append(arr[i][243])
        else:
            temp.append(arr[i])

    # lp = np.array([np.mean(lp)])
    # print(np.array(temp).shape)
    temp_std = np.std(temp, axis=0)
    temp = np.mean(temp, axis=0)

    # return np.concatenate([temp[0:243], lp, temp[243::]])

    m = np.concatenate([temp[0:243], temp[243::]])
    std = np.concatenate([temp_std[0:243], temp_std[243::]])

    return m, std


def temp_lp(arr, ind):

    temp = []
    for i in ind:
        if len(arr[i]) > 365:
            temp.append(np.concatenate([arr[i][0:243], arr[i][244::]]))
        else:
            temp.append(arr[i])

    temp = np.array(temp)
    # temp = np.concatenate([temp[0:243], temp[243::]])

    return temp


def ttest_lp(arr, ind_dc, ind_not_dc):

    temp_dc = temp_lp(arr, ind_dc)
    temp_not_dc = temp_lp(arr, ind_not_dc)

    ttest = []
    for i in range(temp_dc.shape[1]):
        _, tmp = ttest_ind(temp_dc[:, i], temp_not_dc[:, i])
        ttest.append(tmp)

    ttest = np.array(ttest)

    return ttest

##############################################################################
# from lhs

# qa = atmos specific humidity
# Ta = atmos temp
# u = u wind
# v = v wind
# ta = time
# sst = sea surface temp
# ws = wind speed

##############################################################################
# load data

data_paths = []
data_paths.append(glob('../data/GOL5/N-CONT*SI.pickle'))
data_paths.append(glob('../data/GOL5/N-SEAS*SI.pickle'))

for i, data_path in enumerate(data_paths):
    data_paths[i].sort()

data = []

for data_path in data_paths:
    data.append([])
    for path in data_path:
        with open(path, 'rb') as file:
            data[-1].append(pickle.load(file))

temp_data_path = glob('../data/GOL5/N-SEAS*1D_2D.pickle')

temp_data = []
for path in temp_data_path:
    with open(path, 'rb') as file:
        temp_data.append(pickle.load(file))

sst = []
for tdata in temp_data:
    sst = np.append(sst, tdata['sosstsst'])

dc_years = np.array([1999, 2000, 2005, 2009, 2011, 2012, 2013])
years = np.arange(1994, 2014)

ind_dc = []
ind_not_dc = []
dyears = []
for i, year in enumerate(years):
    if year in dc_years:
        ind_dc.append(i)
    else:
        ind_not_dc.append(i)

    dyears.append(datetime(year=year, month=1, day=1))

ind_dc = np.array(ind_dc)
ind_not_dc = np.array(ind_not_dc)
dyears = np.array(dyears)

##############################################################################

data = data[1]

# qsw,          qlw,        qsh,        qlh
# shortwave,    longwave,   sensible,   latent  heat flux down

si_sw = []
si_lw = []
si_sh = []
si_lh = []

sm_sw = []
sm_lw = []
sm_sh = []
sm_lh = []

sd_sw = []
sd_lw = []
sd_sh = []
sd_lh = []

adj1 = 9.81/(2 * 1000 * 4184 * (sst.mean() + 273.15))
adj2 = 10**-9
for i, d in enumerate(data):
    si_sw.append(np.trapz(lhs.qsw[i][psb.si_s_max_ind[i]:psb.inddc[i]], dx=86400))
    si_lw.append(np.trapz(lhs.qlw[i][psb.si_s_max_ind[i]:psb.inddc[i]], dx=86400))
    si_sh.append(np.trapz(lhs.qsh[i][psb.si_s_max_ind[i]:psb.inddc[i]], dx=86400))
    si_lh.append(np.trapz(lhs.qlh[i][psb.si_s_max_ind[i]:psb.inddc[i]], dx=86400))

    sm_sw.append(np.trapz(lhs.qsw[i][0:psb.si_s_max_ind[i]], dx=86400))
    sm_lw.append(np.trapz(lhs.qlw[i][0:psb.si_s_max_ind[i]], dx=86400))
    sm_sh.append(np.trapz(lhs.qsh[i][0:psb.si_s_max_ind[i]], dx=86400))
    sm_lh.append(np.trapz(lhs.qlh[i][0:psb.si_s_max_ind[i]], dx=86400))

    sd_sw.append(np.trapz(lhs.qsw[i][0:psb.inddc[i]], dx=86400))
    sd_lw.append(np.trapz(lhs.qlw[i][0:psb.inddc[i]], dx=86400))
    sd_sh.append(np.trapz(lhs.qsh[i][0:psb.inddc[i]], dx=86400))
    sd_lh.append(np.trapz(lhs.qlh[i][0:psb.inddc[i]], dx=86400))

# max to min
si_sw = np.array(si_sw)
si_lw = np.array(si_lw)
si_sh = np.array(si_sh)
si_lh = np.array(si_lh)

# init to max
sm_sw = np.array(sm_sw)
sm_lw = np.array(sm_lw)
sm_sh = np.array(sm_sh)
sm_lh = np.array(sm_lh)

# init to 
sd_sw = np.array(sd_sw)
sd_lw = np.array(sd_lw)
sd_sh = np.array(sd_sh)
sd_lh = np.array(sd_lh)

total = si_sw + si_lw + si_sh + si_lh
totalm = sm_sw + sm_lw + sm_sh + sm_lh
totald = sd_sw + sd_lw + sd_sh + sd_lh

adj = np.mean((psb.si_s_mindc - psb.si_init)/totald)
adj_temp = (psb.si_s_mindc - psb.si_init)/totald
# adj_temp = abs((psb.si_s_mindc - psb.si_init)/totald)
adj = adj2

si_sw *= adj
si_lw *= adj
si_sh *= adj
si_lh *= adj

sm_sw *= adj
sm_lw *= adj
sm_sh *= adj
sm_lh *= adj

sd_sw *= adj
sd_lw *= adj
sd_sh *= adj
sd_lh *= adj

total *= adj
totalm *= adj
totald *= adj

# si_sw *= adj_temp
# si_lw *= adj_temp
# si_sh *= adj_temp
# si_lh *= adj_temp

# sm_sw *= adj_temp
# sm_lw *= adj_temp
# sm_sh *= adj_temp
# sm_lh *= adj_temp

# sd_sw *= adj_temp
# sd_lw *= adj_temp
# sd_sh *= adj_temp
# sd_lh *= adj_temp

# total *= adj_temp
# totalm *= adj_temp
# totald *= adj_temp

##############################################################################

total = np.sum(np.abs([si_sw, si_lw, si_sh, si_lh]), axis=0)

rel_sw = np.sign(si_sw) * np.abs(si_sw)/total
rel_lw = np.sign(si_lw) * np.abs(si_lw)/total
rel_sh = np.sign(si_sh) * np.abs(si_sh)/total
rel_lh = np.sign(si_lh) * np.abs(si_lh)/total

##############################################################################

fig, ax_all = plt.subplots(4, 4, figsize=(17, 16), dpi=300, constrained_layout=True)
# fig, ax_all = plt.subplots(5, 4, figsize=(17, 20), dpi=300, constrained_layout=True)

ax = ax_all[0, :]
gs = ax_all[0, 0].get_gridspec()

for axj in ax_all[1:, :]:
    for axi in axj:
        axi.remove()

axbig = []
for i in range(1, 4):
# for i in range(1, 5):
    axbig.append(fig.add_subplot(gs[i, :]))

axbig = np.array(axbig)

fsize = 18
tsize = 28

##############################################################################

labels = ['$Q_{SW}$', '$Q_{LW}$', '$Q_{H}$', '$Q_{E}$']
# x_label = ['$SI_{Est,' + label[4:6] + '}$ $m^2/s^2$' for label in labels]
x_label = ['$SI_{' + label[4:6] + '}$ $m^2/s^2$' for label in labels]

q = [si_sw, si_lw, si_sh, si_lh]
# q = [rel_sw, rel_lw, rel_sh, rel_lh]

q_dc = []
for i, qi in enumerate(q):
    q_dc.append(qi[ind_dc])

# q_dcs = np.std(q_dc, axis=1)
q_dcm = np.mean(q_dc, axis=1)

x = np.arange(len(q_dc))

q_ndc = []
for i, qi in enumerate(q):
    temp = np.delete(qi, ind_dc)
    q_ndc.append(temp)

# q_ndcs = np.std(q_ndc, axis=1)
q_ndcm = np.mean(q_ndc, axis=1)

N = 9

bin_edges = []
for i, qi in enumerate(q):
    _, b = np.histogram(qi, bins=N)
    bin_edges.append(b)

print('ttest for dc and ndc')
lw = 4
for i, axi in enumerate(ax):
    # axi.hist(q_dc[i], bins=bin_edges[i], color='tab:blue', edgecolor='blue', alpha=0.5, label='Deep Conv.')
    # axi.hist(q_ndc[i], bins=bin_edges[i], color='tab:red', edgecolor='red', alpha=0.5, label='Non Deep Conv.')

    print(labels[i], ttest_ind(q_dc[i], q_ndc[i]))

    alphas = 0.3

    axi.hist(q[i], bins=bin_edges[i], histtype='step', facecolor='none', alpha=alphas, edgecolor='black', label='All')

    # axi.hist(q_dc[i], bins=bin_edges[i], histtype='stepfilled', edgecolor='none', color='tab:blue', alpha=alphas, label='Deep Conv.')
    # axi.hist(q_ndc[i], bins=bin_edges[i], histtype='stepfilled', color='tab:red', alpha=alphas, label='Non Deep Conv.')

    axi.hist([q_dc[i], q_ndc[i]], bins=bin_edges[i], histtype='barstacked', color=('tab:blue', 'tab:red'), alpha=alphas, label=['DC', 'NDC'])

    axi.axvline(np.mean(q[i]), color='black', linewidth=lw, label='${All}$')
    axi.axvline(q_dcm[i], color='blue', linewidth=lw, label='${DC}$')
    axi.axvline(q_ndcm[i], color='red', linewidth=lw, label='${NDC}$')

    axi.set_title(labels[i], fontsize=tsize, pad=10)


ax = ax.flatten('C')
for i, axi in enumerate(ax):
    axi.tick_params(axis='both', labelsize=fsize)
    axi.set_yticks(np.arange(8))
    axi.set_ylim(0, 7.25)
    axi.set_xlabel(x_label[i], fontsize=fsize)


ax[0].set_ylabel('Number of years', fontsize=fsize)
ax[1].legend(fontsize=fsize-4)

##############################################################################

dq = np.array(lhs.qa, dtype=object) - np.array(lhs.qasat, dtype=object)
dT = np.array(lhs.Ta, dtype=object) - np.array(lhs.sst, dtype=object)
ws = np.array(lhs.ws, dtype=object)
ta = np.array(lhs.ta, dtype=object)

qamdc, _ = mean_lp(lhs.qa, ind_dc)
qasatmdc, _ = mean_lp(lhs.qasat, ind_dc)
Tamdc, _ = mean_lp(lhs.Ta, ind_dc)
sstmdc, _ = mean_lp(lhs.sst, ind_dc)
qamndc, _ = mean_lp(lhs.qa, ind_not_dc)
qasatmndc, _ = mean_lp(lhs.qasat, ind_not_dc)
Tamndc, _ = mean_lp(lhs.Ta, ind_not_dc)
sstmndc, _ = mean_lp(lhs.sst, ind_not_dc)

qam = [qamdc, qamndc]
qasatm = [qasatmdc, qasatmndc]
Tam = [Tamdc, Tamndc]
sstm = [sstmdc, sstmndc]

# dqm = [mean_lp(dq, ind_dc), mean_lp(dq, ind_not_dc)]
# dTm = [mean_lp(dT, ind_dc), mean_lp(dT, ind_not_dc)]
# wsm = [mean_lp(ws, ind_dc), mean_lp(ws, ind_not_dc)]

dqmdc, dqsdc = mean_lp(dq, ind_dc)
dqmndc, dqsndc = mean_lp(dq, ind_not_dc)
dTmdc, dTsdc = mean_lp(dT, ind_dc)
dTmndc, dTsndc = mean_lp(dT, ind_not_dc)
wsmdc, wssdc = mean_lp(ws, ind_dc)
wsmndc, wssndc = mean_lp(ws, ind_not_dc)

dqtt = ttest_lp(dq, ind_dc, ind_not_dc)
dTtt = ttest_lp(dT, ind_dc, ind_not_dc)
wstt = ttest_lp(ws, ind_dc, ind_not_dc)

dqm = [dqmdc, dqmndc]
dTm = [dTmdc, dTmndc]
wsm = [wsmdc, wsmndc]

dqs = [dqsdc, dqsndc]
dTs = [dTsdc, dTsndc]
wss = [wssdc, wssndc]

# x = np.arange(len(dqm[0]))
tx = ta[0]
dateformatter = mdates.DateFormatter('%b. %d')

smaxm = [np.mean(np.array(psb.si_s_max_ind)[ind_dc]), np.mean(np.array(psb.si_s_max_ind)[ind_not_dc])]
dcm = [np.mean(np.array(psb.inddc)[ind_dc]), np.mean(np.array(psb.inddc)[ind_not_dc])]

for j, axj in enumerate(axbig):
    axj.xaxis.set_major_formatter(dateformatter)

colors = ['tab:blue', 'tab:red']
labels = ['${DC}$', '${NDC}$']
labels_std = ['$\\sigma_{DC}$', '$\\sigma_{NDC}$']
alpha = 0.3
linewidth = 3

for i in [1, 0]:
    print(i, labels[i])
    axbig[0].fill_between(tx, y1=dqm[i]-dqs[i], y2=dqm[i]+dqs[i], alpha=alpha, color=colors[i], label=labels_std[i])
    axbig[1].fill_between(tx, y1=dTm[i]-dTs[i], y2=dTm[i]+dTs[i], alpha=alpha, color=colors[i], label=labels_std[i])
    axbig[2].fill_between(tx, y1=wsm[i]-wss[i], y2=wsm[i]+wss[i], alpha=alpha, color=colors[i], label=labels_std[i])

    axbig[0].plot(tx, dqm[i], linewidth=linewidth, color=colors[i], label=labels[i])
    axbig[1].plot(tx, dTm[i], linewidth=linewidth, color=colors[i], label=labels[i])
    axbig[2].plot(tx, wsm[i], linewidth=linewidth, color=colors[i], label=labels[i])

# twinbig = []
# for i, axi in enumerate(axbig):
    # twinbig.append(axi.twinx())

def rel_diff(y, yref):
    return np.abs((y - yref)/yref)

# twinbig[0].plot(tx, rel_diff(dqm[0], dqm[1])*100, color='k', label='% Diff. $\\Delta q$')
# twinbig[1].plot(tx, rel_diff(dTm[0], dTm[1])*100, color='k', label='% Diff. $\\Delta q$')
# twinbig[2].plot(tx, rel_diff(wsm[0], wsm[1])*100, color='k', label='% Diff. $\\Delta q$')
    
# for tbi in twinbig:
    # tbi.set_xlabel('|% Diff.|', fontsize=fsize)

# axbig[-1].plot(tx, rel_diff(dqm[0], dqm[1])*100, color='k', ls='-', label='$\\Delta q$')
# axbig[-1].plot(tx, rel_diff(dTm[0], dTm[1])*100, color='k', ls=':', label='$\\Delta T$')
# axbig[-1].plot(tx, rel_diff(wsm[0], wsm[1])*100, color='k', ls='--', label='$|u_z|$')
# axbig[-1].set_ylabel('$|\\frac{ x_{\\overline{DC}} - x_{\\overline{NDC}} }{ x_{\\overline{NDC}} }|$', fontsize=fsize+4)

# axbig[-1].legend(ncol=3, fontsize=fsize-4)
# axbig[-1].set_ylim(0, 105)

# difference
t0 = int(np.mean(smaxm))
t1 = int(np.mean(dcm))
dif = []
# dif.append(np.nanmean(dqm[0][t0:t1] - dqm[1][t0:t1]))
# dif.append(np.nanmean(dTm[0][t0:t1] - dTm[1][t0:t1]))
# dif.append(np.nanmean(wsm[0][t0:t1] - wsm[1][t0:t1]))
dif.append(dqm[0][t0:t1] - dqm[1][t0:t1])
dif.append(dTm[0][t0:t1] - dTm[1][t0:t1])
dif.append(wsm[0][t0:t1] - wsm[1][t0:t1])
# print(dif[0])
# print(dif[1])
# print(dif[2])

# for j, axj in enumerate(axbig):
    # axj.axvline(smaxm[i], color=colors[i], linestyle=':')
    # axj.axvline(dcm[i], color=colors[i], linestyle='--')

#  ──────────────────────────────────────────────────────────────────────────
# finding passing tests

ind = []
for i, item in enumerate([dqtt, dTtt, wstt]):
    ind.append({})
    tmp = np.where(item > 0.05, True, False)

    ind[i]['beg'] = []
    ind[i]['end'] = []
    for j, tmpj in enumerate(tmp):
        if j < len(tmp) - 1:
            if tmp[j-1] and not tmpj:
                ind[i]['beg'].append(j)
            elif tmp[j+1] and not tmpj:
                ind[i]['end'].append(j)

#  ──────────────────────────────────────────────────────────────────────────

labels=['$\\Delta q$ $kg_w/kg$', '$\\Delta \\theta$ $^\\circ C$', '$|u_z|$ $m/s$']

for i, axi in enumerate(axbig):
    axi.set_ylabel(labels[i], fontsize=fsize)
    axi.tick_params(axis='both', labelsize=fsize)
    axi.set_xlim(tx[0], tx[-1])
    axi.axvline(tx[int(np.mean(smaxm))], color='k', linestyle=':', label='${t_0}$')
    axi.axvline(tx[int(np.mean(dcm))], color='k', linestyle='--', label='${t_1}$')

    # p-values
    for j, _ in enumerate(ind[i]['beg']):
        if i == 1 and j == 0:
            axi.axvspan(tx[ind[i]['beg'][j]], tx[ind[i]['end'][j]], hatch='/', color='k', fc='none', alpha=1, label='p-value < 0.05')
        else:
            axi.axvspan(tx[ind[i]['beg'][j]], tx[ind[i]['end'][j]], hatch='/', color='k', fc='none', alpha=1)

# for i, axi in enumerate(axbig[:-1:]):
    # axi.set_ylabel(labels[i], fontsize=fsize)

axbig[-1].set_xlabel('Date', fontsize=fsize+4)
axbig[1].legend(ncol=7, fontsize=fsize-4)

textx = 0.005
# fig.text(textx, 0.975, '(a)', fontweight='bold', fontsize=fsize+2)
# fig.text(textx, 0.77, '(b)', fontweight='bold', fontsize=fsize+2)
# fig.text(textx, 0.58, '(c)', fontweight='bold', fontsize=fsize+2)
# fig.text(textx, 0.39, '(d)', fontweight='bold', fontsize=fsize+2)
# fig.text(textx, 0.2, '(f)', fontweight='bold', fontsize=fsize+2)

fig.text(textx, 0.975, '(a)', fontweight='bold', fontsize=fsize+2)
fig.text(textx, 0.720, '(b)', fontweight='bold', fontsize=fsize+2)
fig.text(textx, 0.47, '(c)', fontweight='bold', fontsize=fsize+2)
fig.text(textx, 0.235, '(d)', fontweight='bold', fontsize=fsize+2)

# fig.tight_layout()

fig.savefig('../plots/flux_cont_dc_ndc.png')
fig.savefig('../plots/flux_cont_dc_ndc.pdf')

plt.close()

qm = np.mean(q, axis=1) 

print('DC-NDC')
print(q_dcm - q_ndcm)
print('ALL-NDC')
print(qm - q_ndcm)
print('DC-ALL')
print(q_dcm - np.mean(q, axis=1))

print(np.round((q_dcm - q_ndcm)/qm, 2))

##############################################################################

# q = [si_sw, si_lw, si_sh, si_lh]
q = [rel_sw, rel_lw, rel_sh, rel_lh]

q_dc = []
for i, qi in enumerate(q):
    q_dc.append(qi[ind_dc])

# q_dcs = np.std(q_dc, axis=1)
q_dcm = np.mean(q_dc, axis=1)

x = np.arange(len(q_dc))

q_ndc = []
for i, qi in enumerate(q):
    temp = np.delete(qi, ind_dc)
    q_ndc.append(temp)

# q_ndcs = np.std(q_ndc, axis=1)
q_ndcm = np.mean(q_ndc, axis=1)

# print(q_dcm - q_ndcm)

##############################################################################

# q_e = 1.22 * 2.5e6 / 100
# q_h = 1.22 * 1000.5 / 100

# # 0 is DC, 1 is NDC, dif = [q, T, u]
# print(adj * np.trapz(q_e * dif[0] * wsm[1][t0:t1], dx=86400))
# print(adj * np.trapz(q_h * dif[1] * wsm[1][t0:t1], dx=86400))
# print(adj * np.trapz(q_e * dqm[1][t0:t1] * dif[2], dx=86400), adj * np.trapz(q_h * dTm[1][t0:t1] * dif[2], dx=86400), adj * np.trapz(q_e * dqm[1][t0:t1] * dif[2] + q_h * dTm[1][t0:t1] * dif[2], dx=86400))

qh_ref, qe_ref = np.zeros(len(wsm[0])), np.zeros(len(wsm[0]))
dqqe = np.zeros(len(wsm[0]))
dTqh = np.zeros(len(wsm[0]))
duqh, duqe = np.zeros(len(wsm[0])), np.zeros(len(wsm[0]))

# dq
for i, _ in enumerate(wsm[0]):
    Cd, Ch, Ce, T_zu, q_zu = core.turb_core_2z(2, 10, sstm[1][i], Tam[1][i], qasatm[1][i], qam[1][i], wsm[1][i])
    qh_ref[i], qe_ref[i] = -core.Qh(Ch, sstm[1][i], T_zu, wsm[1][i]), -core.E(Ce, qasatm[1][i], q_zu, wsm[1][i])

    Cd, Ch, Ce, T_zu, q_zu = core.turb_core_2z(2, 10, sstm[1][i], Tam[1][i], qasatm[0][i], qam[0][i], wsm[1][i])
    dqqe[i] = -core.E(Ce, qasatm[0][i], q_zu, wsm[1][i])

# dT
for i, _ in enumerate(wsm[0]):
    Cd, Ch, Ce, T_zu, q_zu = core.turb_core_2z(2, 10, sstm[0][i], Tam[0][i], qasatm[1][i], qam[1][i], wsm[1][i])
    dTqh[i] = -core.Qh(Ch, sstm[0][i], T_zu, wsm[1][i])

# du
for i, _ in enumerate(wsm[0]):
    Cd, Ch, Ce, T_zu, q_zu = core.turb_core_2z(2, 10, sstm[1][i], Tam[1][i], qasatm[1][i], qam[1][i], wsm[0][i])
    duqh[i], duqe[i] = -core.Qh(Ch, sstm[1][i], T_zu, wsm[0][i]), -core.E(Ce, qasatm[1][i], q_zu, wsm[0][i])

dq = adj * np.trapz(dqqe[t0:t1] - qe_ref[t0:t1], dx=86400)
dT = adj * np.trapz(dTqh[t0:t1] - qh_ref[t0:t1], dx=86400)
due = adj * np.trapz(duqe[t0:t1] - qe_ref[t0:t1], dx=86400)
duh = adj * np.trapz(duqh[t0:t1] - qh_ref[t0:t1], dx=86400)

print(dq)
print(dT)
print(due, duh, due+duh)

#  ──────────────────────────────────────────────────────────────────────────


