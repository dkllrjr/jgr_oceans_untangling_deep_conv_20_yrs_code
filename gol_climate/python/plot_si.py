##############################################################################
# By Doug Keller
# 
# This script plots the figure showing the difference betweent the control or 
# total stratification (SI) NEMO results and the seasonal SI results.
# Mistral events and important time stamps are overlaid for connectivity.
##############################################################################

import pickle
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import ticker
import matplotlib.dates as mdates
from glob import glob
import numpy as np
from matplotlib.patches import Rectangle

##############################################################################
# load data

data_paths = []
data_paths.append(glob('../data/GOL5/N-CONT*SI.pickle'))
data_paths.append(glob('../data/GOL5/N-SEAS*SI.pickle'))

for i, data_path in enumerate(data_paths):
    data_paths[i].sort()

data = []

for data_path in data_paths:
    data.append([])
    for path in data_path:
        with open(path, 'rb') as file:
            data[-1].append(pickle.load(file))

with open('../data/mistral_attributes_1993-2013.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

with open('../data/mlh_growth_dates.pickle', 'rb') as file:
    mlh_growth_dates = pickle.load(file)

##############################################################################
# plot

dc_years = np.array([1999, 2000, 2005, 2009, 2011, 2012, 2013])
years = np.arange(1994, 2014)

# Consider adding Mistral events

width, height = 14, 18

fsize = 18
tsize = 22

# SI plot

fig, ax = plt.subplots(10, 2, dpi=400, figsize=(width, height))
ax = ax.flatten('F')

twinax = []

for axis in ax:
    twinax.append(axis.twinx())

m = 0
rat_grd = []
avg_grd_dsis = []
avg_grd_siss = []
for i, (cont, seas) in enumerate(zip(data[0], data[1])):

    if years[i] in dc_years:

        mlhb = mlh_growth_dates[m][0]
        mlhe = mlh_growth_dates[m][1]

        tmlh = []
        for ti, tik in enumerate(cont['t']):
            if mlhb <= tik <= mlhe:
                tmlh.append(ti)

        avg_grd_dsi = np.mean(np.gradient((cont['si']-seas['si'])[tmlh]))
        avg_grd_sis = np.mean(np.gradient((seas['si'])[tmlh]))

        # print(avg_grd_dsi, avg_grd_sis)

        rat_grd.append(avg_grd_dsi/avg_grd_sis)
        avg_grd_dsis.append(avg_grd_dsi)
        avg_grd_siss.append(avg_grd_sis)

        m += 1

    p2, = twinax[i].plot(cont['t'], cont['si']-seas['si'], linestyle='--', alpha=0.75, color='tab:red', label='$\delta SI$')
    
    p0, = ax[i].plot(cont['t'], cont['si'], color='black', label='Control')
    p1, = ax[i].plot(seas['t'], seas['si'], color='tab:blue', label='Seasonal')
    
    # twinax[i].set_ylim(-1.15, 0.52)
    twinax[i].set_ylim(-2, 1.4)
    
    twinax[i].yaxis.label.set_color(p2.get_color())
    twinax[i].tick_params(axis='y', colors=p2.get_color(), labelsize=fsize-4)

    y1 = 3.4
    
    ax[i].set_ylim(0, y1)
    ax[i].set_xlim(cont['t'][0], cont['t'][-1])
    
    if years[i] in dc_years:
        ax[i].tick_params(axis='x', labelsize=fsize-4, labelcolor='tab:green')
        ax[i].tick_params(axis='both', labelsize=fsize-4)
    else:
        ax[i].tick_params(axis='both', labelsize=fsize-4)

    # xaxis stuff
    fmt_half_year = mdates.MonthLocator(interval=6)
    ax[i].xaxis.set_major_locator(fmt_half_year)
    
    fmt_month = mdates.MonthLocator()
    ax[i].xaxis.set_minor_locator(fmt_month)

    ax[i].xaxis.set_minor_formatter(mdates.DateFormatter('%m'))

    ax[i].grid(axis='x', which='minor', alpha=.5)
    ax[i].grid(axis='x', which='major', alpha=.5)

    # yaxis stuff
    ax[i].yaxis.set_minor_locator(ticker.AutoMinorLocator())
    
    ax[i].grid(axis='y', which='major', alpha=.5)

    if i < 10:
        ax[i].set_ylabel('$m^2/s^2$', fontsize=fsize)

    if i == 9 or i == 19:
        ax[i].set_xlabel('Time', fontsize=fsize)

    if i > 9:
        twinax[i].set_ylabel('$m^2/s^2$', fontsize=fsize)
    
    mweak = 0
    mstr = 0
    h0, h1 = 0, y1
    for j, event in enumerate(mistral_data['events']):
        if j == 0:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:green', alpha=.25, label='Mistral')
            p3 = rect
        else:
            rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:green', alpha=.25)
        # if mistral_data['strength'][j] == 0:
            # if mweak == 0:
                # rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:red', alpha=.25, label='Weak Mistral')
                # p3 = rect
                # mweak = 1
            # else:
                # rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:red', alpha=.25)
        # else:
            # if mstr == 0:
                # rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:blue', alpha=.25, label='Strong Mistral')
                # p4 = rect
                # mstr = 1
            # else:
                # rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:blue', alpha=.25)
        ax[i].add_patch(rect)


    if i == 0:
        # ax[i].legend(ncol=5, handles=[p0, p1, p2, p3, p4], fontsize=fsize-4, loc='upper right', bbox_transform=fig.transFigure, bbox_to_anchor=(0.95, 1))
        ax[i].legend(ncol=5, handles=[p0, p1, p2, p3], fontsize=fsize-4, loc='upper right', bbox_transform=fig.transFigure, bbox_to_anchor=(0.95, 1))

fig.suptitle('Gulf of Lion $SI$', fontsize=tsize, y=.99, x = 0.1, horizontalalignment='left')
fig.subplots_adjust(top=.97, left=0.05, right=.935, bottom=0.04, hspace=0.25, wspace=0.13)
# fig.tight_layout()
fig.savefig('../plots/si_gol_past_si_dsi.pdf')

print(dc_years)
print(avg_grd_dsis)
print(avg_grd_siss)
print(rat_grd)

with open('../data/mlh_si_gradients.pkl', 'wb') as file:
    save_data = {'years': dc_years, 'dsi': avg_grd_dsis, 'sis': avg_grd_siss, 'ratio': rat_grd, 'desc': 'averaged gradients during deep convection'}
    pickle.dump(save_data, file)

# ##############################################################################
# # SI1000m plot

# fig, ax = plt.subplots(10, 2, dpi=200, figsize=(width, height))
# ax = ax.flatten('F')

# twinax = []

# for axis in ax:
    # twinax.append(axis.twinx())

# for i, (cont, seas) in enumerate(zip(data[0], data[1])):

    # p2, = twinax[i].plot(cont['t'], cont['si1000']-seas['si1000'], linestyle='--', alpha=0.75, color='tab:red', label='$\delta SI_{1000}$')
    
    # p0, = ax[i].plot(cont['t'], cont['si1000'], color='black', label='Control')
    # p1, = ax[i].plot(seas['t'], seas['si1000'], color='tab:blue', label='Seasonal')

    
    # twinax[i].set_ylim(-0.73, 0.19)
    
    # twinax[i].yaxis.label.set_color(p2.get_color())
    # twinax[i].tick_params(axis='y', colors=p2.get_color(), labelsize=fsize-4)
    
    # ax[i].set_ylim(0, 2.8)
    # ax[i].set_xlim(cont['t'][0], cont['t'][-1])
    # ax[i].tick_params(axis='both', labelsize=fsize-4)

    # # xaxis stuff
    # fmt_half_year = mdates.MonthLocator(interval=6)
    # ax[i].xaxis.set_major_locator(fmt_half_year)
    
    # fmt_month = mdates.MonthLocator()
    # ax[i].xaxis.set_minor_locator(fmt_month)

    # ax[i].xaxis.set_minor_formatter(mdates.DateFormatter('%m'))

    # ax[i].grid(axis='x', which='minor', alpha=.5)
    # ax[i].grid(axis='x', which='major', alpha=.5)

    # # yaxis stuff
    # ax[i].yaxis.set_minor_locator(ticker.AutoMinorLocator())
    
    # ax[i].grid(axis='y', which='major', alpha=.5)

    # if i < 10:
        # ax[i].set_ylabel('$m^2/s^2$', fontsize=fsize)

    # if i == 9 or i == 19:
        # ax[i].set_xlabel('Time', fontsize=fsize)

    # if i > 9:
        # twinax[i].set_ylabel('$m^2/s^2$', fontsize=fsize)

    # h0, h1 = 0, 2.8
    # for j, event in enumerate(mistral_data['events']):
        # if j == 0:
            # rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:green', alpha=.25, label='Mistral')
            # p3 = rect
        # else:
            # rect = Rectangle((event[0], h0), event[-1] - event[0] + timedelta(days=1), h1, color='tab:green', alpha=.25)
        # ax[i].add_patch(rect)

    # if i == 0:
        # ax[i].legend(ncol=4, handles=[p0, p1, p2, p3], fontsize=fsize-4, loc='upper right', bbox_transform=fig.transFigure, bbox_to_anchor=(0.95, 1))

# fig.suptitle('Gulf of Lion $SI_{1000}$', fontsize=tsize, y=.99, x = 0.17, horizontalalignment='left')
# fig.subplots_adjust(top=.97, left=0.05, right=.925, bottom=0.04, hspace=0.25, wspace=0.17)
# fig.savefig('../plots/si_gol_past_si_dsi_1000.png')

# # dSI plot

# # fig, ax = plt.subplots(10, 2, dpi=200, figsize=(width, height))
# # ax = ax.flatten('F')


# # for i, (cont, seas) in enumerate(zip(data[0], data[1])):
    # # ax[i].plot(cont['t'], cont['si']-seas['si'], color='black')
    
    # # ax[i].set_ylim(-0.73, 0.19)
    # # ax[i].set_xlim(cont['t'][0], cont['t'][-1])
    # # ax[i].tick_params(axis='both', labelsize=fsize-4)

    # # # xaxis stuff
    # # fmt_half_year = mdates.MonthLocator(interval=6)
    # # ax[i].xaxis.set_major_locator(fmt_half_year)
    
    # # fmt_month = mdates.MonthLocator()
    # # ax[i].xaxis.set_minor_locator(fmt_month)

    # # ax[i].xaxis.set_minor_formatter(mdates.DateFormatter('%m'))

    # # ax[i].grid(axis='x', which='minor', alpha=.5)
    # # ax[i].grid(axis='x', which='major', alpha=.5)

    # # # yaxis stuff
    # # ax[i].yaxis.set_minor_locator(ticker.AutoMinorLocator())
    
    # # ax[i].grid(axis='y', which='major', alpha=.5)

    # # # if i == 0:
        # # # ax[i].legend(ncol=2, fontsize=fsize-4)

    # # if i < 10:
        # # ax[i].set_ylabel('$m^2/s^2$', fontsize=fsize)

    # # if i == 9 or i == 19:
        # # ax[i].set_xlabel('Time', fontsize=fsize)

# # fig.suptitle('Gulf of Lion $\delta SI$', fontsize=tsize, y=.99)
# # fig.tight_layout()
# # fig.savefig('../plots/si_gol_past_dsi.png')
