##############################################################################
# By Doug Keller
# 
# This script plots the figure showing the difference betweent the control or 
# total stratification (SI) NEMO results and the seasonal SI results.
# Mistral events and important time stamps are overlaid for connectivity.
##############################################################################

import pickle
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import ticker
import matplotlib.dates as mdates
from glob import glob
import latent_heat_control as lhc
import latent_heat_seasonal as lhs
import numpy as np
from scipy import signal
import pandas

##############################################################################
# load data

# data_paths = []
# data_paths.append(glob('../data/GOL/N-CONT*SI.pickle'))
# data_paths.append(glob('../data/GOL/N-SEAS*SI.pickle'))

# for i, data_path in enumerate(data_paths):
    # data_paths[i].sort()

# data = []

# for data_path in data_paths:
    # data.append([])
    # for path in data_path:
        # with open(path, 'rb') as file:
            # data[-1].append(pickle.load(file))

with open('../data/GOL1/NEMO_1993-2013_D.pickle', 'rb') as file:
    depth_data = pickle.load(file)

d = depth_data['d']

dc_years = np.array([1999, 2000, 2005, 2009, 2011, 2012, 2013])
years = np.arange(1994, 2014)

with open('../data/mistral_attributes_1993-2013.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

##############################################################################
# plot

# Consider adding Mistral events

width, height = 14, 18

fsize = 18
tsize = 22

# SI plot

fig, ax = plt.subplots(len(dc_years), 1, dpi=300, figsize=(width, height))
ax = ax.flatten('F')

i = -1

imp_dates = []

for j, (cont, seas) in enumerate(zip(lhc.mlh, lhs.mlh)):

    if years[j] in dc_years:

        i += 1

        ax[i].plot(lhc.thf[j], cont, color='black', label='Control')

        peaks, _ = signal.find_peaks(cont, height=1000, prominence=1000)
        abv250 = np.where(cont > 250)[0][0]

        beg = pandas.to_datetime(pandas.Timestamp(lhc.thf[j][abv250]))
        end = pandas.to_datetime(pandas.Timestamp(lhc.thf[j][peaks[0]]))

        imp_dates.append((beg, end))

        ax[i].plot(lhc.thf[j][peaks], cont[peaks], color='red', linestyle='', marker='o', label='Peaks')
        ax[i].plot(lhc.thf[j][abv250], cont[abv250], color='red', linestyle='', marker='*', label='>250')
        
        ax[i].set_ylim(0, d)
        ax[i].invert_yaxis()
        ax[i].set_xlim(lhc.thf[j][0], lhc.thf[j][-1])
        ax[i].tick_params(axis='both', labelsize=fsize-4)

        # xaxis stuff
        fmt_half_year = mdates.MonthLocator(interval=6)
        ax[i].xaxis.set_major_locator(fmt_half_year)
        
        fmt_month = mdates.MonthLocator()
        ax[i].xaxis.set_minor_locator(fmt_month)

        ax[i].xaxis.set_minor_formatter(mdates.DateFormatter('%m'))

        ax[i].grid(axis='x', which='minor', alpha=.5)
        ax[i].grid(axis='x', which='major', alpha=.5)

        # yaxis stuff
        ax[i].yaxis.set_minor_locator(ticker.AutoMinorLocator())
        
        ax[i].grid(axis='y', which='major', alpha=.5)

        if i < 10:
            ax[i].set_ylabel('$m$', fontsize=fsize)

        if i == 9 or i == 19:
            ax[i].set_xlabel('Time', fontsize=fsize)

        # if years[i] in dc_years:
            # ax[i].tick_params(axis='x', labelsize=fsize-4, labelcolor='tab:green')
        else:
            ax[i].tick_params(axis='x', labelsize=fsize-4)

fig.suptitle('Gulf of Lion $MLD$', fontsize=tsize, y=.99)
fig.tight_layout()
fig.savefig('../plots/mlh_gol_peaks.png')

with open('../data/mlh_growth_dates.pickle', 'wb') as file:
    pickle.dump(imp_dates, file)
