##############################################################################
# By Doug Keller
# 
# This script plots the figure showing the difference betweent the control or 
# total stratification (SI) NEMO results and the seasonal SI results.
# Mistral events and important time stamps are overlaid for connectivity.
##############################################################################

import pickle
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import ticker
import matplotlib.dates as mdates
from glob import glob
import numpy as np
import latent_heat_control as lhc
import latent_heat_seasonal as lhs

##############################################################################

def moving_mean(f,N):
    #Window = 2*N + 1
    mm = np.zeros(f.size)
    
    for i in range(f.size):
        if i < N:
            m = []
            for j in range(i+N+1):
                if np.isfinite(f[j]):
                    m.append(f[j])
            m = np.array(m)
            mm[i] = np.nanmean(m)
        elif i+N > f.size-1:
            m = []
            for j in range(i-N,f.size):
                if np.isfinite(f[j]):
                    m.append(f[j])
            m = np.array(m)
            mm[i] = np.nanmean(m)
        else:
            mm[i] = np.nanmean(f[i-N:i+N+1][np.where(np.isfinite(f[i-N:i+N+1]))[0]])
        
    return mm

##############################################################################
# load data

data_paths = []
data_paths.append(glob('../data/GOL5/N-CONT*SI.pickle'))
data_paths.append(glob('../data/GOL5/N-SEAS*SI.pickle'))

for i, data_path in enumerate(data_paths):
    data_paths[i].sort()

data = []

for data_path in data_paths:
    data.append([])
    for path in data_path:
        with open(path, 'rb') as file:
            data[-1].append(pickle.load(file))

dc_years = np.array([1999, 2000, 2005, 2009, 2011, 2012, 2013])
years = np.arange(1994, 2014)

##############################################################################
# plot

# Consider adding Mistral events

width, height = 14, 10

fsize = 18
tsize = 24

# SI plot

fig, ax = plt.subplots(2, 1, dpi=300, figsize=(width, height))
ax = ax.flatten('F')
N = 5

for i, qnet in enumerate(lhs.qnet):

    good_years = [1994, 2005]

    if years[i] in good_years:
        j = good_years.index(years[i])

        ax[j].plot(lhs.tda[i], moving_mean(qnet, N), color='tab:blue', label='$\dot{Q}_{net}$')
        ax[j].plot(lhs.tda[i], moving_mean(lhs.qsh[i], N), color='tab:red', linestyle=':', label='$\dot{Q}_H$')
        ax[j].plot(lhs.tda[i], moving_mean(lhs.qlh[i], N), color='tab:red', label='$\dot{Q}_E$')
        ax[j].plot(lhs.tda[i], moving_mean(lhs.qlw[i], N), color='tab:red', linestyle='--', label='$\dot{Q}_{LW}$')
        ax[j].plot(lhs.tda[i], moving_mean(lhs.qsw[i], N), color='k', label='$\dot{Q}_{SW}$')
        ax[j].plot(data[1][i]['t'], np.gradient(data[1][i]['si'])/86400*1e9, color='tab:green', linestyle='--', label='$\\frac{ \partial SI_S }{ \partial t } \\times 10^9$')
        
        ax[j].set_ylim(-400, 400)
        ax[j].set_xlim(lhs.tda[i][0], lhs.tda[i][-1])
        ax[j].tick_params(axis='both', labelsize=fsize-2)

        # xaxis stuff
        fmt_half_year = mdates.MonthLocator(interval=6)
        ax[j].xaxis.set_major_locator(fmt_half_year)
        
        fmt_month = mdates.MonthLocator()
        ax[j].xaxis.set_minor_locator(fmt_month)

        ax[j].xaxis.set_minor_formatter(mdates.DateFormatter('%m'))
        ax[j].tick_params(axis='both', which='minor', labelsize=fsize-4)

        ax[j].grid(axis='x', which='minor', alpha=.5)
        ax[j].grid(axis='x', which='major', alpha=.5)

        # yaxis stuff
        ax[j].yaxis.set_minor_locator(ticker.AutoMinorLocator())
        
        ax[j].grid(axis='y', which='major', alpha=.5)

        ax[j].set_ylabel('$W/m^2$', fontsize=fsize)

        # if i == 9:
            # ax[j].legend(ncol=6, bbox_to_anchor=(1, 1), bbox_transform=fig.transFigure, fontsize=fsize-4)

        # if i < 10:
            # ax[j].set_ylabel('$W/m^2$', fontsize=fsize)

        # if i == 9 or i == 19:
            # ax[j].set_xlabel('Time', fontsize=fsize)

        # if years[i] in dc_years:
            # ax[j].tick_params(axis='x', labelsize=fsize-4, labelcolor='tab:green')
        # else:
            # ax[j].tick_params(axis='x', labelsize=fsize-4)

ax[0].legend(ncol=6, fontsize=fsize-2)
ax[1].set_xlabel('Time', fontsize=fsize)

fig.suptitle('Seasonal sea surface fluxes', fontsize=tsize, y=.98)

text_x = 0.007
fig.text(text_x, .95, '(a)', fontweight='bold', fontsize=fsize)
fig.text(text_x, .495, '(b)', fontweight='bold', fontsize=fsize)

# fig.subplots_adjust(top=.96, left=0.08, right=.985, bottom=0.04, hspace=0.25, wspace=0.12)
fig.tight_layout()
fig.savefig('../plots/fluxes_gol_2012.png')
