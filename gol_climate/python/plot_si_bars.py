import pickle
from glob import glob
import numpy as np
from scipy import stats

import latent_heat_control as lhc
import latent_heat_seasonal as lhs

##############################################################################

data_paths = []
data_paths.append(glob('../data/GOL5/N-CONT*SI.pickle'))
data_paths.append(glob('../data/GOL5/N-SEAS*SI.pickle'))

for i, data_path in enumerate(data_paths):
    data_paths[i].sort()

data = []

for data_path in data_paths:
    data.append([])
    for path in data_path:
        with open(path, 'rb') as file:
            data[-1].append(pickle.load(file))

with open('../data/mistral_attributes.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

with open('../data/flux_contributions.pickle', 'rb') as file:
    flux = pickle.load(file)

import numpy as np
from datetime import datetime, timedelta

dc_years = np.array([1999, 2000, 2005, 2009, 2011, 2012, 2013])
years = np.arange(1994, 2014)
xyears = np.arange(1993, 2015)

ind_dc = []
dyears = []
ind_not_dc = []
for i, year in enumerate(years):
    if year in dc_years:
        ind_dc.append(i)
    else:
        ind_not_dc.append(i)


    dyears.append(datetime(year=year, month=1, day=1))


xdyears = []
for i, year in enumerate(xyears):
    xdyears.append(datetime(year=year, month=1, day=1))

ind_dc = np.array(ind_dc)
dyears = np.array(dyears)

si_init = []  # initial SI
si_max = []  # max control SI
si_s_max = []  # max seasonal SI
si_min = []  # min control SI
si_s_min = []  # min seasonal SI

dsi_dc = []  # delta SI at time of deep convection
si_s_dc = []  # seasonal SI at time of deep convection

dc_aft = []  # deep convection after seasonal SI minimum
dc_tog = []  # deep convection with seasonal SI minimum occurring about the same time
dc_bef = []  # deep convection before seasonal SI minimum

inddc = []
si_s_max_ind = []
si_s_min_ind = []
si_s_mindc = []

datedc = []
si_s_max_date = []

dc_dif = []
dc_dates = []

diff_dsi = []
diff_si_s = []
diff_dsi_s = []
diff_si_s_s = []

time = []

for i, (cont, seas) in enumerate(zip(data[0], data[1])):

    time.append(cont['t'])
    
    si_init.append(cont['si'][0])

    si_max.append(np.max(cont['si']))
    si_s_max.append(np.max(seas['si'][0:200]))

    si_min.append(np.min(cont['si']))
    si_s_min.append(np.min(seas['si']))

    dc_ind = cont['si'].argmin()
    si_s_ind = seas['si'][100::].argmin()  # min of seasonal
    si_s_ind += 100
    si_s_min_ind.append(si_s_ind)
    si_s_mindc.append(seas['si'][dc_ind])

    inddc.append(cont['si'].argmin())  # min of control
    datedc.append(cont['t'][inddc[-1]])  # date of min of control

    si_s_max_ind.append(seas['si'][0:180].argmax())
    si_s_max_date.append(seas['t'][si_s_max_ind[-1]])

    # determining before, after, or simultaneous destratification for delta SI and seasonal SI

    # print(dc_ind, si_s_ind)
    if dyears[i].year in dc_years:

        dc_dif.append(dc_ind - si_s_ind)
        dc_dates.append((cont['t'][dc_ind], cont['t'][si_s_ind]))

        print(cont['t'][dc_ind], cont['t'][si_s_ind])

        if dc_ind > si_s_ind:
            dc_aft.append(True)
            dc_tog.append(False)
            dc_bef.append(False)

        elif dc_ind == si_s_ind:
            dc_aft.append(False)
            dc_tog.append(True)
            dc_bef.append(False)
            
        else:
            dc_aft.append(False)
            dc_tog.append(False)
            dc_bef.append(True)

        dN = 45
        diff_dsi.append(np.mean(np.gradient((cont['si']-seas['si'])[dc_ind-dN:dc_ind+1])))
        diff_si_s.append(np.mean(np.gradient((seas['si'])[dc_ind-dN:dc_ind+1])))
        diff_dsi_s.append(np.mean(np.gradient((cont['si']-seas['si'])[si_s_ind-dN:si_s_ind+1])))
        diff_si_s_s.append(np.mean(np.gradient((seas['si'])[si_s_ind-dN:si_s_ind+1])))


    dsi_dc.append(cont['si'][dc_ind]-seas['si'][dc_ind])
    si_s_dc.append(seas['si'][dc_ind])

print(np.round(np.array(diff_dsi)/np.array(diff_si_s), 2))
print(np.round(np.array(diff_dsi_s)/np.array(diff_si_s_s), 2))

si_init = np.array(si_init)

si_s_max = np.array(si_s_max)

si_max = np.array(si_max)
si_min = np.array(si_min)

dsi_dc = np.array(dsi_dc)
si_s_dc = np.array(si_s_dc)
si_s_max_ind = np.array(si_s_max_ind)
si_s_mindc = np.array(si_s_mindc)

import matplotlib.pyplot as plt
import matplotlib.dates as mdates

beg = datetime(year=1993, month=6, day=1)
end = datetime(year=2013, month=6, day=1)

# all bar plots

fsize = 16
tsize = 22

# fig, ax = plt.subplots(5, 1, figsize=(12, 14), dpi=200)
# fig, ax = plt.subplots(4, 1, figsize=(12, 14), dpi=200)
fig, ax = plt.subplots(5, 1, figsize=(12, 16), dpi=200)

# seasonal si max and seasonal si min

ax[0].axhline(si_s_max.mean(), color='tab:purple', label='${SI_{S,max}}$')
ax[0].axhline(si_min.mean(), color='tab:olive', label='${SI_{min}}$')

bar_width = timedelta(days=100)
ax[0].bar(dyears - timedelta(days=60), si_min, width=bar_width, color='tab:olive', label='$SI_{min}$')
ax[0].bar(dyears[ind_dc]- timedelta(days=60), si_min[ind_dc], width=bar_width, color='none', hatch='//', label='Deep Conv.')

p = np.polyfit(years, si_s_max, 1)
print('slope','intercept')
print(p)
ax[0].plot(xdyears, np.polyval(p, xyears), color='tab:purple', linestyle=':')
s = stats.linregress(years, si_s_max)
print('Seasonal SI max stats', s)

p = np.polyfit(years, si_min, 1)
print('slope','intercept')
print(p)
ax[0].plot(xdyears, np.polyval(p, xyears), color='tab:olive', linestyle=':')
s = stats.linregress(years, si_min)
print('SI min stats', s)

ax[0].bar(dyears + timedelta(days=60), si_s_max, width=bar_width, color='tab:purple', label='$SI_{S,max}$')
ax[0].bar(dyears[ind_dc]+ timedelta(days=60), si_s_max[ind_dc], width=bar_width, color='none', hatch='//')

fmt_year = mdates.YearLocator()
ax[0].xaxis.set_major_locator(fmt_year)
ax[0].xaxis.set_major_formatter(mdates.DateFormatter('%Y'))

ax[0].set_xlim(beg, end)

ax[0].set_ylabel('$m^2/s^2$', fontsize=fsize)

order = [2, 4, 1, 0, 3]
handles, labels = ax[0].get_legend_handles_labels()
ax[0].legend([handles[idx] for idx in order],[labels[idx] for idx in order], ncol=len(order))

ax[0].set_title('Stratification Min/Max', fontsize=tsize)

# seasonal and delta si contribution

bar_width = timedelta(days=200)
ax[1].bar(dyears, si_s_max-si_s_dc, width=bar_width, color='tab:blue', label='$\Omega SI_{S}$')
ax[1].bar(dyears, -dsi_dc, width=bar_width, color='tab:green', bottom=si_s_max-si_s_dc, label='$\Omega \\delta SI$')
ax[1].bar(dyears, si_s_max - (si_s_max-si_s_dc) + dsi_dc, width=bar_width, color='tab:red', bottom=(si_s_max-si_s_dc) - dsi_dc, label='Leftover $SI$')

ax[1].bar(dyears[ind_dc], (si_s_max-si_s_dc)[ind_dc], width=bar_width, color='none', hatch='//', label='Deep Conv.')
ax[1].bar(dyears[ind_dc], -1*dsi_dc[ind_dc], width=bar_width, color='none', bottom=(si_s_max-si_s_dc)[ind_dc], hatch='//')
ax[1].axhline((si_s_max-si_s_dc).mean(), color='tab:blue', label='${\Omega SI_{S}}$')

p = np.polyfit(years, si_s_max-si_s_dc, 1)
print(p)
ax[1].plot(xdyears, np.polyval(p, xyears), color='tab:blue', linestyle=':')
s = stats.linregress(years, si_s_max-si_s_dc)
print('Seasonal SI contribution stats', s)

p = np.polyfit(years, -dsi_dc, 1)
print(p)
s = stats.linregress(years, -dsi_dc)
print('Mistral SI contribution stats', s)
# ax[1].plot(xdyears, np.polyval(p, xyears), color='tab:green', linestyle=':')

fmt_year = mdates.YearLocator()
ax[1].xaxis.set_major_locator(fmt_year)
ax[1].xaxis.set_major_formatter(mdates.DateFormatter('%Y'))

ax[1].set_xlim(beg, end)

ax[1].set_ylabel('$m^2/s^2$', fontsize=fsize)

order = [3, 2, 1, 0, 4]
handles, labels = ax[1].get_legend_handles_labels()
ax[1].legend([handles[idx] for idx in order],[labels[idx] for idx in order], ncol=len(order))

ax[1].set_title('Destratification Contributions', fontsize=tsize)

# Normalized seasonal and delta si contribution

ax[2].axhline(((si_s_max-si_s_dc)/si_s_max).mean(), color='tab:blue', label='${\Omega SI_{S}}$')

bar_width = timedelta(days=200)
# ax[2].bar(dyears, 1, width=bar_width, color='tab:red', label='Leftover $SI$')
ax[2].bar(dyears, (si_s_max-si_s_dc)/si_s_max, width=bar_width, color='tab:blue', label='$\Omega SI_{S}$')
ax[2].bar(dyears, -dsi_dc/si_s_max, width=bar_width, color='tab:green', bottom=(si_s_max-si_s_dc)/si_s_max, label='$\Omega \\delta SI$')

ax[2].bar(dyears[ind_dc], ((si_s_max-si_s_dc)/si_s_max)[ind_dc], width=bar_width, color='none', hatch='//', label='Deep Conv.')
ax[2].bar(dyears[ind_dc], (-1*dsi_dc/si_s_max)[ind_dc], width=bar_width, color='none', bottom=((si_s_max-si_s_dc)/si_s_max)[ind_dc], hatch='//')

fmt_year = mdates.YearLocator()
ax[2].xaxis.set_major_locator(fmt_year)
ax[2].xaxis.set_major_formatter(mdates.DateFormatter('%Y'))

ax[2].set_xlim(beg, end)
ax[2].set_ylim(0, 1)

# ax[2].set_ylabel('%', fontsize=fsize)
order = [2, 1, 0, 3]
handles, labels = ax[2].get_legend_handles_labels()
ax[2].legend([handles[idx] for idx in order],[labels[idx] for idx in order], ncol=2, loc='upper left')

ax[2].set_title('Normalized Destratification Contributions $(\cdot/SI_{S,max}$)', fontsize=tsize)

# delta SI bar plot

ax[3].axhline(((-dsi_dc)/si_s_max).mean(), color='tab:green', label='${\Omega \delta SI}$')

bar_width = timedelta(days=200)
ax[3].bar(dyears, -dsi_dc/si_s_max, width=bar_width, color='tab:green', label='$\Omega \\delta SI$')

ax[3].bar(dyears[ind_dc], (-1*dsi_dc/si_s_max)[ind_dc], width=bar_width, color='none', hatch='//', label='Deep Conv.')

fmt_year = mdates.YearLocator()
ax[3].xaxis.set_major_locator(fmt_year)
ax[3].xaxis.set_major_formatter(mdates.DateFormatter('%Y'))

ax[3].set_xlim(beg, end)

# ax[3].set_ylabel('%', fontsize=fsize)
order = [1, 0, 2]
handles, labels = ax[3].get_legend_handles_labels()
ax[3].legend([handles[idx] for idx in order],[labels[idx] for idx in order], ncol=len(order), loc='upper left')

ax[3].set_title('Normalized Mistral Destratification Contribution $(\cdot/SI_{S,max}$)', fontsize=tsize)

# separating bars by contribution

# bar_width = timedelta(days=100)
# ax[4].bar(dyears - timedelta(days=60), flux['rsw'] + si_init, width=bar_width)
# ax[4].bar(dyears + timedelta(days=60), flux['rlw'], width=bar_width, bottom=flux['rsw'] + si_init)
# ax[4].bar(dyears + timedelta(days=60), flux['rsh'], width=bar_width, bottom=flux['rsw'] + flux['rlw'] + si_init)
# ax[4].bar(dyears + timedelta(days=60), flux['rlh'], width=bar_width, bottom=flux['rsw'] + flux['rlw'] + flux['rsh'] + si_init)
# # ax[4].bar(dyears, flux['rlh'], width=bar_width, bottom=flux['rsw'] + flux['rlw'] + flux['rsh'])

# fmt_year = mdates.YearLocator()
# ax[4].xaxis.set_major_locator(fmt_year)
# ax[4].xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
# ax[4].set_xlim(beg, end)


si_norm = (si_s_max - si_s_dc) - dsi_dc
ax[4].axhline(((si_s_max-si_s_dc)/si_norm).mean(), color='tab:blue', label='${\Omega SI_{S}}$')

bar_width = timedelta(days=200)
ax[4].bar(dyears, (si_s_max-si_s_dc)/si_norm, width=bar_width, color='tab:blue', label='$\Omega SI_{S}$')
ax[4].bar(dyears, -dsi_dc/si_norm, width=bar_width, color='tab:green', bottom=(si_s_max-si_s_dc)/si_norm, label='$\Omega \\delta SI$')

ax[4].bar(dyears[ind_dc], ((si_s_max-si_s_dc)/si_norm)[ind_dc], width=bar_width, color='none', hatch='//', label='Deep Conv.')
ax[4].bar(dyears[ind_dc], (-1*dsi_dc/si_norm)[ind_dc], width=bar_width, color='none', bottom=((si_s_max-si_s_dc)/si_norm)[ind_dc], hatch='//')

fmt_year = mdates.YearLocator()
ax[4].xaxis.set_major_locator(fmt_year)
ax[4].xaxis.set_major_formatter(mdates.DateFormatter('%Y'))

ax[4].set_xlim(beg, end)
ax[4].set_ylim(0, 1)

# ax[2].set_ylabel('%', fontsize=fsize)
order = [2, 1, 0, 3]
handles, labels = ax[2].get_legend_handles_labels()
ax[4].legend([handles[idx] for idx in order],[labels[idx] for idx in order], ncol=4, loc='upper left')

ax[4].set_title('Normalized Destratification Contributions $(\cdot/(\\Omega SI_S + \\Omega \\delta SI)$)', fontsize=tsize)


for axis in ax:
    axis.tick_params(axis='x', labelsize=fsize-3)
    axis.tick_params(axis='y', labelsize=fsize-2)

# textx = 0.01
# fig.text(textx, 0.975, '(a)', fontweight='bold', fontsize=fsize+2)
# fig.text(textx, 0.728, '(b)', fontweight='bold', fontsize=fsize+2)
# fig.text(textx, 0.485, '(c)', fontweight='bold', fontsize=fsize+2)
# fig.text(textx, 0.235, '(d)', fontweight='bold', fontsize=fsize+2)

textx = 0.01
fig.text(textx, 0.975, '(a)', fontweight='bold', fontsize=fsize+2)
fig.text(textx, 0.77, '(b)', fontweight='bold', fontsize=fsize+2)
fig.text(textx, 0.585, '(c)', fontweight='bold', fontsize=fsize+2)
fig.text(textx, 0.375, '(d)', fontweight='bold', fontsize=fsize+2)
fig.text(textx, 0.19, '(e)', fontweight='bold', fontsize=fsize+2)

fig.tight_layout()
fig.subplots_adjust(left=0.07)

fig.savefig('../plots/si_bars.pdf')
fig.savefig('../plots/si_bars.png')

##############################################################################
# saving preconditioning period information

import pandas

begd = []
endd = []
diffd = []
for i, d in enumerate(datedc):
    endd.append(str(d)[0:19])
    beg = datetime(1993+i, 9, 1, 12)
    begd.append(beg.isoformat())

    beg = np.datetime64(beg)
    dif = np.timedelta64(d-beg, 'D')
    diffd.append(str(dif))

dfdata = {'Prec. Begin': begd, 'Prec. End': endd, 'Prec. No. of days': diffd }
dfdata = pandas.DataFrame(dfdata)

dfdata.to_csv('../data/prec_periods_1993-2013.csv')

##############################################################################

si_data = {'ind_dc': ind_dc, 'si_s': si_s_max-si_s_dc, 'dsi': -dsi_dc, 'si_max': si_s_max}

with open('../data/si_contributions.pickle', 'wb') as file:
    pickle.dump(si_data, file)

##############################################################################
# normalized contributions

print('normalized')

# mean
print(np.nanmean((-dsi_dc/si_s_max)[ind_dc]))
print(np.nanmean(((si_s_max-si_s_dc)/si_s_max)[ind_dc]))
print()

print(np.nanmean((-dsi_dc/si_s_max)[ind_not_dc]))
print(np.nanmean(((si_s_max-si_s_dc)/si_s_max)[ind_not_dc]))
print()

print(np.nanmean((-dsi_dc/si_s_max)))
print(np.nanmean(((si_s_max-si_s_dc)/si_s_max)))

print()
print()

# std
print(np.nanstd((-dsi_dc/si_s_max)[ind_dc]))
print(np.nanstd(((si_s_max-si_s_dc)/si_s_max)[ind_dc]))
print()

print(np.nanstd((-dsi_dc/si_s_max)[ind_not_dc]))
print(np.nanstd(((si_s_max-si_s_dc)/si_s_max)[ind_not_dc]))
print()

print(np.nanstd((-dsi_dc/si_s_max)))
print(np.nanstd(((si_s_max-si_s_dc)/si_s_max)))

print()
print('max')

# max
print(np.max((-dsi_dc/si_s_max)[ind_dc]))
print(np.max(((si_s_max-si_s_dc)/si_s_max)[ind_dc]))
print()

print(np.max((-dsi_dc/si_s_max)[ind_not_dc]))
print(np.max(((si_s_max-si_s_dc)/si_s_max)[ind_not_dc]))
print()

print(np.max((-dsi_dc/si_s_max)))
print(np.max(((si_s_max-si_s_dc)/si_s_max)))

print()
print('min')

# min
print(np.min((-dsi_dc/si_s_max)[ind_dc]))
print(np.min(((si_s_max-si_s_dc)/si_s_max)[ind_dc]))
print()

print(np.min((-dsi_dc/si_s_max)[ind_not_dc]))
print(np.min(((si_s_max-si_s_dc)/si_s_max)[ind_not_dc]))
print()

print(np.min((-dsi_dc/si_s_max)))
print(np.min(((si_s_max-si_s_dc)/si_s_max)))

print()
print()

##############################################################################
# non normalized contributions

print('non normalized')

# mean
print('mean')
print(np.nanmean((-dsi_dc)[ind_dc]))
print(np.nanmean(((si_s_max-si_s_dc))[ind_dc]))
print()

print(np.nanmean((-dsi_dc)[ind_not_dc]))
print(np.nanmean(((si_s_max-si_s_dc))[ind_not_dc]))
print()

print(np.nanmean((-dsi_dc)))
print(np.nanmean(((si_s_max-si_s_dc))))

print()
print()

# std
print('std')
print(np.nanstd((-dsi_dc)[ind_dc]))
print(np.nanstd(((si_s_max-si_s_dc))[ind_dc]))
print()

print(np.nanstd((-dsi_dc)[ind_not_dc]))
print(np.nanstd(((si_s_max-si_s_dc))[ind_not_dc]))
print()

print(np.nanstd((-dsi_dc)))
print(np.nanstd(((si_s_max-si_s_dc))))

print()
print('max')

# max
print(np.max((-dsi_dc)[ind_dc]))
print(np.max(((si_s_max-si_s_dc))[ind_dc]))
print()

print(np.max((-dsi_dc)[ind_not_dc]))
print(np.max(((si_s_max-si_s_dc))[ind_not_dc]))
print()

print(np.max((-dsi_dc)))
print(np.max(((si_s_max-si_s_dc))))

print()
print('min')

# min
print(np.min((-dsi_dc)[ind_dc]))
print(np.min(((si_s_max-si_s_dc))[ind_dc]))
print()

print(np.min((-dsi_dc)[ind_not_dc]))
print(np.min(((si_s_max-si_s_dc))[ind_not_dc]))
print()

print(np.min((-dsi_dc)))
print(np.min(((si_s_max-si_s_dc))))

print()
print()

##############################################################################
# normalized contributions

print('normalized to destratification experienced')

# mean
print('mean')
print(np.nanmean((-dsi_dc/si_norm)[ind_dc]))
print(np.nanmean(((si_s_max-si_s_dc)/si_norm)[ind_dc]))
print()

print(np.nanmean((-dsi_dc/si_norm)[ind_not_dc]))
print(np.nanmean(((si_s_max-si_s_dc)/si_norm)[ind_not_dc]))
print()

print(np.nanmean((-dsi_dc)/si_norm))
print(np.nanmean(((si_s_max-si_s_dc)/si_norm)))

print()
print()

# std
print('std')
print(np.nanstd((-dsi_dc/si_norm)[ind_dc]))
print(np.nanstd(((si_s_max-si_s_dc)/si_norm)[ind_dc]))
print()

print(np.nanstd((-dsi_dc/si_norm)[ind_not_dc]))
print(np.nanstd(((si_s_max-si_s_dc)/si_norm)[ind_not_dc]))
print()

print(np.nanstd((-dsi_dc)/si_norm))
print(np.nanstd(((si_s_max-si_s_dc)/si_norm)))

print()
print('max')

# max
print(np.max((-dsi_dc/si_norm)[ind_dc]))
print(np.max(((si_s_max-si_s_dc)/si_norm)[ind_dc]))
print()

print(np.max((-dsi_dc/si_norm)[ind_not_dc]))
print(np.max(((si_s_max-si_s_dc)/si_norm)[ind_not_dc]))
print()

print(np.max((-dsi_dc/si_norm)))
print(np.max(((si_s_max-si_s_dc)/si_norm)))

print()
print('min')

# min
print(np.min((-dsi_dc/si_norm)[ind_dc]))
print(np.min(((si_s_max-si_s_dc)/si_norm)[ind_dc]))
print()

print(np.min((-dsi_dc/si_norm)[ind_not_dc]))
print(np.min(((si_s_max-si_s_dc)/si_norm)[ind_not_dc]))
print()

print(np.min((-dsi_dc/si_norm)))
print(np.min(((si_s_max-si_s_dc)/si_norm)))

print()
print()

##############################################################################
# si s max

print('si s max')

# mean
print('mean')
print(np.nanmean((si_s_max)[ind_dc]))
print(np.nanmean((si_s_max)[ind_not_dc]))
print(np.nanmean((si_s_max)))

print()
print('std')

# std
print(np.nanstd((si_s_max)[ind_dc]))
print(np.nanstd((si_s_max)[ind_not_dc]))
print(np.nanstd((si_s_max)))

print()
print('max')

# max
print(np.max((si_s_max)[ind_dc]))
print(np.max((si_s_max)[ind_not_dc]))
print(np.nanmax((si_s_max)))

print()
print('min')

# min
print(np.min((si_s_max)[ind_dc]))
print(np.min((si_s_max)[ind_not_dc]))
print(np.nanmin((si_s_max)))
