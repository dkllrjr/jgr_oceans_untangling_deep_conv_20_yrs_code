import pickle
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import ticker
import matplotlib.dates as mdates
from glob import glob
import numpy as np
from matplotlib.patches import Rectangle
import pandas
import plot_si_bars as psb
import view_mistral_types as vmt

##############################################################################
# load data

dfmist = pandas.read_csv('../data/mistral_intensity_clusters_1993-2013_precond.csv', index_col=0)

##############################################################################
# plot

dc_years = np.array([1999, 2000, 2005, 2009, 2011, 2012, 2013])
years = np.arange(1994, 2014)

ind_dc = []
dyears = []
for i, year in enumerate(years):
    if year in dc_years:
        ind_dc.append(i)

    dyears.append(datetime(year=year, month=1, day=1))

ind_dc = np.array(ind_dc)
dyears = np.array(dyears)

##############################################################################

ind = []

years -= 1

for year in years:
    
    ind.append([])

    beg = datetime(year, 7, 1, 12)
    end = datetime(year+1, 6, 30, 12)

    for j, date in enumerate(dfmist.Date):

        if beg <= datetime.fromisoformat(date) <= end:
            ind[-1].append(j)

year += 1

##############################################################################

# all bar plots

fsize = 18
tsize = 24

fig, ax = plt.subplots(5, 4, figsize=(16, 20), dpi=300)

ax = ax.flatten('F')

# seasonal si max and seasonal si min

bins = np.arange(-1, 2) + .5

strength = ['Weak', 'Strong']

for i, axis in enumerate(ax):

    ws = np.hstack(np.array(dfmist['Boolean'])[ind[i]])

    # counts, bins, _ = axis.hist(ws, bins=bins, rwidth=.9)
    counts, bins = np.histogram(ws, bins=bins)

    bars = np.append(counts, np.sum(counts))

    colors = ['tab:red', 'tab:blue', 'black']

    axis.bar(strength + ['Total'], bars, width=0.9, color=colors)

    # axis.set_xticks([0, 1])
    # axis.set_xticklabels(strength)
    
    # print(max(bars))

    if years[i] in dc_years:
        axis.set_title(str(years[i]) + '-' + str(years[i]+1), fontsize=fsize, color='tab:green')
    else:
        axis.set_title(str(years[i]) + '-' + str(years[i]+1), fontsize=fsize, color='k')

    axis.set_ylim(0, 75)

    axis.tick_params(axis='x', labelsize=fsize-4)
    axis.tick_params(axis='y', labelsize=fsize-4)

fig.suptitle('Mistral Weak/Strong per Preconditioning Period', y=.99, fontsize=tsize)

fig.tight_layout()

# fig.savefig('../plots/mistral_weak_strong_bars.png')

##############################################################################

# all bar plots

fsize = 18
tsize = 24

fig, ax = plt.subplots(2, 1, figsize=(16, 8), dpi=300)

bins = np.arange(-1, 2) + .5

strength = ['Weak', 'Strong']

weak = []
strong = []
total = []

wk = []
strn = []
tot = []

dc = np.array([])
nondc = np.array([])

dc_days = []
nondc_days = []

for i, year in enumerate(years):

    ws = np.hstack(np.array(dfmist['Boolean'])[ind[i]])

    if 1994+i in dc_years:
        print(i)
        dc = np.append(dc, ws)
        dc_days.append(vmt.precond_days[i])

    else:
        nondc = np.append(nondc, ws)
        nondc_days.append(vmt.precond_days[i])

    # counts, bins, _ = axis.hist(ws, bins=bins, rwidth=.9)
    counts, bins = np.histogram(ws, bins=bins)

    bars = np.append(counts, np.sum(counts))

    wk.append(bars[0])
    strn.append(bars[1])
    tot.append(bars[2])

    bars = np.append(counts, np.sum(counts))/np.sum(counts)
    weak.append(bars[0])
    strong.append(bars[1])
    total.append(bars[2])

    colors = ['tab:red', 'tab:blue', 'black']

    # if years[i] in dc_years:
        # axis.set_title(str(years[i]) + '-' + str(years[i]+1), fontsize=fsize, color='tab:green')
    # else:
        # axis.set_title(str(years[i]) + '-' + str(years[i]+1), fontsize=fsize, color='k')

dc_days = np.sum(dc_days)
nondc_days = np.sum(nondc_days)

wk = np.array(wk)
weak = np.array(weak)
strn = np.array(strn)
strong = np.array(strong)
tot = np.array(tot)
total = np.array(total)

bar_width = timedelta(days=60)
ax[0].bar(psb.dyears - timedelta(days=70), wk, width=bar_width, color='tab:red')
ax[0].bar(psb.dyears, strn, width=bar_width, color='tab:blue')
ax[0].bar(psb.dyears + timedelta(days=70), tot, width=bar_width, color='k')
ax[0].bar(psb.dyears[psb.ind_dc] - timedelta(days=70), wk[psb.ind_dc], width=bar_width, color='none', hatch='//')
ax[0].bar(psb.dyears[psb.ind_dc], strn[psb.ind_dc], width=bar_width, color='none', hatch='//')

bar_width = timedelta(days=110)
ax[1].bar(psb.dyears - timedelta(days=60), weak, width=bar_width, color='tab:red')
ax[1].bar(psb.dyears + timedelta(days=60), strong, width=bar_width, color='tab:blue')
ax[1].bar(psb.dyears[psb.ind_dc]- timedelta(days=60), weak[psb.ind_dc], width=bar_width, color='none', hatch='//')
ax[1].bar(psb.dyears[psb.ind_dc] + timedelta(days=60), strong[psb.ind_dc], width=bar_width, color='none', hatch='//')
# ax[1].bar(psb.dyears + timedelta(days=60), total, width=bar_width, color='k')

for i, axis in enumerate(ax):

    fmt_year = mdates.YearLocator()
    axis.xaxis.set_major_locator(fmt_year)
    axis.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))

    axis.set_xlim(psb.beg, psb.end)

    axis.set_ylim(0, 1.)

    axis.tick_params(axis='x', labelsize=fsize-4)
    axis.tick_params(axis='y', labelsize=fsize-4)

ax[0].set_title('Mistral Weak/Strong per Preconditioning Period', fontsize=tsize)

ax[1].set_title('Normalized Mistral Weak/Strong per Preconditioning Period', fontsize=tsize)

ax[0].set_ylim(0, 75)
ax[1].set_ylim(0, 1.05)

# fig.suptitle('Normalized Mistral Weak/Strong per Preconditioning Period', y=.99, fontsize=tsize)

fig.tight_layout()

# fig.savefig('../plots/normalized_mistral_weak_strong_bars.png')

##############################################################################
# cumulative mistrals

fig, ax = plt.subplots(2, 2, figsize=(12, 12), dpi=300)

ax = ax.flatten()

bins = [-.5, .5, 1.5]

strength += ['Total']

dcounts, _ = np.histogram(dc, bins=bins)
dcounts = np.append(dcounts, np.sum(dcounts))

ndcounts, _ = np.histogram(nondc, bins=bins)
ndcounts = np.append(ndcounts, np.sum(ndcounts))

dcheights = dcounts/dc_days
ndcheights = ndcounts/nondc_days

ax[0].bar(strength, dcheights, color=colors)
ax[1].bar(strength, ndcheights, color=colors)

for i, h in enumerate(dcheights):
    ax[0].text(i, h + .005, str(round(h*100, 1)) + ' %', ha='center', fontsize=fsize)

for i, h in enumerate(ndcheights):
    ax[1].text(i, h + .005, str(round(h*100, 1)) + ' %', ha='center', fontsize=fsize)

for i, axis in enumerate(ax[0:2]):
    
    # axis.set_xlim(0.5, 16.5)
    axis.set_ylim(0, .375)

for i, axis in enumerate(ax[2:4]):
    
    # axis.set_xlim(0.5, 16.5)
    axis.set_ylim(0, .68)

for i, axis in enumerate(ax):
    
    # axis.set_xlim(0.5, 16.5)
    axis.tick_params(axis='x', labelsize=fsize-4)
    axis.tick_params(axis='y', labelsize=fsize-4)

    axis.grid(which='both', axis='y', alpha=.75)
    axis.set_axisbelow(True)
    
rel_dc = dcheights[0:2]/dcheights[2]
rel_ndc = ndcheights[0:2]/ndcheights[2]
rel_str = strength[0:2]
rel_col = colors[0:2]

ax[2].bar(rel_str, rel_dc, color=rel_col)
ax[3].bar(rel_str, rel_ndc, color=rel_col)

for i, h in enumerate(rel_dc):
    ax[2].text(i, h + .01, str(round(h*100, 1)) + ' %', ha='center', fontsize=fsize)

for i, h in enumerate(rel_ndc):
    ax[3].text(i, h + .01, str(round(h*100, 1)) + ' %', ha='center', fontsize=fsize)

ax[0].set_title('Deep convection years', pad=10, fontsize=tsize)
ax[1].set_title('Non-deep convection years', pad=10, fontsize=tsize)
ax[2].set_title('Deep convection years', pad=10, fontsize=tsize)
ax[3].set_title('Non-deep convection years', pad=10, fontsize=tsize)

fig.suptitle('Mistral day percentage of preconditioning days', fontsize=tsize+4, y = .985)
fig.text(0.5, 0.472, 'Relative percentage of Weak and Strong Mistral days', va='center', ha='center', fontsize=tsize+4)

fig.tight_layout(h_pad=6)

textx = 0.01
fig.text(textx, 0.97, '(a)', fontweight='bold', fontsize=fsize+2)
fig.text(textx, 0.475, '(b)', fontweight='bold', fontsize=fsize+2)

fig.savefig('../plots/mistral_weak_strong_dc_nondc.png')
