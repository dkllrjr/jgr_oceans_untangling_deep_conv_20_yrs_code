from scipy import stats
import numpy as np
import matplotlib.pyplot as plt

import plot_si_bars as psb

#  ──────────────────────────────────────────────────────────────────────────

t = np.array([], dtype=np.datetime64)
cont = []
seas = []
for i, datai in enumerate(psb.data[0]):
    t = np.append(t, datai['t'])
    cont = np.append(cont, datai['si'])
    seas = np.append(seas, psb.data[1][i]['si'])

#  ──────────────────────────────────────────────────────────────────────────

x = np.arange(len(t))
out_c = stats.linregress(x, cont)
out_s = stats.linregress(x, seas)

print(out_c)
print(out_s)

y = np.zeros(len(t))
out_test = stats.linregress(x, y)
