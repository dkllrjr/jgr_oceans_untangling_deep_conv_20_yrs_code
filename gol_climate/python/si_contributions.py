from plot_si_bars import *

##############################################################################
# important params

IDC = ind_dc  # index for deep convection years
DYEARS = dyears  # years
DSI = -dsi_dc  # anomaly contribution
DSIN = -dsi_dc/si_s_max  # anomaly contribution normalized
SIS = si_s_max-si_s_dc  # seasonal contribution
SISN = SIS/si_s_max  # seasonal contribution normalized
SISMAX = si_s_max  # seasonal max stratification
LEFTSI = si_s_max - (si_s_max-si_s_dc) + dsi_dc  # leftover SI
LEFTSIN = LEFTSI/SISMAX  # leftover SI normalized

std_DSI = np.std(DSI)
std_DSIN = np.std(DSIN)
std_SIS = np.std(SIS)
std_SISN = np.std(SISN)
std_SISMAX = np.std(SISMAX)
std_LEFTSI = np.std(LEFTSI)
std_LEFTSIN = np.std(LEFTSIN)

mu_DSI = np.mean(DSI)
mu_DSIN = np.mean(DSIN)
mu_SIS = np.mean(SIS)
mu_SISN = np.mean(SISN)
mu_SISMAX = np.mean(SISMAX)
mu_LEFTSI = np.std(LEFTSI)
mu_LEFTSIN = np.std(LEFTSIN)

##############################################################################
# saving preconditioning period information

dfdata = {'deep_convection_index': IDC, 'years': DYEARS, 'anomaly': DSI, 'anomaly_normalized': DSIN, 'seasonal': SIS, 'seasonal_normalized': SISN, 'seasonal_max': SISMAX, 'leftover': LEFTSI, 'leftover_normalized': LEFTSIN}

with open('../data/destratification_contributions.pickle', 'wb') as file:
    pickle.dump(dfdata, file)

