import pickle
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np
import latent_heat_control as lhc
import latent_heat_seasonal as lhs
from datetime import datetime, timedelta

#  ──────────────────────────────────────────────────────────────────────────

overall = -1
t = lhc.ta[overall]

fsize = 18

fig = plt.figure(figsize=(18, 10), dpi=300)

N = 4
ax = []
for i in range(N):
    ax.append(fig.add_subplot(N, 2, i*2+1))

ax.append(fig.add_subplot(4, 2, 2))
ax.append(fig.add_subplot(4, 2, (4, 8), projection='polar'))

fig.suptitle('WRF Filtered Variables', fontsize=fsize+6, y = .99)

colors = ['black', 'tab:blue']
labels = ['Control', 'Seasonal']
alphas = [1, 1]
linestyles = ['-', '--']
ylabels = ['$q$ $kg/kg$', '$T$ $^\circ C$', '$u$ $m/s$', '$v$ $m/s$']

# T, q, u, v
tmp = {'control':
       {'T': lhc.Ta[overall], 'q': lhc.qa[overall], 'u': lhc.u[overall], 'v': lhc.v[overall]},
       'seasonal':
       {'T': lhs.Ta[overall], 'q': lhs.qa[overall], 'u': lhs.u[overall], 'v': lhs.v[overall]}}

for i, key in enumerate(tmp['control'].keys()):
    for j, top_key in enumerate(tmp.keys()):
        ax[i].plot(t, tmp[top_key][key], color=colors[j], label=labels[j], linestyle=linestyles[j], alpha=alphas[j])

# ws
for i, ws in enumerate([lhc.ws[overall], lhs.ws[overall]]):
    ax[4].plot(t, ws, color=colors[i], linestyle=linestyles[i], alpha=alphas[i])
    ax[4].set_ylabel('Wind Speed $m/s$', fontsize=fsize)

# wd
for i, theta in enumerate([lhc.wd[overall], lhs.wd[overall]]):
    alphas = [.8, .7]
    hist, bins = np.histogram(theta, bins=60, range=(-180, 180), density=True)
    bars = hist * 100
    bar_thickness = np.diff(bins)[0]
    bar_locs = bins[:-1] + bar_thickness/2

    ax[5].bar(np.deg2rad(bar_locs), bars, width=np.deg2rad(bar_thickness), color=colors[i], alpha=alphas[i])

for axis in ax:
    axis.tick_params(axis="x", labelsize=fsize-3)
    axis.tick_params(axis="y", labelsize=fsize-3)

for i, key in enumerate(ylabels):
    ax[i].set_ylabel(ylabels[i], fontsize = fsize)

for axis in ax[0:-1]:
    axis.set_xlim(t[0], t[-1])
    axis.xaxis.set_major_locator(mdates.MonthLocator(interval=2))

ax[0].legend(ncol=2, fontsize=fsize-4)
ax[3].set_xlabel('Time', fontsize = fsize)
ax[5].set_xlabel('Wind Direction Density %', fontsize=fsize)

wpad = .06
fig.subplots_adjust(hspace=0.4, wspace=.11, top=.925, bottom=.08, right=1-wpad+.05, left=wpad)

textx = 0.003
fig.text(textx, 0.935, '(a)', fontweight='bold', fontsize=fsize)
fig.text(textx, 0.7, '(b)', fontweight='bold', fontsize=fsize)
fig.text(textx, 0.465, '(c)', fontweight='bold', fontsize=fsize)
fig.text(textx, 0.24, '(d)', fontweight='bold', fontsize=fsize)

textx = 0.54
fig.text(textx, 0.935, '(e)', fontweight='bold', fontsize=fsize)
fig.text(textx, 0.7, '(f)', fontweight='bold', fontsize=fsize)

fig.savefig('../plots/wrf_filtered_ex.png')

#  ──────────────────────────────────────────────────────────────────────────

with open('../data/mistral_attributes_1993-2013.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

#  ──────────────────────────────────────────────────────────────────────────

fig, ax = plt.subplots(2, 1, figsize=(18, 8), dpi=300)

fig.suptitle('Filtered Heat Fluxes', fontsize=fsize+6, y = .99)

colors = ['black', 'tab:blue']
labels = ['Control', 'Seasonal']
alphas = [1, 1]
linestyles = ['-', '--']
ylabels = ['$Q_E$ $W/m^2$', '$Q_H$ $W/m^2$']

# E, H
tmp = {'control':
       {'E': lhc.qlh[overall], 'H': lhc.qsh[overall]},
       'seasonal':
       {'E': lhs.qlh[overall], 'H': lhs.qsh[overall]}}

for i, key in enumerate(tmp['control'].keys()):
    for j, top_key in enumerate(tmp.keys()):
        ax[i].plot(t, tmp[top_key][key], color=colors[j], label=labels[j], linestyle=linestyles[j], alpha=alphas[j])

for i, axis in enumerate(ax):
    axis.tick_params(axis="x", labelsize=fsize-3)
    axis.tick_params(axis="y", labelsize=fsize-3)
    axis.set_ylabel(ylabels[i], fontsize = fsize)
    axis.set_xlim(t[0], t[-1])
    axis.xaxis.set_major_locator(mdates.MonthLocator(interval=1))

    for j, event in enumerate(mistral_data['events']):
        if j == 0:
            axis.axvspan(np.datetime64(event[0]), np.datetime64(event[-1] + timedelta(days=1)), color='tab:green', alpha=.25, label='Mistral')
        else:
            axis.axvspan(np.datetime64(event[0]), np.datetime64(event[-1] + timedelta(days=1)), color='tab:green', alpha=.25)

ax[0].legend(ncol=3, fontsize=fsize-4)
ax[-1].set_xlabel('Time', fontsize = fsize)

textx = 0.003
fig.text(textx, 0.94, '(a)', fontweight='bold', fontsize=fsize+2)
fig.text(textx, 0.475, '(b)', fontweight='bold', fontsize=fsize+2)

fig.tight_layout()
fig.savefig('../plots/fluxes_filtered_ex.png')
