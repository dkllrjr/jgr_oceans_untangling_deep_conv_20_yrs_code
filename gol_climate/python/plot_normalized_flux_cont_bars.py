import pickle
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime, timedelta

##############################################################################

with open('../data/flux_contributions.pickle', 'rb') as file:
    flux = pickle.load(file)

with open('../data/si_contributions.pickle', 'rb') as file:
    si = pickle.load(file)

##############################################################################

sums = []
for i, _ in enumerate(flux['rsw']):
    sums.append(0)

for i, fluxi in enumerate(flux):

    for j, fluxj in enumerate(flux[fluxi]):
        sums[j] += fluxj

sums = np.array(sums)
sums *= -1

beta = si['si_s']/sums

##############################################################################

dc_years = np.array([1999, 2000, 2005, 2009, 2011, 2012, 2013])
years = np.arange(1994, 2014)

ind_dc = []
dyears = []
for i, year in enumerate(years):
    if year in dc_years:
        ind_dc.append(i)

    dyears.append(datetime(year=year, month=1, day=1))

ind_dc = np.array(ind_dc)
dyears = np.array(dyears)

##############################################################################

bar_width = timedelta(days=100)

fig, ax = plt.subplots(1, 1, figsize=(14, 6), dpi=400)

ax.bar(dyears, beta*flux['rsw'], width=bar_width)

fig.tight_layout()
fig.savefig('../plots/flux_cont_bars.png')
