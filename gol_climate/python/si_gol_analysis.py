##############################################################################
# By Doug Keller
# 
# This script plots the figure showing the difference betweent the control or 
# total stratification (SI) NEMO results and the seasonal SI results.
# Mistral events and important time stamps are overlaid for connectivity.
##############################################################################

import pickle
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import ticker
import matplotlib.dates as mdates
from glob import glob
import numpy as np

from matplotlib.patches import Rectangle

##############################################################################
# load data

data_paths = []
data_paths.append(glob('../data/GOL5/N-CONT*SI.pickle'))
data_paths.append(glob('../data/GOL5/N-SEAS*SI.pickle'))

for i, data_path in enumerate(data_paths):
    data_paths[i].sort()

data = []

for data_path in data_paths:
    data.append([])
    for path in data_path:
        with open(path, 'rb') as file:
            data[-1].append(pickle.load(file))

with open('../data/mistral_attributes.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

##############################################################################

dc_years = np.array([1999, 2000, 2005, 2009, 2011, 2012, 2013])
years = np.arange(1994, 2014)

ind_dc = []
for i, year in enumerate(years):
    if year in dc_years:
        ind_dc.append(i)

si_init = []
si_max = []
si_s_max = []
si_min = []
si_s_min = []
dsi_min = []

dsi_dc = []
si_s_dc = []
dc_after_si_s_min = []
dc_together = []
dc_before_si_s_min = []

for i, (cont, seas) in enumerate(zip(data[0], data[1])):
    
    si_init.append(cont['si'][0])

    si_max.append(np.max(cont['si']))
    si_s_max.append(np.max(seas['si']))

    si_min.append(np.min(cont['si']))
    si_s_min.append(np.min(seas['si']))

    dsi_min.append(np.min(cont['si']-seas['si']))

    ind = cont['si'].argmin()

    si_s_ind = seas['si'].argmin()

    if ind > si_s_ind:
        dc_after_si_s_min.append(True)
        dc_together.append(False)
        dc_before_si_s_min.append(False)

    elif ind == si_s_ind:
        dc_after_si_s_min.append(False)
        dc_together.append(True)
        dc_before_si_s_min.append(False)
        
    else:
        dc_after_si_s_min.append(False)
        dc_together.append(False)
        dc_before_si_s_min.append(True)

    dsi_dc.append(cont['si'][ind]-seas['si'][ind])
    si_s_dc.append(seas['si'][ind])


# plt.plot(years, si_init, color='tab:blue', label='$SI_{init}$')

# plt.plot(years, si_max, color='tab:red', label='$SI_{max}$')
# plt.plot(years, si_min, color='tab:red', linestyle=':', label='$SI_{min}$')

# plt.plot(years, si_s_max, color='tab:green', label='$SI_{S,max}$')
# plt.plot(years, si_s_min, color='tab:green', linestyle=':', label='$SI_{S,min}$')

# plt.plot(years, dsi_min, color='k', label='$\\delta SI_{min}$')

plt.plot(years, dsi_dc, color='blue')
plt.plot(years, si_s_dc, color='red')

for dc_year in dc_years:
    plt.axvline(dc_year)

plt.plot(years[dc_after_si_s_min], np.ones(len(years))[dc_after_si_s_min], linestyle='', marker='o')
plt.plot(years[dc_together], 0-np.ones(len(years))[dc_together], linestyle='', marker='o', color='red')
plt.plot(years[dc_before_si_s_min], np.zeros(len(years))[dc_before_si_s_min], linestyle='', marker='o', color='k')

plt.legend()
plt.show()
