import xarray as xr
import numpy as np
from glob import glob
import matplotlib.pyplot as plt

# ──────────────────────────────────────────────────────────────────────────

host_data = '../../../host_data/phd/argo/'

# ──────────────────────────────────────────────────────────────────────────

dir_paths = glob(host_data + '*/')

files = []
for dir_path in dir_paths:
    files += glob(dir_path + '/*.nc')

files.sort()

argos = []
times = np.array([], dtype='datetime64[ns]')
for file in files:
    argos.append(xr.open_dataset(file))
    # times = np.append(times, argos[-1]['JULD'])
    times = np.append(times, argos[-1]['JULD'].values)

var = list(argos[0].variables)

plt.plot(times, np.ones(len(times)))
plt.show()


