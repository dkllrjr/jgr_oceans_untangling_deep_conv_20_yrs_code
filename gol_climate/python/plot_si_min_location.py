##############################################################################
#   Created: Thu Jun  4 12:11:38 2020
#   Author: mach
##############################################################################

import os
import xarray as xr
from glob import glob
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib.patches import Rectangle
import pickle

##############################################################################
# Plotting

def gol_si_plot(locs, bounds, plot_path):

    fig, ax = plt.subplots(1, 1, figsize=(15, 10), dpi=200, subplot_kw={'projection': ccrs.PlateCarree(5)})
    fsize = 22
    tsize = 32
        
    europe_land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m', edgecolor='black', facecolor='white') 
    # ax.add_feature(europe_land_10m)
    
    gl = ax.gridlines(crs=ccrs.PlateCarree(),draw_labels=True,linewidth=.75, color='black', alpha=0.35, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator([-90,2,5,8,90])
    gl.ylocator = mticker.FixedLocator([0,40,42,44,60])
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'size': fsize}
    gl.ylabel_style = {'size': fsize}
    
    ax.set_extent([0,10,38,45], crs=ccrs.PlateCarree());

    ax.add_feature(europe_land_10m)

    for loc in locs:
        ax.scatter(loc[1], loc[0], transform=ccrs.PlateCarree())

    # x, y = bounds['lon_min'], bounds['lat_min']

    # rect = Rectangle((x, y), bounds['lon_max']-x, bounds['lat_max']-y, fill=False, edgecolor='black', linestyle='--', transform=ccrs.PlateCarree())

    x, y = 4.25, 42

    rect = Rectangle((x, y), 5-x, 42.5-y, fill=False, edgecolor='black', linestyle='-', transform=ccrs.PlateCarree())

    ax.add_patch(rect)
    # ax.text(bounds['lon_max']*1.015, bounds['lat_max'], 'Search\nZone', fontsize=fsize, color='black', transform=ccrs.PlateCarree())
    
    fig.suptitle('Spatial Minimum of SI', y=.95, fontsize=tsize)
    # fig.suptitle('Spatial Minimum of SI', fontsize=tsize)
    # ax.set_title('Spatial Minimum of SI', fontsize=tsize)
    
    # fig.subplots_adjust(top = .85, wspace = .33, hspace = .3, left = left)
    fig.savefig(plot_path)
    plt.close()

##############################################################################

with open('../data/si_min_location.pickle', 'rb') as file:
    data = pickle.load(file)

plot_path = '../plots/gol_si_spatial_min.png'

gol_si_plot(data['locs'], data['bounds'],  plot_path)
