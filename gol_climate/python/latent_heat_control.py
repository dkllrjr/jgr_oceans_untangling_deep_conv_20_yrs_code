import pickle
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from glob import glob
from datetime import datetime
import core

##############################################################################

def q_lw(rlds, SST):
    o = 5.67 * 10**-8
    return rlds - o * SST**4
    
def qsat(sst):
    rhoa = 1.22
    q1 = .98 * 640380
    q2 = -5107.4
    return q1/rhoa * np.exp(q2/sst)

def hf2da(hf):
    hf = np.array(hf)
    da_tmp = np.resize(hf, (len(hf)//8, 8))
    da = np.mean(da_tmp, axis=1)
    return da

def hf2dadate(npdtarr):
    arr = []
    for i in npdtarr[::8]:
        tmp = datetime.fromisoformat(str(i)[0:16])
        arr.append(datetime(tmp.year, tmp.month, tmp.day, 12))
    arr = np.array(arr, dtype=np.datetime64)
    return arr

##############################################################################

data_paths = []

# seasonal sea surface paths
data_paths.append(glob('../data/GOL5/N-CONT*3H_2D.pickle'))  # hf
data_paths.append(glob('../data/GOL5/N-CONT*1D_2D.pickle'))  # da

# atmos paths
data_paths.append(glob('../data/GOL5/*RLDS.pickle'))  # rlds
data_paths.append(glob('../data/GOL5/M*HUSS.pickle'))  # huss
data_paths.append(glob('../data/GOL5/M*TAS.pickle'))  # tas
data_paths.append(glob('../data/GOL5/M*UAS.pickle'))  # uas
data_paths.append(glob('../data/GOL5/M*VAS.pickle'))  # vas

for i, data_path in enumerate(data_paths):
    data_paths[i].sort()

# load paths
data = []

for data_path in data_paths:
    data.append([])
    for path in data_path:
        with open(path, 'rb') as file:
            data[-1].append(pickle.load(file))

##############################################################################

# hf
mlh = []
thf = []

for item in data[0]:
    mlh.append(item['somixhgt'])
    thf.append(item['t'])

rldstmp = []
qatmp = []
Tatmp = []
utmp = []
vtmp = []
tatmp = []

for item in data[2]:
    rldstmp.append(hf2da(item['rlds']))

for item in data[3]:
    qatmp.append(hf2da(item['huss']))

for item in data[4]:
    Tatmp.append(hf2da(item['tas']))

for item in data[5]:
    utmp.append(hf2da(item['uas']))

for item in data[6]:
    vtmp.append(hf2da(item['vas']))
    tatmp.append(hf2dadate(item['t']))

# da
sst = []
qnet = []
qsw = []
tda = []

for item in data[1]:
    sst.append(item['sosstsst']+273.15)
    qnet.append(item['sohefldo'])
    qsw.append(item['soshfldo'])
    tda.append(item['t'])

##############################################################################
# separating

ind_byear = []
ind_eyear = []

for i, t in enumerate(tda):

    beg = np.datetime64(t[0])
    end = np.datetime64(t[-1])

    ind_byear.append(tatmp[i] >= beg)
    ind_eyear.append(tatmp[i+1] <= end)

rlds = []
qa = []
Ta = []
u = []
v = []
ta = []

for i, _ in enumerate(tda):
    rlds.append(np.append(rldstmp[i][ind_byear[i]], rldstmp[i+1][ind_eyear[i]]))
    qa.append(np.append(qatmp[i][ind_byear[i]], qatmp[i+1][ind_eyear[i]]))
    Ta.append(np.append(Tatmp[i][ind_byear[i]], Tatmp[i+1][ind_eyear[i]]))
    u.append(np.append(utmp[i][ind_byear[i]], utmp[i+1][ind_eyear[i]]))
    v.append(np.append(vtmp[i][ind_byear[i]], vtmp[i+1][ind_eyear[i]]))
    ta.append(np.append(tatmp[i][ind_byear[i]], tatmp[i+1][ind_eyear[i]]))

ws = []
wd = []
for i, ui in enumerate(u):
    ws.append((ui**2 + v[i]**2)**.5)
    wd.append(np.rad2deg(np.arctan2(v[i], u[i])))

##############################################################################

qlw = []

for i, rldsi in enumerate(rlds):
    qlw.append(q_lw(rldsi, sst[i]))

qsh = []
qlh = []

for i, wsi in enumerate(ws):
    qshtmp = []
    qlhtmp = []
    for j, _ in enumerate(wsi):
        Cd, Ch, Ce, T_zu, q_zu = core.turb_core_2z(2, 10, sst[i][j], Ta[i][j], qsat(sst[i][j]), qa[i][j], ws[i][j])
        qshtmpi, qlhtmpi = core.Qh(Ch, sst[i][j], T_zu, ws[i][j]), core.E(Ce, qsat(sst[i][j]), q_zu, ws[i][j])

        qshtmp.append(qshtmpi)
        qlhtmp.append(qlhtmpi)

    qsh.append(-np.array(qshtmp))
    qlh.append(-np.array(qlhtmp))
