##############################################################################
# By Doug Keller
# 
# This script plots the figure showing the difference betweent the control or 
# total stratification (SI) NEMO results and the seasonal SI results.
# Mistral events and important time stamps are overlaid for connectivity.
##############################################################################

import pickle
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import ticker
import matplotlib.dates as mdates
from glob import glob
import numpy as np
import latent_heat_control as lhc
import latent_heat_seasonal as lhs
from sklearn.metrics import mean_squared_error

##############################################################################
# load data

data_paths = []
data_paths.append(glob('../data/GOL5/N-CONT*SI.pickle'))
data_paths.append(glob('../data/GOL5/N-SEAS*SI.pickle'))

for i, data_path in enumerate(data_paths):
    data_paths[i].sort()

data = []

for data_path in data_paths:
    data.append([])
    for path in data_path:
        with open(path, 'rb') as file:
            data[-1].append(pickle.load(file))

dc_years = np.array([1999, 2000, 2005, 2009, 2011, 2012, 2013])
years = np.arange(1994, 2014)

##############################################################################
# plot

# Consider adding Mistral events

width, height = 14, 18

fsize = 18
tsize = 22

# SI plot

fig, ax = plt.subplots(10, 2, dpi=300, figsize=(width, height))
ax = ax.flatten('F')

M = 800

eSIs = []
for i, qnet in enumerate(lhs.qnet):

    # ax[i].plot(lhs.tda[i], qnet, color='black', label='$\dot{Q}_{net}$', zorder=10)
    # ax[i].plot(lhs.tda[i], lhs.qsh[i], color='tab:red', label='$\dot{Q}_H$')
    # ax[i].plot(lhs.tda[i], lhs.qlh[i], color='tab:green', label='$\dot{Q}_E$')
    # ax[i].plot(lhs.tda[i], lhs.qlw[i], color='tab:blue', label='$\dot{Q}_{LW}$')
    # ax[i].plot(lhs.tda[i], lhs.qsw[i], color='tab:orange', label='$\dot{Q}_{SW}$')

    ax[i].plot(lhs.tda[i], qnet, color='black', label='${Q}_{net}$', zorder=10)
    ax[i].plot(lhs.tda[i], lhs.qsh[i], color='tab:red', label='${Q}_H$')
    ax[i].plot(lhs.tda[i], lhs.qlh[i], color='tab:green', label='${Q}_E$')
    ax[i].plot(lhs.tda[i], lhs.qlw[i], color='tab:blue', label='${Q}_{LW}$')
    ax[i].plot(lhs.tda[i], lhs.qsw[i], color='tab:orange', label='${Q}_{SW}$')

    # eSI = np.gradient(data[1][i]['si'])/86400*1e9
    # eSIs.append(eSI)
    # ax[i].plot(data[1][i]['t'], eSI, color='black', linestyle='--', label='$\\frac{ \partial SI_S }{ \partial t } \\times 10^9$')
    
    ax[i].set_ylim(-M, 400)
    ax[i].set_xlim(lhs.tda[i][0], lhs.tda[i][-1])
    ax[i].tick_params(axis='both', labelsize=fsize-4)

    # xaxis stuff
    fmt_half_year = mdates.MonthLocator(interval=6)
    ax[i].xaxis.set_major_locator(fmt_half_year)
    
    fmt_month = mdates.MonthLocator()
    ax[i].xaxis.set_minor_locator(fmt_month)

    ax[i].xaxis.set_minor_formatter(mdates.DateFormatter('%m'))

    ax[i].grid(axis='x', which='minor', alpha=.5)
    ax[i].grid(axis='x', which='major', alpha=.5)

    # yaxis stuff
    ax[i].yaxis.set_minor_locator(ticker.AutoMinorLocator())
    
    ax[i].grid(axis='y', which='major', alpha=.5)

    if i == 9:
        ax[i].legend(ncol=6, bbox_to_anchor=(1, 1), bbox_transform=fig.transFigure, fontsize=fsize-4)

    if i < 10:
        ax[i].set_ylabel('$W/m^2$', fontsize=fsize)

    if i == 9 or i == 19:
        ax[i].set_xlabel('Time', fontsize=fsize)

    if years[i] in dc_years:
        ax[i].tick_params(axis='x', labelsize=fsize-4, labelcolor='tab:green')
    else:
        ax[i].tick_params(axis='x', labelsize=fsize-4)

fig.suptitle('Gulf of Lion Seasonal Fluxes', fontsize=tsize, y=.99, x=0.03, horizontalalignment='left')
fig.subplots_adjust(top=.96, left=0.08, right=.985, bottom=0.04, hspace=0.25, wspace=0.12)
# fig.tight_layout()
fig.savefig('../plots/fluxes_seasonal_gol.png')
fig.savefig('../plots/fluxes_seasonal_gol.pdf')

##############################################################################

# rmse = np.zeros(len(eSIs))
# bias = []

# for i, qnet in enumerate(lhs.qnet):

    # rmse[i] = np.sqrt(mean_squared_error(qnet, eSIs[i]))
    # bias.append(np.nanmean(qnet - eSIs[i]))

# print(np.nanmean(rmse), np.nanmean(bias))

#  ──────────────────────────────────────────────────────────────────────────

# SI plot

fig, ax = plt.subplots(10, 2, dpi=300, figsize=(width, height))
ax = ax.flatten('F')


for i, qnet in enumerate(lhc.qnet):

    # ax[i].plot(lhs.tda[i], qnet, color='black', label='$\dot{Q}_{net}$', zorder=10)
    # ax[i].plot(lhs.tda[i], lhc.qsh[i], color='tab:red', label='$\dot{Q}_H$')
    # ax[i].plot(lhs.tda[i], lhc.qlh[i], color='tab:green', label='$\dot{Q}_E$')
    # ax[i].plot(lhs.tda[i], lhc.qlw[i], color='tab:blue', label='$\dot{Q}_{LW}$')
    # ax[i].plot(lhs.tda[i], lhc.qsw[i], color='tab:orange', label='$\dot{Q}_{SW}$')

    ax[i].plot(lhs.tda[i], qnet, color='black', label='${Q}_{net}$', zorder=10)
    ax[i].plot(lhs.tda[i], lhc.qsh[i], color='tab:red', label='${Q}_H$')
    ax[i].plot(lhs.tda[i], lhc.qlh[i], color='tab:green', label='${Q}_E$')
    ax[i].plot(lhs.tda[i], lhc.qlw[i], color='tab:blue', label='${Q}_{LW}$')
    ax[i].plot(lhs.tda[i], lhc.qsw[i], color='tab:orange', label='${Q}_{SW}$')

    # eSI = np.gradient(data[1][i]['si'])/86400*1e9
    # eSIs.append(eSI)
    # ax[i].plot(data[1][i]['t'], eSI, color='black', linestyle='--', label='$\\frac{ \partial SI_S }{ \partial t } \\times 10^9$')
    
    ax[i].set_ylim(-M, 400)
    ax[i].set_xlim(lhs.tda[i][0], lhs.tda[i][-1])
    ax[i].tick_params(axis='both', labelsize=fsize-4)

    # xaxis stuff
    fmt_half_year = mdates.MonthLocator(interval=6)
    ax[i].xaxis.set_major_locator(fmt_half_year)
    
    fmt_month = mdates.MonthLocator()
    ax[i].xaxis.set_minor_locator(fmt_month)

    ax[i].xaxis.set_minor_formatter(mdates.DateFormatter('%m'))

    ax[i].grid(axis='x', which='minor', alpha=.5)
    ax[i].grid(axis='x', which='major', alpha=.5)

    # yaxis stuff
    ax[i].yaxis.set_minor_locator(ticker.AutoMinorLocator())
    
    ax[i].grid(axis='y', which='major', alpha=.5)

    if i == 9:
        ax[i].legend(ncol=6, bbox_to_anchor=(1, 1), bbox_transform=fig.transFigure, fontsize=fsize-4)

    if i < 10:
        ax[i].set_ylabel('$W/m^2$', fontsize=fsize)

    if i == 9 or i == 19:
        ax[i].set_xlabel('Time', fontsize=fsize)

    if years[i] in dc_years:
        ax[i].tick_params(axis='x', labelsize=fsize-4, labelcolor='tab:green')
    else:
        ax[i].tick_params(axis='x', labelsize=fsize-4)

fig.suptitle('Gulf of Lion Control Fluxes', fontsize=tsize, y=.99, x=0.03, horizontalalignment='left')
fig.subplots_adjust(top=.96, left=0.08, right=.985, bottom=0.04, hspace=0.25, wspace=0.12)
# fig.tight_layout()
fig.savefig('../plots/fluxes_control_gol.png')
fig.savefig('../plots/fluxes_control_gol.pdf')

#  ──────────────────────────────────────────────────────────────────────────

cmin = []
smin = []
for i, _ in enumerate(lhs.qlh):
    smin.append(np.nanmin(lhs.qlh[i]))
    cmin.append(np.nanmin(lhc.qlh[i]))

print(np.nanmin(cmin))
print(np.nanmin(smin))
