##############################################################################
# By Doug Keller
# 
# This script plots the figure showing the difference betweent the control or 
# total stratification (SI) NEMO results and the seasonal SI results.
# Mistral events and important time stamps are overlaid for connectivity.
##############################################################################

import pickle
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import ticker
import matplotlib.dates as mdates
from glob import glob
import numpy as np
from matplotlib.patches import Rectangle

##############################################################################
# load data

data_paths = []
data_paths.append(glob('../data/GOL5/N-CONT*SI.pickle'))
data_paths.append(glob('../data/GOL5/N-SEAS*SI.pickle'))

for i, data_path in enumerate(data_paths):
    data_paths[i].sort()

data = []

for data_path in data_paths:
    data.append([])
    for path in data_path:
        with open(path, 'rb') as file:
            data[-1].append(pickle.load(file))

##############################################################################
# plot

year = 0
cont, seas = data[0][year], data[1][year]

# Consider adding Mistral events

width, height = 6, 3

fsize = 10
tsize = 14
asize = 10

# SI plot

fig, ax = plt.subplots(1, 1, dpi=400, figsize=(width, height))

# plots

ax.plot(cont['t'], cont['si'], color='black', label='Control')
ax.plot(seas['t'], seas['si'], color='tab:blue', label='Seasonal')

sismax_t = np.argmax(seas['si'])
simin_t = np.argmin(cont['si'])
sismax = np.max(seas['si'])
sisminatt = seas['si'][simin_t]
simin = np.min(cont['si'])

ax.axhline(sismax, linestyle='-', linewidth=1.5, color='tab:purple', label='$SI_{S,max}$', ls='--')
ax.axhline(sisminatt, linestyle=(0, (5, 5)), linewidth=1.5, color='black', label='$SI_S$ at $SI_{min}$')
ax.axhline(simin, linestyle='-', linewidth=1.5, color='tab:olive', label='$SI_{min}$', ls='--')

ax.axhline(0, linestyle=':', color='black', alpha=.75)

ax.axvline(cont['t'][simin_t], linestyle='-.', color='red', label='$t$ at $SI_{min}$')

ax.axvline(cont['t'][sismax_t], linestyle=':', color='red', label='$t$ at $SI_{S,max}$')

# legend

ax.legend(bbox_to_anchor=(0., .05, 1., .05), ncol=4, loc='lower left', fontsize=fsize-4)

# arrows

x = cont['t'][-30]

# sis

ax.annotate('', xy=(x, sismax),  xycoords='data',
            xytext=(x, sisminatt), textcoords='data',
            size=asize,
            arrowprops=dict(arrowstyle="simple",
                fc='tab:blue',
                ec='tab:blue',
                shrinkA=5,
                connectionstyle="arc3"))

ax.annotate('', xytext=(x, sismax),  xycoords='data',
            xy=(x, sisminatt), textcoords='data',
            size=asize,
            arrowprops=dict(arrowstyle="simple",
                fc='tab:blue',
                ec='tab:blue',
                shrinkA=5,
                connectionstyle="arc3"))

# dsi

ax.annotate('', xy=(x, simin),  xycoords='data',
            xytext=(x, sisminatt), textcoords='data',
            size=asize,
            arrowprops=dict(arrowstyle="simple",
                fc='tab:green',
                ec='tab:green',
                shrinkA=5,
                connectionstyle="arc3"))

ax.annotate('', xytext=(x, simin),  xycoords='data',
            xy=(x, sisminatt), textcoords='data',
            size=asize,
            arrowprops=dict(arrowstyle="simple",
                fc='tab:green',
                ec='tab:green',
                shrinkA=5,
                connectionstyle="arc3"))

# remainder

ax.annotate('', xy=(x, simin),  xycoords='data',
            xytext=(x, 0), textcoords='data',
            size=asize,
            arrowprops=dict(arrowstyle="simple",
                fc='tab:red',
                ec='tab:red',
                shrinkA=5,
                connectionstyle="arc3"))

ax.annotate('', xytext=(x, simin),  xycoords='data',
            xy=(x, 0), textcoords='data',
            size=asize,
            arrowprops=dict(arrowstyle="simple",
                fc='tab:red',
                ec='tab:red',
                shrinkA=5,
                connectionstyle="arc3"))

ax.annotate('$\Omega SI_{S}$', xy=(x, 1.4), xycoords='data',
            xytext=(1.01, .9), textcoords='axes fraction',
            fontsize=fsize,
            arrowprops=dict(arrowstyle='-|>',
                            fc='k',
                            ec='k',
                            shrinkB=5,
                            connectionstyle='arc3'))

ax.annotate('$\Omega \delta SI$', xy=(x, 0.6), xycoords='data',
            xytext=(1.015, .4), textcoords='axes fraction',
            fontsize=fsize,
            arrowprops=dict(arrowstyle='-|>',
                            fc='k',
                            ec='k',
                            shrinkB=5,
                            connectionstyle='arc3'))

ax.annotate('Leftover $SI$', xy=(x, 0.19), xycoords='data',
            xytext=(1.015, .15), textcoords='axes fraction',
            fontsize=fsize,
            arrowprops=dict(arrowstyle='-|>',
                            fc='k',
                            ec='k',
                            shrinkB=5,
                            connectionstyle='arc3'))

# xaxis stuff

fmt_half_year = mdates.MonthLocator(interval=6)
ax.xaxis.set_major_locator(fmt_half_year)
fmt_month = mdates.MonthLocator()
ax.xaxis.set_minor_locator(fmt_month)
ax.xaxis.set_minor_formatter(mdates.DateFormatter('%m'))
ax.grid(axis='x', which='minor', alpha=.5)
ax.grid(axis='x', which='major', alpha=.5)
ax.set_xlim(cont['t'][0], cont['t'][-1])

# yaxis stuff

ax.yaxis.set_minor_locator(ticker.AutoMinorLocator())
ax.grid(axis='y', which='major', alpha=.5)

# both axes

ax.tick_params(axis='y', labelsize=fsize-2)
ax.tick_params(axis='x', which='minor', labelsize=fsize-4)
ax.tick_params(axis='x', which='major', labelsize=fsize-2)

fig.suptitle('Gulf of Lion $SI$: Contributions to Destratification', y=.96, fontsize=tsize)
fig.tight_layout()
fig.subplots_adjust(top=.87, bottom=0.09)
fig.savefig('../plots/si_gol_example.pdf')
