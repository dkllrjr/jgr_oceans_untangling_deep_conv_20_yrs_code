##############################################################################

##############################################################################

import pickle
import numpy as np
import datetime

##############################################################################
# load data

with open('../data/42_5/si_gol_past.pickle', 'rb') as file:
    si_data = pickle.load(file)

##############################################################################
# calculations

# deep_conv_t = si_data['t'][np.where(si_data['control']['SI']<=0)][0]
# seas_min_t = si_data['t'][np.argmin(si_data['seasonal']['SI'])]

# e = deep_conv_t - seas_min_t

SI_S_start = []

SI_S_min = []
SI_S_argmin = []
SI_S_mint = []

SI_S_max = []
SI_S_argmax = []
SI_S_maxt = []


SI_cont_min = []
SI_cont_mint = []

for i, t in enumerate(si_data['t']):

    seas_SI = si_data['seasonal']['SI'][i]
    cont_SI = si_data['control']['SI'][i]

    SI_S_start.append(seas_SI[0])

    SI_S_min.append(np.nanmin(seas_SI))
    SI_S_argmin.append(np.argmin(seas_SI))
    SI_S_mint.append(t[np.argmin(seas_SI)])

    SI_S_max.append(np.nanmax(seas_SI[0:200]))
    SI_S_argmax.append(np.argmax(seas_SI[0:200]))
    SI_S_maxt.append(t[np.argmax(seas_SI[0:200])])

    SI_cont_min.append(np.nanmin(cont_SI))
    SI_cont_mint.append(t[np.argmin(cont_SI)])

##############################################################################

# fig, ax = plt.subplots(4, 1)

# ax[0].plot(SI_S
