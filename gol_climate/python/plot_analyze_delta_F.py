import pickle
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import ticker
import matplotlib.dates as mdates
from glob import glob
import numpy as np
import plot_si_bars as psb
from matplotlib import cm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import FormatStrFormatter
import pandas

##############################################################################

def DSI(dSIk_0, dFk, D, ad, dtk):
    return (dSIk_0 + D**2/2 * dFk/ad) * (np.exp(-ad*dtk) - 1)

##############################################################################

dc_years = np.array([1999, 2000, 2005, 2009, 2011, 2012, 2013])
ab_years = np.array([2009, 2012, 2013]) # abnormal deep convection years in that, control SI min is after seasonal SI min
years = np.arange(1994, 2014)

ind_dc = []
ind_not_dc = []
dyears = []
ind_ab = []
for i, year in enumerate(years):
    if year in dc_years:
        ind_dc.append(i)
        if year in ab_years:
            ind_ab.append(i)
    else:
        ind_not_dc.append(i)

    dyears.append(datetime(year=year, month=1, day=1))

ind_dc = np.array(ind_dc)
ind_not_dc = np.array(ind_not_dc)
dyears = np.array(dyears)
ind_ab = np.array(ind_ab)

##############################################################################

t = np.array(psb.time) # time
t0 = np.array(psb.si_s_max_ind) # seasonal SI maximum
t1_c = np.array(psb.inddc) # control SI minimum
t1 = np.array(psb.si_s_min_ind) # seasonal SI minimum

##############################################################################

with open('../data/depth.pickle', 'rb') as file:
    D = pickle.load(file)

with open('../data/dsi_curve_fit.pickle', 'rb') as file:
    alpha_data = pickle.load(file)

alpha = alpha_data['alpha']

with open('../data/delta_F_20yrs.pickle', 'rb') as file:
    dF = pickle.load(file)

with open('../data/mistral_attributes.pickle', 'rb') as file:
    mistrals = pickle.load(file)

with open('../data/mlh_growth_dates.pickle', 'rb') as file:
    mld = np.array(pickle.load(file))

data_paths = glob('../data/GOL5/N-SEAS*SI.pickle')
data_paths.sort()

data = []
for path in data_paths:
    with open(path, 'rb') as file:
        data.append(pickle.load(file))

##############################################################################

# mld 0: > 250m deep, 1: first major peak depth

# mistral_ind, strength, duration

bef = []
aft = []
bet = []

ab_ind = set()

for i, si_s_min_ind in enumerate(t1):
    if i in ind_dc:
        bef.append({'mistral_ind': [], 'dF': [], 'dt': [], 'DSI': [], 'DSIS': []})
        bet.append({'mistral_ind': [], 'dF': [], 'dt': [], 'DSI': [], 'DSIS': []})
        aft.append({'mistral_ind': [], 'dF': [], 'dt': [], 'DSI': [], 'DSIS': []})

        if i < 19:
            temp_dFs = {}
            for key in dF[i].keys():
                temp_dFs[key] = dF[i][key] + dF[i+1][key]

            temp_si = {}
            for key in data[i].keys():
                temp_si[key] = list(data[i][key]) + list(data[i+1][key])

        else:
            temp_dFs = dF[i]
            temp_si = data[i]

        if i in ind_ab:
            ab_ind.add(len(bef))

        for j, dF_ind in enumerate(temp_dFs['mistral_ind']):
            
            mistral_date = np.datetime64(mistrals['events'][dF_ind][0])
            time_dif = np.timedelta64(120, 'D')

            if t[i][si_s_min_ind] - time_dif < mistral_date <= t[i][si_s_min_ind]:
                bef[-1]['mistral_ind'].append(dF_ind)
                bef[-1]['dF'].append(temp_dFs['dF'][j])
                bef[-1]['dt'].append(mistrals['duration'][dF_ind])

                dsi = DSI(temp_dFs['dSI_k-1'][j], temp_dFs['dF'][j], D, alpha, mistrals['duration'][dF_ind])
                bef[-1]['DSI'].append(dsi)

                m = list(temp_si['t']).index(mistral_date + np.timedelta64(12, 'h'))
                n = list(temp_si['t']).index(np.datetime64(mistrals['events'][dF_ind][-1]) + np.timedelta64(12, 'h'))
                bef[-1]['DSIS'].append(temp_si['si'][n] - temp_si['si'][m])

            elif t[i][si_s_min_ind] < mistral_date <= t[i][t1_c[i]]:
                bet[-1]['mistral_ind'].append(dF_ind)
                bet[-1]['dF'].append(temp_dFs['dF'][j])
                bet[-1]['dt'].append(mistrals['duration'][dF_ind])

                dsi = DSI(temp_dFs['dSI_k-1'][j], temp_dFs['dF'][j], D, alpha, mistrals['duration'][dF_ind])
                bet[-1]['DSI'].append(dsi)

                m = list(temp_si['t']).index(mistral_date + np.timedelta64(12, 'h'))
                n = list(temp_si['t']).index(np.datetime64(mistrals['events'][dF_ind][-1]) + np.timedelta64(12, 'h'))
                bet[-1]['DSIS'].append(temp_si['si'][n] - temp_si['si'][m])

            elif t[i][t1_c[i]] < mistral_date < t[i][si_s_min_ind] + time_dif:
                aft[-1]['mistral_ind'].append(dF_ind)
                aft[-1]['dF'].append(temp_dFs['dF'][j])
                aft[-1]['dt'].append(mistrals['duration'][dF_ind])

                dsi = DSI(temp_dFs['dSI_k-1'][j], temp_dFs['dF'][j], D, alpha, mistrals['duration'][dF_ind])
                aft[-1]['DSI'].append(dsi)

                m = list(temp_si['t']).index(mistral_date + np.timedelta64(12, 'h'))
                n = list(temp_si['t']).index(np.datetime64(mistrals['events'][dF_ind][-1]) + np.timedelta64(12, 'h'))
                aft[-1]['DSIS'].append(temp_si['si'][n] - temp_si['si'][m])

ab_ind = list(ab_ind)

##############################################################################
# plot integrated heat transfer

# bef_dF = []
# aft_dF = []
# for i, b in enumerate(bef):
    # bef_dF += list(np.array(b['dF']) * np.array(b['dt']))
    # aft_dF += list(np.array(aft[i]['dF']) * np.array(aft[i]['dt']))

# total = bef_dF + aft_dF
# _, bin_edges = np.histogram(total, bins=25)

# plt.hist(total, bins=bin_edges, color='k', histtype='step')
# plt.hist(bef_dF, bins=bin_edges, color='tab:blue', histtype='step')
# plt.hist(aft_dF, bins=bin_edges, color='tab:red', histtype='step')
# plt.show()

##############################################################################
# plot DSI

bef_DSI = []
bet_DSI = []
aft_DSI = []

total_bef_ab_DSIS = []
total_bef_non_ab_DSIS = []

total_bet_ab_DSIS = []
total_bet_non_ab_DSIS = []

total_aft_ab_DSIS = []
total_aft_non_ab_DSIS = []

for i, b in enumerate(bef):
    bef_DSI += b['DSI']
    bet_DSI += bet[i]['DSI']
    aft_DSI += aft[i]['DSI']

    if i in ab_ind:
        total_bef_ab_DSIS += b['DSI'] + b['DSIS']
        total_bet_ab_DSIS += bet[i]['DSI'] + bet[i]['DSIS']
        total_aft_ab_DSIS += aft[i]['DSI'] + aft[i]['DSIS']
    else:
        total_bef_non_ab_DSIS += b['DSI'] + b['DSIS']
        total_bet_non_ab_DSIS += bet[i]['DSI'] + bet[i]['DSIS']
        total_aft_non_ab_DSIS += aft[i]['DSI'] + aft[i]['DSIS']

total = bef_DSI + bet_DSI + aft_DSI
_, bin_edges = np.histogram(total, bins=50)

total_total = total_bef_ab_DSIS + total_bef_non_ab_DSIS + total_aft_ab_DSIS + total_aft_non_ab_DSIS + total_bet_ab_DSIS + total_bet_non_ab_DSIS
_, total_bin_edges = np.histogram(total_total, bins=50)

tsize = 24
fsize = 20
lw = 2
ls = '-'
a = 1
lw_bar = 2
a_bar = 0.5

fig, ax = plt.subplots(1, 2, figsize=(16, 6), dpi=400)

# focus on before

ax[0].hist(total, bins=bin_edges, color='k', histtype='step', alpha=a_bar, linestyle='--', lw=1, label='All', zorder=1, facecolor='w', fill=True)
ax[0].hist(bef_DSI + bet_DSI, bins=bin_edges, color='tab:blue', histtype='step', ls='-', lw=lw_bar, label='Before', zorder=3, edgecolor=None, facecolor='tab:blue', fill=True)

ax[0].axvline(np.mean(total), alpha=a, color='k', linewidth=lw, ls=ls, label='${\mathrm{All}}$', zorder=1)
ax[0].axvline(np.mean(bef_DSI + bet_DSI), alpha=a, color='tab:blue', linewidth=lw, ls=ls, label='${\mathrm{Before}}$', zorder=2)

# focus on after

ax[1].hist(total, bins=bin_edges, color='k', histtype='step', alpha=a_bar, linestyle='--', lw=1, label='All', zorder=1, facecolor='w', fill=True)
ax[1].hist(aft_DSI, bins=bin_edges, color='tab:red', histtype='step', lw=lw_bar, label='After', zorder=3, edgecolor=None, facecolor='tab:red', fill=True)

ax[1].axvline(np.mean(total), alpha=a, color='k', linewidth=lw, ls=ls, label='${\mathrm{All}}$', zorder=1)
ax[1].axvline(np.mean(aft_DSI), alpha=a, color='tab:red', linewidth=lw, ls=ls, label='${\mathrm{After}}$', zorder=2)

ax[0].set_ylabel('Number of Mistral Events', fontsize=fsize)

ax[0].set_title('Before $SI_{S,min}$', fontsize=fsize)
ax[1].set_title('After $SI_{S,min}$', fontsize=fsize)

for axi in ax:

    axi.legend(fontsize=fsize-4, loc='upper left')
    axi.tick_params(axis='both', labelsize=fsize-4)
    axi.set_xlabel('$\\Delta \\delta SI$ $m^2/s^2$', fontsize=fsize)

print(np.round(np.mean(total),3))
print(np.round(np.mean(bef_DSI + bet_DSI),3))
print(np.round(np.mean(aft_DSI),3))

# fig, ax = plt.subplots(1, 3, figsize=(20, 6), dpi=400)

# dsi

# ax[0].hist(total, bins=bin_edges, color='k', histtype='step', linestyle='--', label='All')
# ax[0].hist(bef_DSI + bet_DSI, bins=bin_edges, color='tab:blue', histtype='step', label='Before $SI_{S,min}$')
# ax[0].hist(aft_DSI, bins=bin_edges, color='tab:red', histtype='step', label='After $SI_{S,min}$')

# ax[0].axvline(np.mean(total), color='k', linewidth=lw, label='${\mathrm{All}}$')
# ax[0].axvline(np.mean(bef_DSI), color='tab:blue', linewidth=lw, label='${\mathrm{Before } SI_{S,min}}$')
# ax[0].axvline(np.mean(aft_DSI), color='tab:red', linewidth=lw, label='${\mathrm{After } SI_{S,min}}$')

# ax[0].set_ylabel('Number of Mistral Events', fontsize=fsize)

# si

# ax[1].hist(total_total, bins=bin_edges, color='k', histtype='step', linestyle='--', label='All')
# ax[1].hist(total_bef_ab_DSIS, bins=bin_edges, color='tab:blue', histtype='step', label='Before $SI_{S,min}$')
# ax[1].hist(total_bet_ab_DSIS, bins=bin_edges, color='tab:purple', histtype='step', label='Between $SI_{S,min}$ and $SI_{min}$')
# ax[1].hist(total_aft_ab_DSIS, bins=bin_edges, color='tab:red', histtype='step', label='After $SI_{min}$')

# ax[1].axvline(np.mean(total), color='k', linewidth=lw, label='${\mathrm{All}}$')
# ax[1].axvline(np.mean(total_bef_ab_DSIS), color='tab:blue', linewidth=lw, label='${\mathrm{Before } SI_{S,min}}$')
# ax[1].axvline(np.mean(total_bet_ab_DSIS), color='tab:purple', linewidth=lw, label='${\mathrm{Between } SI_{S,min} \mathrm{ and } SI_{min}}$')
# ax[1].axvline(np.mean(total_aft_ab_DSIS), color='tab:red', linewidth=lw, label='${\mathrm{After } SI_{S,min}}$')

# ax[2].hist(total_total, bins=bin_edges, color='k', histtype='step', linestyle='--', label='All')
# ax[2].hist(total_bef_non_ab_DSIS, bins=bin_edges, color='tab:blue', histtype='step', label='Before $SI_{S,min}$')
# ax[2].hist(total_bet_non_ab_DSIS, bins=bin_edges, color='tab:purple', histtype='step', label='Between $SI_{S,min}$ and $SI_{min}$')
# ax[2].hist(total_aft_non_ab_DSIS, bins=bin_edges, color='tab:red', histtype='step', label='After $SI_{min}$')

# ax[2].axvline(np.mean(total), color='k', linewidth=lw, label='${\mathrm{All}}$')
# ax[2].axvline(np.mean(total_bef_non_ab_DSIS), color='tab:blue', linewidth=lw, label='${\mathrm{Before } SI_{S,min}}$')
# ax[2].axvline(np.mean(total_bet_non_ab_DSIS), color='tab:purple', linewidth=lw, label='${\mathrm{Between } SI_{S,min} \mathrm{ and } SI_{min}}$')
# ax[2].axvline(np.mean(total_aft_non_ab_DSIS), color='tab:red', linewidth=lw, label='${\mathrm{After } SI_{S,min}}$')

# # fig.suptitle('Mistral Induced Destratification', fontsize=tsize)

# for axi in ax:
    # axi.legend(fontsize=fsize-4, loc='upper left')
    # axi.tick_params(axis='both', labelsize=fsize-4)

    # axi.set_xlabel('$\\Delta \\delta SI$', fontsize=fsize)

    # # axi.set_xlabel('$\\Delta \\delta SI + \\Delta SI_S$', fontsize=fsize)

# ax.legend(fontsize=fsize-4, loc='upper left')
# ax.tick_params(axis='both', labelsize=fsize-4)

# ax.set_xlabel('$\\Delta \\delta SI$ $m^2/s^2$', fontsize=fsize)

# texty = 0.95

# fig.text(0.03, texty, '(a)', fontweight='bold', fontsize=fsize-2)
# fig.text(0.35, texty, '(b)', fontweight='bold', fontsize=fsize-2)
# fig.text(0.67, texty, '(c)', fontweight='bold', fontsize=fsize-2)

fig.tight_layout()

fig.savefig('../plots/model_dsi_bef_aft.pdf')
