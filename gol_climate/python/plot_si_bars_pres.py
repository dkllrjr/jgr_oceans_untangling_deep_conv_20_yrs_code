import pickle
from glob import glob

data_paths = []
data_paths.append(glob('../data/GOL5/N-CONT*SI.pickle'))
data_paths.append(glob('../data/GOL5/N-SEAS*SI.pickle'))

for i, data_path in enumerate(data_paths):
    data_paths[i].sort()

data = []

for data_path in data_paths:
    data.append([])
    for path in data_path:
        with open(path, 'rb') as file:
            data[-1].append(pickle.load(file))

with open('../data/mistral_attributes.pickle', 'rb') as file:
    mistral_data = pickle.load(file)

with open('../data/flux_contributions.pickle', 'rb') as file:
    flux = pickle.load(file)

import numpy as np
from datetime import datetime, timedelta

dc_years = np.array([1999, 2000, 2005, 2009, 2011, 2012, 2013])
years = np.arange(1994, 2014)

ind_dc = []
dyears = []
for i, year in enumerate(years):
    if year in dc_years:
        ind_dc.append(i)

    dyears.append(datetime(year=year, month=1, day=1))

ind_dc = np.array(ind_dc)
dyears = np.array(dyears)

si_init = []  # initial SI
si_max = []  # max control SI
si_s_max = []  # max seasonal SI
si_min = []  # min control SI
si_s_min = []  # min seasonal SI

dsi_dc = []  # delta SI at time of deep convection
si_s_dc = []  # seasonal SI at time of deep convection

dc_aft = []  # deep convection after seasonal SI minimum
dc_tog = []  # deep convection with seasonal SI minimum occurring about the same time
dc_bef = []  # deep convection before seasonal SI minimum

inddc = []
si_s_max_ind = []
si_s_mindc = []

datedc = []

for i, (cont, seas) in enumerate(zip(data[0], data[1])):
    
    si_init.append(cont['si'][0])

    si_max.append(np.max(cont['si']))
    si_s_max.append(np.max(seas['si']))

    si_min.append(np.min(cont['si']))
    si_s_min.append(np.min(seas['si']))

    dc_ind = cont['si'].argmin()
    si_s_ind = seas['si'].argmin()
    si_s_mindc.append(seas['si'][dc_ind])

    inddc.append(cont['si'].argmin())
    datedc.append(cont['t'][inddc[-1]])

    si_s_max_ind.append(cont['si'].argmax())

    # determining before, after, or simultaneous destratification for delta SI and seasonal SI
    if dc_ind > si_s_ind:
        dc_aft.append(True)
        dc_tog.append(False)
        dc_bef.append(False)

    elif dc_ind == si_s_ind:
        dc_aft.append(False)
        dc_tog.append(True)
        dc_bef.append(False)
        
    else:
        dc_aft.append(False)
        dc_tog.append(False)
        dc_bef.append(True)

    dsi_dc.append(cont['si'][dc_ind]-seas['si'][dc_ind])
    si_s_dc.append(seas['si'][dc_ind])

si_init = np.array(si_init)

si_s_max = np.array(si_s_max)

si_max = np.array(si_max)
si_min = np.array(si_min)

dsi_dc = np.array(dsi_dc)
si_s_dc = np.array(si_s_dc)
si_s_max_ind = np.array(si_s_max_ind)
si_s_mindc = np.array(si_s_mindc)

import matplotlib.pyplot as plt
import matplotlib.dates as mdates

beg = datetime(year=1993, month=6, day=1)
end = datetime(year=2013, month=6, day=1)

# all bar plots

fsize = 16
tsize = 22

# fig, ax = plt.subplots(5, 1, figsize=(12, 14), dpi=200)
fig, ax = plt.subplots(2, 2, figsize=(24,8), dpi=250)
ax = ax.flatten('C')

# seasonal si max and seasonal si min

ax[0].axhline(si_s_max.mean(), color='tab:purple', label='$\overline{SI_{S,max}}$')

bar_width = timedelta(days=100)
ax[0].bar(dyears - timedelta(days=60), si_min, width=bar_width, color='tab:olive', label='$SI_{min}$')
ax[0].bar(dyears[ind_dc]- timedelta(days=60), si_min[ind_dc], width=bar_width, color='none', hatch='//', label='Deep conv.')

ax[0].bar(dyears + timedelta(days=60), si_s_max, width=bar_width, color='tab:purple', label='$SI_{S,max}$')
ax[0].bar(dyears[ind_dc]+ timedelta(days=60), si_s_max[ind_dc], width=bar_width, color='none', hatch='//')

fmt_year = mdates.YearLocator()
ax[0].xaxis.set_major_locator(fmt_year)
ax[0].xaxis.set_major_formatter(mdates.DateFormatter('%Y'))

ax[0].set_xlim(beg, end)

ax[0].set_ylabel('$m^2/s^2$', fontsize=fsize)
ax[0].legend(ncol=2)

ax[0].set_title('Stratification minimum/maximum', fontsize=tsize)

# seasonal and delta si contribution

bar_width = timedelta(days=200)
ax[1].bar(dyears, si_s_max-si_s_dc, width=bar_width, color='tab:blue', label='$SI_{S,Cont}$')
ax[1].bar(dyears, -dsi_dc, width=bar_width, color='tab:green', bottom=si_s_max-si_s_dc, label='$\\delta SI_{Cont}$')
ax[1].bar(dyears, si_s_max - (si_s_max-si_s_dc) + dsi_dc, width=bar_width, color='tab:red', bottom=(si_s_max-si_s_dc) - dsi_dc, label='Leftover $SI$')

ax[1].bar(dyears[ind_dc], (si_s_max-si_s_dc)[ind_dc], width=bar_width, color='none', hatch='//', label='Deep conv.')
ax[1].bar(dyears[ind_dc], -1*dsi_dc[ind_dc], width=bar_width, color='none', bottom=(si_s_max-si_s_dc)[ind_dc], hatch='//')
ax[1].axhline((si_s_max-si_s_dc).mean(), color='tab:blue', label='$\overline{SI_{S,Cont}}$')

fmt_year = mdates.YearLocator()
ax[1].xaxis.set_major_locator(fmt_year)
ax[1].xaxis.set_major_formatter(mdates.DateFormatter('%Y'))

ax[1].set_xlim(beg, end)

ax[1].set_ylabel('$m^2/s^2$', fontsize=fsize)
ax[1].legend(ncol=2)

ax[1].set_title('Contribution to deep convection', fontsize=tsize)

# Normalized seasonal and delta si contribution

ax[2].axhline(((si_s_max-si_s_dc)/si_s_max).mean(), color='tab:blue', label='$\overline{SI_{S,Cont}}$')

bar_width = timedelta(days=200)
ax[2].bar(dyears, 1, width=bar_width, color='tab:red', label='Leftover $SI$')
ax[2].bar(dyears, (si_s_max-si_s_dc)/si_s_max, width=bar_width, color='tab:blue', label='$SI_{S,Cont}$')
ax[2].bar(dyears, -dsi_dc/si_s_max, width=bar_width, color='tab:green', bottom=(si_s_max-si_s_dc)/si_s_max, label='$\\delta SI_{Cont}$')

ax[2].bar(dyears[ind_dc], ((si_s_max-si_s_dc)/si_s_max)[ind_dc], width=bar_width, color='none', hatch='//', label='Deep conv.')
ax[2].bar(dyears[ind_dc], (-1*dsi_dc/si_s_max)[ind_dc], width=bar_width, color='none', bottom=((si_s_max-si_s_dc)/si_s_max)[ind_dc], hatch='//')

fmt_year = mdates.YearLocator()
ax[2].xaxis.set_major_locator(fmt_year)
ax[2].xaxis.set_major_formatter(mdates.DateFormatter('%Y'))

ax[2].set_xlim(beg, end)

# ax[2].set_ylabel('%', fontsize=fsize)
ax[2].legend(ncol=2)

ax[2].set_title('Normalized contribution to deep convection ($\cdot/SI_{S,max}$)', fontsize=tsize)

# delta SI bar plot

ax[3].axhline(((-dsi_dc)/si_s_max).mean(), color='tab:green', label='$\overline{\delta SI_{Cont}}$')

bar_width = timedelta(days=200)
ax[3].bar(dyears, -dsi_dc/si_s_max, width=bar_width, color='tab:green', label='$\\delta SI_{Cont}$')

ax[3].bar(dyears[ind_dc], (-1*dsi_dc/si_s_max)[ind_dc], width=bar_width, color='none', hatch='//', label='Deep conv.')

fmt_year = mdates.YearLocator()
ax[3].xaxis.set_major_locator(fmt_year)
ax[3].xaxis.set_major_formatter(mdates.DateFormatter('%Y'))

ax[3].set_xlim(beg, end)

# ax[3].set_ylabel('%', fontsize=fsize)
ax[3].legend(ncol=2)

ax[3].set_title('Normalized anomaly contribution to deep convection ($\cdot/SI_{S,max}$)', fontsize=tsize)

# separating bars by contribution

# bar_width = timedelta(days=100)
# ax[4].bar(dyears - timedelta(days=60), flux['rsw'] + si_init, width=bar_width)
# ax[4].bar(dyears + timedelta(days=60), flux['rlw'], width=bar_width, bottom=flux['rsw'] + si_init)
# ax[4].bar(dyears + timedelta(days=60), flux['rsh'], width=bar_width, bottom=flux['rsw'] + flux['rlw'] + si_init)
# ax[4].bar(dyears + timedelta(days=60), flux['rlh'], width=bar_width, bottom=flux['rsw'] + flux['rlw'] + flux['rsh'] + si_init)
# # ax[4].bar(dyears, flux['rlh'], width=bar_width, bottom=flux['rsw'] + flux['rlw'] + flux['rsh'])

# fmt_year = mdates.YearLocator()
# ax[4].xaxis.set_major_locator(fmt_year)
# ax[4].xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
# ax[4].set_xlim(beg, end)

for axis in ax:
    axis.tick_params(axis='x', labelsize=fsize-3)
    axis.tick_params(axis='y', labelsize=fsize-2)

textx = 0.01
textx1 = 0.5
fig.text(textx, 0.95, '(a)', fontweight='bold', fontsize=fsize+2)
fig.text(textx1, 0.95, '(b)', fontweight='bold', fontsize=fsize+2)
fig.text(textx, 0.47, '(c)', fontweight='bold', fontsize=fsize+2)
fig.text(textx1, 0.47, '(d)', fontweight='bold', fontsize=fsize+2)

fig.tight_layout()
# fig.subplots_adjust(left=0.07)

fig.savefig('../plots/si_bars_pres.png')
