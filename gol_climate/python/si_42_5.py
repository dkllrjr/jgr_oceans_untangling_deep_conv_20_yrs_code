##############################################################################



##############################################################################

import xarray as xr
import numpy as np
from scipy.integrate import simps
from datetime import datetime
import pickle
from glob import glob

##############################################################################
# functions

def datetime64todatetime(d64):
    d = np.empty_like(d64).astype(datetime)
    for i, item in enumerate(d64):
        d[i] = datetime.fromisoformat(str(item)[0:19])

    return d


##############################################################################
# loading data

N_cont_path = glob('../data/42_5/CONT*N.nc')
N_seas_path = glob('../data/42_5/SEAS*N.nc')

N_cont_path.sort()
N_seas_path.sort()

##############################################################################
# calculating

SI_cont_list = []
SI_seas_list = []
N_cont_list = []
N_seas_list = []
t_list = []
dSI_list = []

for i, cont_path in enumerate(N_cont_path):

    N_cont = xr.open_dataset(cont_path)
    N_seas = xr.open_dataset(N_seas_path[i])

    d = N_cont.depthw.values

    D = np.max(d[np.where(N_cont.vovaisala.values[0]>0)])
    t = datetime64todatetime(N_cont.time_counter.values)

    SI_cont = simps(N_cont.vovaisala*d, d)
    SI_seas = simps(N_seas.vovaisala*d, d)

    dSI = SI_cont - SI_seas

    N_cont_ave = 2*SI_cont/D**2
    N_seas_ave = 2*SI_seas/D**2

    SI_cont_list.append(SI_cont)
    SI_seas_list.append(SI_seas)
    N_cont_list.append(N_cont_ave)
    N_seas_list.append(N_seas_ave)
    t_list.append(t)
    dSI_list.append(dSI)

##############################################################################
# saving

cont = {'SI': SI_cont_list, 'N_ave': N_cont_list}
seas = {'SI': SI_seas_list, 'N_ave': N_seas_list}
data = {'D': D, 'd': d, 't': t_list, 'dSI': dSI_list, 'control': cont, 'seasonal': seas}

with open('../data/42_5/si_gol_past.pickle', 'wb') as file:
    pickle.dump(data, file)
