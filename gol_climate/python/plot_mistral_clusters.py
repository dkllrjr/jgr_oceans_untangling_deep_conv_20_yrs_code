import pickle
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import ticker
import matplotlib.dates as mdates
from glob import glob
import numpy as np
from matplotlib.patches import Rectangle
import pandas
import view_mistral_types as vmt
import plot_mistral_weak_strong as pmws

##############################################################################
# load data

# with open('../data/mistral_attributes_1993-2013.pickle', 'rb') as file:
    # mistral_data = pickle.load(file)

mistral_data = pandas.read_csv('../data/mistral_intensity_clusters_1993-2013_precond.csv', index_col=0)

##############################################################################
# plot

dc_years = np.array([1999, 2000, 2005, 2009, 2011, 2012, 2013])
years = np.arange(1994, 2014)

ind_dc = []
dyears = []
for i, year in enumerate(years):
    if year in dc_years:
        ind_dc.append(i)

    dyears.append(datetime(year=year, month=1, day=1))

ind_dc = np.array(ind_dc)
dyears = np.array(dyears)

##############################################################################

ind = []

years -= 1

for year in years:
    
    ind.append([])

    beg = datetime(year, 7, 1, 12)
    end = datetime(year+1, 6, 30, 12)

    for j, date in enumerate(mistral_data.Date):

        if beg <= datetime.fromisoformat(date) <= end:
            ind[-1].append(j)

year += 1

# year += 1  # this is here for some reason but im not sure why

##############################################################################

# all bar plots

fsize = 18
tsize = 24

fig, ax = plt.subplots(10, 2, figsize=(14, 24), dpi=300)

ax = ax.flatten('F')

# seasonal si max and seasonal si min

bins = np.arange(17) + .5
clusters = np.arange(1, 17)

dc_clusters = np.array([])
nondc_clusters = np.array([])

for i, axis in enumerate(ax):

    # clust = np.hstack(np.array(mistral_data['cluster'])[ind[i]])
    clust = np.hstack(np.array(mistral_data['Cluster'])[ind[i]])
    
    if 1994+i in dc_years:
        dc_clusters = np.append(dc_clusters, clust)
    else:
        nondc_clusters = np.append(nondc_clusters, clust)

    counts, bins, _ = axis.hist(clust, bins=bins, rwidth=.9)
    axis.set_xticks(clusters)
    axis.set_xticklabels(clusters)

    if years[i] in dc_years:
        axis.set_title(str(years[i]) + '-' + str(years[i]+1), fontsize=fsize, color='tab:green')
    else:
        axis.set_title(str(years[i]) + '-' + str(years[i]+1), fontsize=fsize, color='k')

    axis.set_ylim(0.5, 17.5)

    axis.tick_params(axis='x', labelsize=fsize-4)
    axis.tick_params(axis='y', labelsize=fsize-4)

fig.suptitle('Mistral Clusters per Year', y=.99, fontsize=tsize)

fig.tight_layout()

# fig.savefig('../plots/mistral_cluster_bars.png')

##############################################################################
# cumulative mistrals

fsize += 2
tsize += 2

fig, ax = plt.subplots(3, 1, figsize=(16, 15), dpi=300)

# counts, bins, _ = ax[0].hist(dc_clusters, bins=bins, rwidth=.9)
counts, bins = np.histogram(dc_clusters, bins=bins)
dcperc = counts/pmws.dcounts[2] * 100
ax[0].bar(clusters, dcperc, width=.9)

# counts, bins, _ = ax[1].hist(nondc_clusters, bins=bins, rwidth=.9)
counts, bins = np.histogram(nondc_clusters, bins=bins)
ndcperc = counts/pmws.ndcounts[2] * 100
ax[1].bar(clusters, ndcperc, width=.9)

dperc = dcperc - ndcperc
posdif = np.where(dperc>=0, dperc, np.nan)
negdif = np.where(dperc<0, dperc, np.nan)

ax[2].bar(clusters, posdif, width=.9, color='tab:green')
ax[2].bar(clusters, negdif, width=.9, color='tab:red')

for i, axis in enumerate(ax):
    
    axis.set_xticks(clusters)
    axis.set_xticklabels(clusters)

    axis.set_xlim(0.4, 16.6)

    axis.tick_params(axis='x', labelsize=fsize-2)
    axis.tick_params(axis='y', labelsize=fsize-2)
    
for i, h in enumerate(dcperc):
    ax[0].text(i+1, h + .2, str(round(h, 1)), ha='center', fontsize=fsize)

for i, h in enumerate(ndcperc):
    ax[1].text(i+1, h + .2, str(round(h, 1)), ha='center', fontsize=fsize)

for i, h in enumerate(posdif):
    ax[2].text(i+1, h + .5, str(round(h, 1)), ha='center', va='center', fontsize=fsize)

for i, h in enumerate(negdif):
    ax[2].text(i+1, h - .6, str(round(h, 1)), ha='center', va='center', fontsize=fsize)

ax[0].set_ylim(0, 17)
ax[1].set_ylim(0, 17)
ax[2].set_ylim(-6.25, 7.75)

ax[0].text(1, 15, 'Mistrals = ' + str(pmws.dcounts[2]) + ' days', fontsize=fsize-2)
ax[1].text(1, 15, 'Mistrals = ' + str(pmws.ndcounts[2]) + ' days', fontsize=fsize-2)

ax[0].set_title('Deep convection years', pad=10, fontsize=tsize)
ax[1].set_title('Non-deep convection years', pad=10, fontsize=tsize)
ax[2].set_title('Difference (%deep - %non-deep)', pad=10, fontsize=tsize)

ax[0].set_ylabel('% Days', fontsize=fsize)
ax[1].set_ylabel('% Days', fontsize=fsize)
ax[2].set_ylabel('$\\Delta$% Days', fontsize=fsize)

ax[-1].set_xlabel('Clusters', fontsize=fsize)

fig.suptitle('Mistral day clustering', fontsize=tsize+4, y=.99)

fig.tight_layout()

fig.savefig('../plots/mistral_clusters_dc_nondc.png')
