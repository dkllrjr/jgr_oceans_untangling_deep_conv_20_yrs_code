from scipy.io import loadmat
import pandas
from datetime import datetime
import numpy as np
import pickle
import plot_si_bars as psb

##############################################################################

def datetime64todatetime(d64):
    d = np.empty_like(d64).astype(datetime)
    for i, item in enumerate(d64):
        d[i] = datetime.fromisoformat(str(item)[0:19])

    return d

##############################################################################

mist = loadmat('../data/Mist_intens_ind.mat')
mist = mist['C_ind2']

# rows
# cluster
# date
# mistral label
# weak (1) or strong (2)

date_mist = []
str_mist = []
cluster = []
label = []
boo = []

for i, date in enumerate(mist[1]):
    if mist[0][i] > 0:

        cluster.append(mist[0][i])

        label.append(mist[2][i])

        strdate = str(date)
        date_mist.append(datetime(year=int(strdate[0:4]), month=int(strdate[4:6]), day=int(strdate[6:8]), hour=12))

        if mist[3][i] < 2:
            str_mist.append('Weak')
            boo.append(0)

        else:
            str_mist.append('Strong')
            boo.append(1)

dmist = {'Date': date_mist, 'Label': label, 'Cluster': cluster, 'Strength': str_mist, 'Boolean': boo}
dfmist = pandas.DataFrame(dmist)

dfmist.to_csv('../data/mistral_intensity_clusters.csv')

##############################################################################
# separate 1993-2013

beg = datetime(1993, 7, 1, 12)
end = datetime(2013, 6, 30, 12)

ind = []

for i, date in enumerate(dfmist.Date):

    if beg <= date <= end:
        ind.append(i)

dfm20 = dfmist.iloc[ind]
dfm20.to_csv('../data/mistral_intensity_clusters_1993-2013.csv')

datesdc = datetime64todatetime(psb.datedc)

precond_days = []
nonprecond_days = []
precond_dates = []
precond_mistrals = []
for i, dc in enumerate(datesdc):

    precond_days.append((dc - pandas.to_datetime(pandas.Timestamp(psb.si_s_max_date[i]))).days)
    nonprecond_days.append((datetime(1994+i, 7, 1, 0) - datetime(1993+i, 6, 30, 0)).days - precond_days[-1])
    
    mistral_days = 0
    for it, date in enumerate(dfmist.Date):

        if pandas.to_datetime(pandas.Timestamp(psb.si_s_max_date[i])) <= date <= dc:

            precond_dates.append(it)
            mistral_days += 1

    precond_mistrals.append(mistral_days)

# precond_dates = np.array(precond_dates)

dfm20pre = dfmist.iloc[precond_dates]
dfm20pre.to_csv('../data//mistral_intensity_clusters_1993-2013_precond.csv')

precond_days = np.array(precond_days)
precond_mistrals = np.array(precond_mistrals)

percent_mistrals = precond_mistrals/precond_days

dy = np.array([1999, 2000, 2005, 2009, 2011, 2012, 2013])
y = np.arange(1994, 2014)

ind_dc = []
nind_dc = []
dyears = []
for i, year in enumerate(y):
    if year in dy:
        ind_dc.append(i)
    else:
        nind_dc.append(i)

per_data = {'percent': percent_mistrals, 'days': precond_days, 'mistrals': precond_mistrals}

with open('../data/precond_mistrals_percentage.pickle', 'wb') as file:
    pickle.dump(per_data, file)

##############################################################################
# Grouping dates into events

dates = dfm20pre.Date
dates_diff = np.diff(dates.values) 
events = []
strength = []
cluster = []

i = 0
while i < len(dates_diff):
    events.append([])
    events[-1].append(dates.iloc[i])
    strength.append(dfm20pre.Boolean.iloc[i])
    cluster.append([])
    cluster[-1].append(dfm20pre.Cluster.iloc[i])

    # print(np.timedelta64(dates_diff[i], 'D'))

    if np.timedelta64(dates_diff[i], 'D') < 2:

        while i < len(dates_diff) and np.timedelta64(dates_diff[i], 'D') < 4:
            i += 1
            events[-1].append(dates.iloc[i])
            cluster[-1].append(dfm20pre.Cluster.iloc[i])

    i += 1

# looking at the duration of the events
duration = []
for i, event in enumerate(events):
    duration.append(len(event))

# looking at the period between the beginning dates of each event
period = []
for i, event in enumerate(events[0:-1]):
    period.append((events[i+1][0] - events[i][0]).days)

dci = []
nondci = []

dc_years = np.array([1999, 2000, 2005, 2009, 2011, 2012, 2013])
dc_years -= 1

for year in dc_years:
    
    ind.append([])

    beg = datetime(year, 7, 1, 12)
    end = datetime(year+1, 6, 30, 12)
    
    for i, event in enumerate(events):
        if beg <= event[0] <= end:
            dci.append(i)
            # print(beg.year, end.year, event[0], i)
        

for i in range(len(events)):
    if i not in dci:
        nondci.append(i)

ave_dc_duration = np.mean(np.array(duration)[dci])
ave_nondc_duration = np.mean(np.array(duration)[nondci])
# print('average duration:', ave_dc_duration, ave_nondc_duration)

ave_dc_per = np.mean(np.array(period)[dci[0:-1]])
ave_nondc_per = np.mean(np.array(period)[nondci[0:-1]])
# print('average period:', ave_dc_per, ave_nondc_per)

##############################################################################
# Saving data

data = {'events': events, 'duration': duration, 'period': period, 'strength': strength, 'cluster': cluster}

with open('../data/mistral_attributes_1993-2013_precond.pickle','wb') as file:
    pickle.dump(data,file)
